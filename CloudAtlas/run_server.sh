#!/bin/bash


if [ $# -ne 1 ]; then
  echo "Usage: run_server.sh <conf.file>" >&2
  exit 1
fi

source pathes.sh

#set classpath to java classes
CLASSPATH=$CLASSPATH:$CA_PATH/bin/:$CA_PATH/lib/JLex.jar:/$CA_PATH/lib/cup.jar:/$CA_PATH/kryo/lib/minlog-1.2.jar:/$CA_PATH/kryo/lib/objenesis-1.2.jar:/$CA_PATH/kryo/lib/reflectasm-1.09-shaded.jar:/$CA_PATH/kryo/kryo-3.0.0.jar; export CLASSPATH;
#echo "CLASSPATH added $CA_PATH"; 

#run rmi if not running
RMIREG="rmiregistry"
if ps ax | grep -v grep | grep $RMIREG > /dev/null
then
    echo "$RMIREG is already running"
else
    echo "run $RMIREG"
		rmiregistry & 
fi

#run server
java -Djava.rmi.server.codebase=file:$CA_PATH/bin/ -Djava.rmi.server.hostname=$HOSTNAME -Djava.security.policy=file:$CA_PATH/rmi/server.policy pl.edu.mimuw.cloudatlas.main.Main $1

