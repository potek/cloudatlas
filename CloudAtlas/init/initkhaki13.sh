#!/bin/bash
export CA_PATH=$(dirname $(pwd))
export HOSTNAME=$(hostname)

export CLASSPATH=$CLASSPATH:$CA_PATH/bin:$CA_PATH/rmi

java -Djava.rmi.server.codebase=file:$CA_PATH/bin/ -Djava.security.policy=file:$CA_PATH/rmi/client.policy pl.edu.mimuw.cloudatlas.client.InitKhaki13 $HOSTNAME 
