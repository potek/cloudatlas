#!/bin/bash
rm -r crypto/CA_PotekSign/zones
rm -r crypto/CA_MaroSign/zones
#rm -r crypto/CA_PotekSign/clients
#rm -r crypto/CA_MaroSign/clients

#shortcuts:
potek=crypto/CA_PotekSign/
maro=crypto/CA_MaroSign/
smieci=/dev/null



root=ini/acc/Potek.conf\ ini/acc/Maro.conf 
uw=ini/acc/uw/Potek.conf\ ini/acc/uw/Maro.conf 
v07=ini/acc/uw/violet07/Potek.conf\ ini/acc/uw/violet07/Maro.conf 
k31=ini/acc/uw/khaki31/Potek.conf\ ini/acc/uw/khaki31/Maro.conf
k13=ini/acc/uw/khaki13/Potek.conf\ ini/acc/uw/khaki13/Maro.conf
pjwstk=ini/acc/pjwstk/Potek.conf\ ini/acc/pjwstk/Maro.conf
w01=ini/acc/pjwstk/whatever01/Potek.conf\ ini/acc/pjwstk/whatever01/Maro.conf
w02=ini/acc/pjwstk/whatever02/Potek.conf\ ini/acc/pjwstk/whatever02/Maro.conf



# ZONE CERT
./run_CA $potek 3600 /uw/ > $smieci
./run_CA $potek 3600 /uw/violet07/ > $smieci
./run_CA $maro 3600 /uw/khaki13/ > $smieci
./run_CA $maro 3600 /uw/khaki31/ > $smieci
./run_CA $maro 3600 /pjwstk/ > $smieci
./run_CA $maro 3600 /pjwstk/whatever01/ > $smieci
./run_CA $potek 3600 /pjwstk/whatever02/ > $smieci

#ACC /
./run_CA_ACC $potek 3600 / $root
#ACC /uw/
./run_CA_ACC $potek 3600 /uw/ $root $uw $v07 $k13 $k31
#ACC /uw/violet07/
./run_CA_ACC $potek 3600 /uw/violet07/ $uw $v07 $k13 $k31
#ACC /uw/khaki13/
./run_CA_ACC $maro 3600 /uw/khaki13/ $uw $v07 $k13 $k31
#ACC /uw/khaki31/
./run_CA_ACC $maro 3600 /uw/khaki31/ $uw $v07 $k13 $k31 
#ACC /pjwstk/
./run_CA_ACC $maro 3600 /pjwstk/ $pjwstk $uw $v07 $k13 $k31 $w02 $w01
#ACC /pjwstk/whatever01/
./run_CA_ACC $maro 3600 /pjwstk/whatever01/ $w02 $w01
#ACC /pjwstk/whatever02/
./run_CA_ACC $potek 3600 /pjwstk/whatever02/ $w02 $w01

# CA_CERT
./run_CA_client $potek 3600 ini/clients/uw/violet07/DataSender.ini > $smieci
./run_CA_client $potek 3600 ini/clients/uw/khaki13/DataSender.ini > $smieci
./run_CA_client $potek 3600 ini/clients/uw/khaki31/DataSender.ini > $smieci
./run_CA_client $potek 3600 ini/clients/pjwstk/whatever02/DataSender.ini > $smieci
./run_CA_client $potek 3600 ini/clients/pjwstk/whatever01/DataSender.ini > $smieci

./run_CA_client $maro 3600 ini/clients/Webapp_Maro.ini > $smieci
./run_CA_client $potek 3600 ini/clients/Webapp_Potek.ini >$smieci

