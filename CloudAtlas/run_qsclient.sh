#!/bin/bash
. pathes.sh

export CLASSPATH=$CLASSPATH:$CA_PATH/bin:$CA_PATH/rmi

java -Djava.rmi.server.codebase=file:$CA_PATH/bin/ -Djava.security.policy=file:$CA_PATH/rmi/qsclient.policy pl.edu.mimuw.cloudatlas.qsclient.Client $HOSTNAME
