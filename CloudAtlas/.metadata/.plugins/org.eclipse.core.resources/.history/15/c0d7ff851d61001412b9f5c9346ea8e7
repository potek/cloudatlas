/**
 * Copyright (c) 2014, University of Warsaw
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted
 * provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 * conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other materials provided
 * with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
 * WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package pl.edu.mimuw.cloudatlas.model;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.lang.Math;
import  pl.edu.mimuw.cloudatlas.interpreter.*;


/**
 * A class representing duration in milliseconds. The duration can be negative. This is a simple wrapper of a Java
 * <code>Long</code> object.
 */
public class ValueDuration extends ValueSimple<Long> {
	/**
	 * Constructs a new <code>ValueDuration</code> object wrapping the specified <code>value</code>.
	 * 
	 * @param value the value to wrap
	 */
	
	//public static final DateFormat TIME_FORMAT = new SimpleDateFormat("dd HH:mm:ss.SSS");
	//public static final DateFormat TIME_FORMAT = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SSS");
	
	final static long ONE_SECOND_IN_MILISECONDS = 1000;
	final static long ONE_MINUTE_IN_MILISECONDS = 60 * ONE_SECOND_IN_MILISECONDS;
	final static long ONE_HOUR_IN_MILISECONDS = 60 * ONE_MINUTE_IN_MILISECONDS;
	final static long ONE_DAY_IN_MILISECONDS = 24 * ONE_HOUR_IN_MILISECONDS;
	
	public ValueDuration(Long value) {
		super(value);
	}
	
	@Override
	public Type getType() {
		return TypePrimitive.DURATION;
	}
	

	
	@Override
	public Value getDefaultValue() {
		return new ValueDuration(0L);
	}
	
	/**
	 * Constructs a new <code>ValueDuration</code> object from the specified amounts of different time units.
	 * 
	 * @param seconds a number of full seconds
	 * @param milliseconds a number of milliseconds (an absolute value does not have to be lower than 1000)
	 */
	public ValueDuration(long seconds, long milliseconds) {
		this(seconds * 1000l + milliseconds);
	}
	
	/**
	 * Constructs a new <code>ValueDuration</code> object from the specified amounts of different time units.
	 * 
	 * @param minutes a number of full minutes
	 * @param seconds a number of full seconds (an absolute value does not have to be lower than 60)
	 * @param milliseconds a number of milliseconds (an absolute value does not have to be lower than 1000)
	 */
	public ValueDuration(long minutes, long seconds, long milliseconds) {
		this(minutes * 60l + seconds, milliseconds);
	}
	
	/**
	 * Constructs a new <code>ValueDuration</code> object from the specified amounts of different time units.
	 * 
	 * @param hours a number of full hours
	 * @param minutes a number of full minutes (an absolute value does not have to be lower than 60)
	 * @param seconds a number of full seconds (an absolute value does not have to be lower than 60)
	 * @param milliseconds a number of milliseconds (an absolute value does not have to be lower than 1000)
	 */
	public ValueDuration(long hours, long minutes, long seconds, long milliseconds) {
		this(hours * 60l + minutes, seconds, milliseconds);
	}
	
	/**
	 * Constructs a new <code>ValueDuration</code> object from the specified amounts of different time units.
	 * 
	 * @param days a number of full days
	 * @param hours a number of full hours (an absolute value does not have to be lower than 24)
	 * @param minutes a number of full minutes (an absolute value does not have to be lower than 60)
	 * @param seconds a number of full seconds (an absolute value does not have to be lower than 60)
	 * @param milliseconds a number of milliseconds (an absolute value does not have to be lower than 1000)
	 */
	public ValueDuration(long days, long hours, long minutes, long seconds, long milliseconds) {
		this(days * 24l + hours, minutes, seconds, milliseconds);
	}
	
	/**
	 * Constructs a new <code>ValueDuration</code> object from its textual representation. The representation has
	 * format: <code>sd hh:mm:ss.lll</code> where:
	 * <ul>
	 * <li><code>s</code> is a sign (<code>+</code> or <code>-</code>),</li>
	 * <li><code>d</code> is a number of days,</li>
	 * <li><code>hh</code> is a number of hours (between <code>00</code> and <code>23</code>),</li>
	 * <li><code>mm</code> is a number of minutes (between <code>00</code> and <code>59</code>),</li>
	 * <li><code>ss</code> is a number of seconds (between <code>00</code> and <code>59</code>),</li>
	 * <li><code>lll</code> is a number of milliseconds (between <code>000</code> and <code>999</code>).</li>
	 * </ul>
	 * <p>
	 * All fields are obligatory.
	 * 
	 * @param value a textual representation of a duration
	 * @throws IllegalArgumentException if <code>value</code> does not meet described rules
	 */
	public ValueDuration(String value) throws ParseException{
		this(parseDuration(value));
	}
	
	//(-|+)days hh:mm:ss.fff and 0 is formatted to +0,
	private static long parseDuration(String value) throws ParseException {
		long days, hours, minutes, seconds, miliseconds;
		boolean negative = false;
		value = value.trim();
		if(value.equals("+0"))
			return 0;		
		String[] days_and_time = value.split(" ");
		if(days_and_time.length == 1)
			throw new ParseException(value, value.length());
		else if(days_and_time.length > 2)
			throw new ParseException(value, days_and_time[0].length()+days_and_time[1].length());
		else{
			days = Integer.parseInt(days_and_time[0]);
			System.out.println("DAYS: " + days);
			if(days < 0){
				negative = true;
				days = days*(-1);
			}
			days_and_time[1] = days_and_time[1].trim();
			String[] hour_min_sec = days_and_time[1].split(":");
			if(hour_min_sec.length != 3)
				throw new ParseException(value, value.length());
			else{
				hours = Integer.parseInt(hour_min_sec[0]);
				System.out.println("HOURS: " + hours);
				minutes = Integer.parseInt(hour_min_sec[1]);
				System.out.println("MINUTES: " + minutes);
				String[] sec_mili = hour_min_sec[2].split("\.");
				if(sec_mili.length != 2){
					System.out.println(hour_min_sec[2]+"    "+sec_mili.length);
					throw new ParseException(value, value.length());
					
				}else{
					seconds = Integer.parseInt(sec_mili[0]);
					System.out.println("SECONDS: " + seconds);
					miliseconds = Integer.parseInt(sec_mili[1]);
					System.out.println("MILISECONDS: " + miliseconds);
				}
			}			
		}
		long time_duration  = 	miliseconds + 
								seconds * ONE_SECOND_IN_MILISECONDS + 
								minutes * ONE_MINUTE_IN_MILISECONDS + 
								hours * ONE_HOUR_IN_MILISECONDS + 
								days * ONE_DAY_IN_MILISECONDS;
		return ((negative) ? time_duration*(-1) : time_duration);
	}
	
	@Override
	public ValueBoolean isLowerThan(Value value) {
		sameTypesOrThrow(value, Operation.COMPARE);
		if(isNull() || value.isNull())
			return new ValueBoolean(null);
		return new ValueBoolean(getValue() < ((ValueDuration)value).getValue());
	}
	
	@Override
	public ValueDuration addValue(Value value) {
		if(isNull() || value.isNull())
			return new ValueDuration((Long)null);
		switch(value.getType().getPrimaryType()) {
			case DURATION:
				return new ValueDuration(getValue() + ((ValueDuration)value).getValue());
			default:
				throw new UnsupportedOperationException("Cannot add this type "+value.getType().getPrimaryType()+" to Duration");
		}
	}
		
	@Override
	public ValueDuration subtract(Value value) {
		if(isNull() || value.isNull())
			return new ValueDuration((Long)null);
		switch(value.getType().getPrimaryType()) {
			case DURATION:
				return new ValueDuration(getValue() - ((ValueDuration)value).getValue());
			default:
				throw new UnsupportedOperationException("Cannot aubtract this type "+value.getType().getPrimaryType()+" to Duration");
		}
	}
	
	@Override
	public ValueDuration multiply(Value value) {
		if(isNull() || value.isNull())
			return new ValueDuration((Long)null);
		switch(value.getType().getPrimaryType()) {
			case INT:
				return new ValueDuration(getValue() * ((ValueInt)value).getValue());
			case DOUBLE:
				return new ValueDuration(Math.round(getValue() * ((ValueDouble)value).getValue()));		
			default:
				throw new UnsupportedOperationException("Cannot aubtract this type "+value.getType().getPrimaryType()+" to Duration");
		}
	}
	
	@Override
	public Value divide(Value value) {
		if(isNull() || value.isNull())
			return new ValueDuration((Long)null);
		switch(value.getType().getPrimaryType()) {
			case INT:
				return new ValueDuration(getValue() / ((ValueInt)value).getValue());
			case DOUBLE:
				return new ValueDuration((long)Math.round(getValue() / ((ValueDouble)value).getValue()));		
			case DURATION:
				return new ValueDuration((long)Math.round(getValue() / ((ValueDuration)value).getValue()));		
			default:
				throw new UnsupportedOperationException("Cannot aubtract this type "+value.getType().getPrimaryType()+" to Duration");
		}
	}
	
	@Override
	public ValueDuration modulo(Value value) {
		throw new UnsupportedOperationException("Cannot perform modulo on ValueDuration");
	}
	
	@Override
	public ValueDuration negate() {
		throw new UnsupportedOperationException("Cannot perform negate on ValueDuration");
	}
	
	@Override
	public Value convertTo(Type type) {
		switch(type.getPrimaryType()) {
			case DURATION:
				return this;
			case DOUBLE:
				return new ValueDouble(getValue() == null? null : getValue().doubleValue());
			case INT:
				return new ValueInt(getValue() == null? null : getValue().longValue());
			case STRING:
				return getValue() == null? ValueString.NULL_STRING : new ValueString(getValue().toString());
			default:
				throw new UnsupportedConversionException(getType(), type);
		}
	}
	
	//(-|+)days hh:mm:ss.fff and 0 is formatted to +0,
	@Override
	public String toString(){
		if(getValue()==0)
			return "+0";
		int days = getDays();
		int hours = getHours();
		int minutes = getMinutes();
		int seconds = getSeconds();
		int miliseconds = getMiliseconds();
		String duration_str = (getValue()<0 ? "-" : "+") + Integer.toString(days);
		duration_str += " " + (hours<10 ? ("0"+Integer.toString(hours)) : (Integer.toString(hours)));
		duration_str += ":" + (minutes<10 ? ("0"+Integer.toString(minutes)) : (Integer.toString(minutes)));
		duration_str += ":" + (seconds<10 ? ("0"+Integer.toString(seconds)) : (Integer.toString(seconds)));
		
		if(miliseconds >= 100)
			duration_str += "." + Integer.toString(miliseconds);
		else if (miliseconds >= 10)
			duration_str += ".0" + Integer.toString(miliseconds);
		else
			duration_str += ".00" + Integer.toString(miliseconds);
		
		return duration_str;
	}
	
	public int getDays(){
		return (int) (Math.abs(getValue()/ONE_DAY_IN_MILISECONDS));
	}
	
	public int getHours(){
		return (int) (Math.abs((getValue()/ONE_HOUR_IN_MILISECONDS) % 24));
	}
	
	public int getMinutes(){
		return (int) (Math.abs((getValue()/ONE_MINUTE_IN_MILISECONDS) % 60));
	}
	
	public int getSeconds(){
		return (int) (Math.abs((getValue()/ONE_SECOND_IN_MILISECONDS) % 60));
	}
	
	public int getMiliseconds(){
		return (int) (Math.abs(getValue() % 1000));
	}
}
