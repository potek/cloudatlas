package pl.edu.mimuw.cloudatlas.interpreter;

import java.io.IOException;
import java.io.Serializable;

import pl.edu.mimuw.cloudatlas.certification.ClientCert;

public class Query implements Serializable {

	private static final long serialVersionUID = -7324550883159584621L;
	private String name;
	private String[] select_queries;
	private int minLevel, maxLevel;
	private QueryState queryState;
	private String CAname;
	private String owner;
	private ClientCert cc;

	public enum QueryState {
		NOT_CHECKED_YET, NOT_ACCEPTED, INSTALLED, UNINSTALLED
	}

	public Query(String name, String[] select_queries, int minLevel,
			int maxLevel, String CAname, String owner, ClientCert cc) {
		this.queryState = QueryState.NOT_CHECKED_YET;
		this.name = name;
		this.select_queries = select_queries;
		this.minLevel = minLevel;
		this.maxLevel = maxLevel;
		this.CAname = CAname;
		this.owner = owner;
		this.setCc(cc);
	}
	
	public Query(String name, String select_queries, int minLevel,
			int maxLevel, String CAname, String owner, ClientCert cc) {
		this(name, select_queries.split(";"), minLevel, maxLevel, CAname, owner, cc);
	}



	public QueryState getQueryState() {
		return queryState;
	}

	public void setQueryState(QueryState qs) {
		this.queryState = qs;
	}

	public String getName() {
		return name;
	}

	public String getOneSelect(int num) {
		return select_queries[num];
	}

	public String[] getAllSelect() {
		return select_queries;
	}

	public String getOwner() {
		return owner;
	}

	public int getMinLevel() {
		return minLevel;
	}

	public int getMaxLevel() {
		return maxLevel;
	}

	public String getCAname() {
		return CAname;
	}

	public Query copy() {
		Query copyQuery = new Query(name, select_queries, minLevel, maxLevel,
				CAname, owner, getCc());
		return copyQuery;
	}

	private void writeObject(java.io.ObjectOutputStream stream)
			throws IOException {

		stream.writeObject(minLevel);
		stream.writeObject(maxLevel);
		stream.writeObject(owner);
		stream.writeObject(CAname);
		stream.writeObject(name);
		stream.writeObject(queryState);
		stream.writeObject(getCc());
		
		stream.writeObject(select_queries.length);
		for (String s : select_queries) {
			stream.writeObject(s);
		}
	}

	private void readObject(java.io.ObjectInputStream stream)
			throws IOException, ClassNotFoundException {

		minLevel = (int) stream.readObject();
		maxLevel = (int) stream.readObject();
		owner = (String) stream.readObject();
		CAname = (String) stream.readObject();
		name = (String) stream.readObject();
		queryState = (QueryState) stream.readObject();
		setCc((ClientCert) stream.readObject());
		
		int size = (int) stream.readObject();
		select_queries = new String[size];
		for (int i = 0; i < size; i++) {
			select_queries[i] = (String) stream.readObject();
		}
	}

	public String toString() {
  
    	String ret = "(status="+queryState + ", Signed by="+ CAname + ", Provided by="+owner+", min level="+minLevel +", maxLevel="+maxLevel+") ";
    	for(String s : select_queries) {
    		ret += s + "; ";
    	}
    	return ret;
    }

	public Boolean getIsActive() {
		return (queryState == QueryState.INSTALLED);
	}

	public boolean equals(Query q) {

		if (!q.getName().equals(this.getName()))
			return false;

		if (q.getAllSelect().length != this.getAllSelect().length)
			return false;

		for (int i = 0; i < q.getAllSelect().length; i++) {
			if (!q.getAllSelect()[i].equals(this.getAllSelect()[i]))
				return false;
		}

		return true;
	}

	public ClientCert getCc() {
		return cc;
	}

	public void setCc(ClientCert cc) {
		this.cc = cc;
	}
}
