package pl.edu.mimuw.cloudatlas.interpreter;

@SuppressWarnings("serial")
public class NoSuchQueryException extends InterpreterException {
	protected NoSuchQueryException(String message) {
		super(message);
	}
}