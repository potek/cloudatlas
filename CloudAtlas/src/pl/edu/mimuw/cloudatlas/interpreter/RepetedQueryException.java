package pl.edu.mimuw.cloudatlas.interpreter;

@SuppressWarnings("serial")
public class RepetedQueryException extends InterpreterException {
	protected RepetedQueryException(String message) {
		super(message);
	}
}