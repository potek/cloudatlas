package pl.edu.mimuw.cloudatlas.interpreter;

@SuppressWarnings("serial")
public class NullSonsArgumentsException extends InterpreterException {
	protected NullSonsArgumentsException(String message) {
		super(message);
	}
}
