/**
 * Copyright (c) 2014, University of Warsaw
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted
 * provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 * conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other materials provided
 * with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
 * WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package pl.edu.mimuw.cloudatlas.interpreter;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import pl.edu.mimuw.cloudatlas.model.Type;
import pl.edu.mimuw.cloudatlas.model.TypeCollection;
import pl.edu.mimuw.cloudatlas.model.Value;
import pl.edu.mimuw.cloudatlas.model.ValueBoolean;
import pl.edu.mimuw.cloudatlas.model.ValueList;

class ResultColumn extends Result {
	private final ValueList values;

	public ResultColumn(ValueList values) {
		this.resultType = ResultType.COLUMN;
		this.values = values;
	}
	
	@Override
	protected ResultColumn binaryOperationTyped(BinaryOperation operation, ResultSingle right) {
		List<Value> newListValue = new ArrayList<Value>();
		for(Value v : values) {
			newListValue.add(operation.perform(v, right.getValue()));
		}
		return new ResultColumn(new ValueList(newListValue, TypeCollection.computeElementType(newListValue)));
	}

	@Override
	public ResultColumn unaryOperation(UnaryOperation operation) {
		List<Value> newListValue = new ArrayList<Value>();
		for(Value v : values) {
			newListValue.add(operation.perform(v));
		}
		return new ResultColumn(new ValueList(newListValue, TypeCollection.computeElementType(newListValue)));
	}

	protected ResultColumn callMe(BinaryOperation operation, Result left) {
		List<Value> newListValue = new ArrayList<Value>();
		switch(left.resultType){
			case COLUMN: 
				Iterator<Value> itl = left.getColumn().iterator();
				if(left.getColumn().size() != values.size())
					throw new UnsupportedOperationException("Cannot operate on two different sizes columns");
				
				for(Value v : values) {
					ResultSingle tmpR = new ResultSingle(v);
					ResultSingle tmpL = new ResultSingle(itl.next());
					newListValue.add(tmpL.binaryOperationTyped(operation, tmpR).getValue());
				}				
				break;
			case SINGLE:								
				for(Value v : values) {
					ResultSingle tmpR = new ResultSingle(v);					
					newListValue.add(left.binaryOperationTyped(operation, tmpR).getValue());
				}				
				break;	
			case LIST:
				throw new UnsupportedOperationException("Cannot perform binary operation on LIST and COLUMN");
		}
		return new ResultColumn(new ValueList(newListValue, TypeCollection.computeElementType(newListValue)));
	}	
	
	@Override
	public ResultSingle aggregationOperation(AggregationOperation operation) {
		return new ResultSingle(operation.perform(values));
	}

	@Override
	public Result transformOperation(TransformOperation operation) {
		return new ResultList(operation.perform(values));
	}
	
	@Override
	public Value getValue() {
		//return this.values;
		throw new UnsupportedOperationException("RC Not a ResultSingle.");
	}

	@Override
	public ValueList getList() {
		throw new UnsupportedOperationException("RC Not a ResultList.");
	}

	@Override
	public ValueList getColumn() {
		return values;
	}

	@Override
	public Result filterNulls() {
		ValueList newValueList = filterNullsList(this.values);
		return new ResultColumn(newValueList);
	}

	@Override
	public Result first(int size) {
		ValueList newValueList = firstList(this.values, size);
		return new ResultSingle(newValueList);
	}

	@Override
	public Result last(int size) {
		ValueList newValueList = lastList(this.values, size);
		return new ResultSingle(newValueList);
	}

	@Override
	public Result random(int size) {
		ValueList newValueList = randomList(this.values, size);
		return new ResultSingle(newValueList);
	}

	@Override
	public Result convertTo(Type to) {
		List<Value> newListValue = new ArrayList<Value>();
		for(Value v : values) {
			newListValue.add(v.convertTo(to));
		}
		return new ResultColumn(new ValueList(newListValue, TypeCollection.computeElementType(newListValue)));
	}


	@Override
	public ResultSingle isNull() {
		return new ResultSingle(new ValueBoolean(values.isNull()));
	}

	@Override
	public Type getType() {
		return ((TypeCollection)(values.getType())).getElementType();
	}
}
