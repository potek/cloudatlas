package pl.edu.mimuw.cloudatlas.webapp;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.zip.InflaterInputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

import pl.edu.mimuw.cloudatlas.certification.ClientCert;
import pl.edu.mimuw.cloudatlas.common.CryptoManager;
import pl.edu.mimuw.cloudatlas.main.FetchDataInterface;
import pl.edu.mimuw.cloudatlas.model.Attribute;
import pl.edu.mimuw.cloudatlas.model.AttributesMap;
import pl.edu.mimuw.cloudatlas.model.Type.PrimaryType;
import pl.edu.mimuw.cloudatlas.model.Value;
import pl.edu.mimuw.cloudatlas.model.ValueDouble;
import pl.edu.mimuw.cloudatlas.model.ValueInt;
import pl.edu.mimuw.cloudatlas.model.ZMI;



public class WebApp {
	
	private final static Logger LOGGER = Logger.getLogger(WebApp.class.getName());
	
	private static final ThreadLocal<Kryo> kryoThreadLocal = new ThreadLocal<Kryo>() {
		@Override
		protected Kryo initialValue() {
			Kryo kryo = new Kryo();
			kryo.register(ClientCert.class);
			return kryo;
		}
	};

	public static Registry registry_fetch;
	public static Registry registry_qs;
	public static FetchDataInterface stub;
	public static NumericValueHolder nvh;
	
	public static final int REFRESH_MS = 2000;
	
	public static PublicKey CA_PublicKey;
	public static PrivateKey webapp_PrivateKey;
	public static ClientCert webapp_cc;
	
	private static String parseZMIToHtml(ZMI zmi) {
		StringBuilder buf = new StringBuilder();

		buf.append("<table id=\"t01\"><tr><th>Name</th> <th> Value </th><th> Type </th> </tr>");

		Iterator<Entry<Attribute, Value>> it = zmi.getAttributes().iterator();
		while (it.hasNext()) {
			Entry<Attribute, Value> pair = it.next();
			buf.append("<tr>");
			buf.append("<td>" + pair.getKey().getName() + "</td>");
			buf.append("<td>" + pair.getValue().toString() + "</td>");
			buf.append("<td>" + pair.getValue().getType().toString() + "</td>");
			buf.append("</tr>");
		}

		buf.append("</table>");
		return buf.toString();
	}
	

	public static void main(String[] args) {
		try {
			Path pathToCert = Paths.get(args[2]);
			File cert_f = new File(pathToCert.toString(), "cert_client.crt");
			Kryo kryo = kryoThreadLocal.get();
			Input in = new Input(new InflaterInputStream(new FileInputStream(cert_f)));
			//kryo.register(ClientCert.class);
			//ClientCert ncc = (ClientCert)kryo.readClassAndObject(in);
			
			
			webapp_cc = (ClientCert)kryo.readClassAndObject(in);
//			System.out.println("Client Cert po serializacji " + webapp_cc.toString());

			
			try {
				webapp_PrivateKey = CryptoManager.getPrivateKey(pathToCert.toString()+"/private_key.der");
			} catch (InvalidKeySpecException e) {
				LOGGER.log(Level.WARNING, "InvalidKeySpecException");
			}
			
			registry_qs = LocateRegistry.getRegistry(args[0]);
			registry_fetch = LocateRegistry.getRegistry("localhost");
			stub = (FetchDataInterface) registry_fetch.lookup("FetchData:" + args[1]);

			HttpServer server = HttpServer.create(new InetSocketAddress(18000),
					0);
			server.createContext("/", new IndexHandler());
			server.createContext("/zones", new ZoneHandler());
			server.createContext("/resources", new ResourceHandler());
			server.createContext("/install", new InstallHandler(stub, webapp_cc, webapp_PrivateKey));
			//server.createContext("/uninstall", new UninstallHandler(stub, webapp_cc));
			
			server.setExecutor(null); // creates a default executor
			server.start();
		} catch (Exception e) {
			LOGGER.log(Level.WARNING, "WebApp in main exception", e);
		}
	}

	static class IndexHandler implements HttpHandler {
		public void handle(HttpExchange t) throws IOException {
			StringBuilder buf = new StringBuilder();

			buf.append("<html><head>");

			//buf.append(addRefresh(5L));

			buf.append("</head><body>");

			buf.append("<h1>CloudAtlas Web Application</h1>");

			buf.append("<h2>Known Zones</h2>");
			Set<String> knownZones = stub.getKnownZones(webapp_cc);

			buf.append("<table>");
			for (String s : knownZones) {
				buf.append("<tr><td><a href=\"/zones/" + s + "\"> " + s	+ "</a></td>");
				buf.append("<td><a href=\"/install/" + s + "\"> Install new Query </a></td>");
				//buf.append("<td><a href=\"/uninstall/" + s + "\"> Uninstall Query </a></td></tr>");
			}
			buf.append("</table>");

			String response = buf.toString();
			t.sendResponseHeaders(200, response.length());
			OutputStream os = t.getResponseBody();
			os.write(response.getBytes());
			os.close();
		}
	}

	static class ZoneHandler implements HttpHandler {		
		public static String CHART_SCRIPT = 
				"<script type=\"text/javascript\" src=\"/resources/amcharts.js\"></script>\n"
				+ "<script type=\"text/javascript\" src=\"/resources/serial.js\"></script>\n"
				+ "<script type=\"text/javascript\" src=\"/resources/none.js\"></script>\n";

		private static String createCharts() {
			StringBuilder buf = new StringBuilder();
			//buf.append("<div id=\"chartdiv\"></div>");
			
			Iterator<Entry<String, Map<Long, Number>>> it = nvh.iterator();
			while(it.hasNext()) {
				Entry<String, Map<Long, Number>> entry = it.next();
				buf.append("<h3> Attribute: " + entry.getKey() + "</h3>");
				buf.append("<div class=\"chartdiv\" id=\"chartdiv"+entry.getKey()+"\"></div><br><br>");
			}
			return buf.toString();
		}
		
		private static String createDataChar(String name) {

			StringBuilder buf = new StringBuilder();
			
			buf.append("var chart = AmCharts.makeChart(\"chartdiv"+name+"\", { \n"+
					"    \"type\": \"serial\", \n"+
					"    \"theme\": \"none\", \n"+
					"     \"pathToImages\": \"http://www.amcharts.com/lib/3/images/\", \n"+
					"    \"dataProvider\": [ \n");
					
			Map<Long, Number> map = nvh.getValues(name);
			Iterator<Entry<Long, Number>> it = map.entrySet().iterator();
			while(it.hasNext()) {
				Entry<Long, Number> entry = it.next();
				buf.append("{ \"timestamp\": \""+entry.getKey() + "\",\n");
				buf.append("\"value\": "+entry.getValue().toString() + " }");
				if(it.hasNext())
					buf.append(",\n");
				else {
					buf.append("],\n");
				}
			}
			
			buf.append(
					"    \"valueAxes\": [{\n"+
					"        \"axisAlpha\": 0,\n"+
					"        \"ignoreAxisWidth\": true,\n"+
					"        \"inside\": true,\n"+
					"        \"position\": \"left\"\n"+
					"    }],\n"+
					"    \"graphs\": [{\n"+
					"        \"balloonText\": \"[[category]]<br><b>[[value]]</b>\",\n"+
					"        \"type\": \"step\",\n"+
					"        \"bullet\":\"square\",\n"+
					"        \"bulletAlpha\":0,\n"+
					"        \"bulletSize\":4,\n"+
					"        \"bulletBorderAlpha\":0,\n"+
					"        \"valueField\": \"value\"\n"+
					"    }],\n"+
					"    \"chartScrollbar\": {},\n"+
					"    \"chartCursor\": {\n"+
					"        \"cursorAlpha\": 0,\n"+
					"        \"graphBulletAlpha\":1,\n"+
					"        \"cursorPosition\": \"mouse\"\n"+
					"    },\n"+
					"    \"categoryField\": \"timestamp\",\n"+
					"    \"categoryAxis\": {\n"+
					"        \"parseDates\": false,\n"+
					"        \"gridAlpha\": 0,\n"+
					"		 \"labelRotation\" :  -90\n" +
					"    }\n"+
					"});\n"
					);
			
			return buf.toString();
		}
		
		private static String createJavaScript() {

			StringBuilder buf = new StringBuilder();
			buf.append(CHART_SCRIPT);
			buf.append(
				"<script type=\"text/javascript\">\n"+
				"stop = false;\n" +
				"var interval = setInterval(function() {if(!stop) {window.location.reload()}}, "+REFRESH_MS+");"+
				"</script>\n");
			
			buf.append("<script type=\"text/javascript\">");
					
			Iterator<Entry<String, Map<Long, Number>>> it = nvh.iterator();
			
			while(it.hasNext()) {
				Entry<String, Map<Long, Number>> entry = it.next();
				buf.append(createDataChar(entry.getKey()));
			}

			buf.append("</script>");
      
		    return buf.toString();
		}
		
		private String createStyle() {

			StringBuilder buf = new StringBuilder();
			buf.append("<style type=\"text/css\">\n");
			
			buf.append("table { \n"
					+ "   width:100%;\n" + "}\n" + "table, th, td {\n"
					+ "    border: 1px solid black;\n"
					+ "    border-collapse: collapse;\n" + "}\n" + "th, td {\n"
					+ "    padding: 5px;\n" + "    text-align: left;\n" + "}\n"
					+ "table#t01 tr:nth-child(even) {\n"
					+ "    background-color: #eee;\n" + "}\n"
					+ "table#t01 tr:nth-child(odd) {\n"
					+ "   background-color:#fff;\n" + "}\n"
					+ "table#t01 th	{\n" + "    background-color: black;\n"
					+ "    color: white;\n" + "}\n");

			
			buf.append(".chartdiv { \n" +
						"width	: 100%; \n"+
						"height	: 500px;\n" +
						"}\n");	
			
			buf.append("</style>");

			return buf.toString();

		}

		private String createHeader() {
			StringBuilder buf = new StringBuilder();
			buf.append("<head>\n");
			//buf.append(addRefresh(5L));
			buf.append(createStyle());
			buf.append(createJavaScript());
			buf.append("</head>\n");

			return buf.toString();
		}

		private String createBody(HttpExchange t, Boolean isValid, ZMI zmi) throws RemoteException {
			StringBuilder buf = new StringBuilder();
			buf.append("<body>\n");
			buf.append("<h1>CloudAtlas Web Application</h1><br>");

			String zoneName = t.getRequestURI().toASCIIString()
					.replaceFirst("/zones/", "");
			
			//buf.append("<button onclick=\"function(){$(document).clearInterval(interval)};\">Stop refresh</button>");

			buf.append("<h2> Zone: " + zoneName + "</h2>");

			if (isValid) {
				buf.append(parseZMIToHtml(zmi));
			} else {
				buf.append("Such zone doesn't exist!<br>");
			}

			buf.append(createCharts());
			
			buf.append("</body>\n");

			return buf.toString();
		}

		public void updateNVHwithZMI(ZMI zmi) {
			AttributesMap am = zmi.getAttributes();
			Iterator<Entry<Attribute, Value>> it = am.iterator();
			while(it.hasNext()) {
				Entry<Attribute, Value> entry = it.next();
				PrimaryType entryType = entry.getValue().getType().getPrimaryType();
				switch (entryType) {
					case DOUBLE:
						ValueDouble vd = (ValueDouble) entry.getValue();
						nvh.addValue(entry.getKey().getName(), vd.getValue(), new Date().getTime());
						break;
					case INT:
						ValueInt vi = (ValueInt) entry.getValue();
						nvh.addValue(entry.getKey().getName(), vi.getValue(), new Date().getTime());
						break;
				default:
					break;
				}
			}
		}

		public void handle(HttpExchange t) throws IOException {
			StringBuilder buf = new StringBuilder();
			
			String zoneName = t.getRequestURI().toASCIIString().replaceFirst("/zones/", "");;
			if(nvh == null) {
				nvh = new NumericValueHolder(zoneName, 30);
			}
			
			if(!nvh.owner.equals(zoneName)) {
				nvh = new NumericValueHolder(zoneName, 30);
			}

			buf.append("<!DOCTYPE html>\n<html>");


			
			Set<String> knownZones = stub.getKnownZones(webapp_cc);
			if (knownZones.contains(zoneName)) {
				ZMI zmi = stub.getZoneZMI(zoneName, webapp_cc);
				updateNVHwithZMI(zmi);
				buf.append(createHeader());
				buf.append(createBody(t, true, zmi));
			} else {
				buf.append(createBody(t, false, null));
			}
			

			buf.append("</html>");
			
			String response = buf.toString();
			t.sendResponseHeaders(200, response.length());
			OutputStream os = t.getResponseBody();
			os.write(response.getBytes());
			os.close();
		}

	}
}
