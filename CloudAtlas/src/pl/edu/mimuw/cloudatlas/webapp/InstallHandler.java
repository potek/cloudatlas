package pl.edu.mimuw.cloudatlas.webapp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.security.PrivateKey;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import pl.edu.mimuw.cloudatlas.certification.AFCert;
import pl.edu.mimuw.cloudatlas.certification.ClientCert;
import pl.edu.mimuw.cloudatlas.interpreter.Query;
import pl.edu.mimuw.cloudatlas.main.FetchDataInterface;
import pl.edu.mimuw.cloudatlas.model.ZMI;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

public class InstallHandler implements HttpHandler {

	FetchDataInterface stub;

	public String postMessage;
	private ClientCert webapp_cc;
	private PrivateKey webapp_PrivateKey;

	public InstallHandler(FetchDataInterface stub, ClientCert webapp_cc,
			PrivateKey webapp_PrivateKey) {
		this.stub = stub;
		this.webapp_cc = webapp_cc;
		this.setWebapp_PrivateKey(webapp_PrivateKey);
	}

	private void parseQuery(String query, Map<String, String> parameters)
			throws UnsupportedEncodingException {

		if (query != null) {
			String pairs[] = query.split("[&]");
			for (String pair : pairs) {
				String param[] = pair.split("[=]");
				String key = null;
				String val = null;
				if (param.length > 0) {
					key = URLDecoder.decode(param[0],
							System.getProperty("file.encoding"));
				}

				if (param.length > 1) {
					val = URLDecoder.decode(param[1],
							System.getProperty("file.encoding"));
				}
				parameters.put(key, val);
			}
		}
	}

	private void parsePostParameters(HttpExchange exchange) throws IOException {
		if ("post".equalsIgnoreCase(exchange.getRequestMethod())) {
			InputStreamReader isr = new InputStreamReader(
					exchange.getRequestBody(), "utf-8");
			BufferedReader br = new BufferedReader(isr);
			String query = br.readLine();
			String zoneName = exchange.getRequestURI().toASCIIString()
					.replaceFirst("/install/", "");
			
			Map<String, String> parameters = new HashMap<String, String>();
			parseQuery(query, parameters);

			// TODO set min & max level
			int minLevel = -1;
			int maxLevel = -1;
			try {
				minLevel = Integer.parseInt(parameters.get("minLevel"));
				maxLevel = Integer.parseInt(parameters.get("maxLevel"));
				String name = parameters.get("qname");
				String body = parameters.get("qbody");

				Long id = (new Random()).nextLong();
				Long creation = (new Date()).getTime();
				Long expiry = creation + 3600000;

				AFCert afc = new AFCert(id, creation, expiry, "&" + name, body,
						minLevel, maxLevel, webapp_cc);

				afc.generateSignature(webapp_PrivateKey, true);
//				System.out.println("AFC" + afc);
//				
//				System.out.println("AFC validacja " + afc.validSignature(webapp_cc.getPublickey()));

				// Query q = new Query("&" + parameters.get("qname"),
				// parameters.get("qbody"), minLevel, maxLevel);

				if (stub.installQuery(afc, zoneName)) {
					postMessage = "Query Installed";
				} else {
					postMessage = "Query cannot be installed";
				}
			} catch (NumberFormatException e) {
				System.err.println("Cannot parse " + parameters.get("minLevel")
						+ " " + parameters.get("maxLevel"));
			}
		}

	}

	private String showQueries(ZMI zmi, String zoneName) {
		StringBuilder buf = new StringBuilder();

		buf.append("<h2>Install Query " + zoneName + "</h2>");

		List<Query> lq = zmi.getQueries();

		buf.append("<table><th>Query Name</th><th>Query Body</th><th>Active</th>");
		for (Query q : lq) {
			buf.append("<tr><td>" + q.getName() + "</td><td>"
					+ concatSelect(q.getAllSelect()) + "</td><td>"
					+ (q.getIsActive() ? "YES" : "NO") + "</td></tr>");
		}
		buf.append("</table>");

		return buf.toString();
	}

	private String concatSelect(String[] selects) {
		StringBuilder buf = new StringBuilder();
		for (String s : selects) {
			buf.append(s + "; ");
		}
		return buf.toString();
	}

	@Override
	public void handle(HttpExchange t) throws IOException {
		StringBuilder buf = new StringBuilder();

		buf.append("<html><head>");

		String zoneName = t.getRequestURI().toASCIIString()
				.replaceFirst("/install/", "");

		postMessage = "";
		parsePostParameters(t);

		ZMI zmi = stub.getZoneZMI(zoneName, webapp_cc);
		// buf.append(addRefresh(5L));

		buf.append("</head><body>");

		buf.append("<h1>CloudAtlas Web Application</h1>");

		buf.append(showQueries(zmi, zoneName));

		buf.append("<br>");
		buf.append("<form action=\"http://127.0.0.1:18000/install/" + zoneName
				+ "\" method=\"post\">"
				+ "Query Name: &<input type=\"text\" name=\"qname\"><br>"
				+ "Query Body: <input type=\"text\" name=\"qbody\"><br>"
				+ "Query MinLevel: <input type=\"text\" name=\"minLevel\"><br>"
				+ "Query MaxLevel: <input type=\"text\" name=\"maxLevel\"><br>"
				+ "<input type=\"submit\" value=\"Submit\">" + "</form>");

		if (postMessage != null && postMessage != "") {
			buf.append("<br><h3>" + postMessage + "</h3><br>");
		}

		String response = buf.toString();
		t.sendResponseHeaders(200, response.length());
		OutputStream os = t.getResponseBody();
		os.write(response.getBytes());
		os.close();

	}

	public PrivateKey getWebapp_PrivateKey() {
		return webapp_PrivateKey;
	}

	public void setWebapp_PrivateKey(PrivateKey webapp_PrivateKey) {
		this.webapp_PrivateKey = webapp_PrivateKey;
	}

}
