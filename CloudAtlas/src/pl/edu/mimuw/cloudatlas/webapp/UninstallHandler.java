//package pl.edu.mimuw.cloudatlas.webapp;
//
//import java.io.BufferedReader;
//import java.io.IOException;
//import java.io.InputStreamReader;
//import java.io.OutputStream;
//import java.io.UnsupportedEncodingException;
//import java.net.URLDecoder;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//import java.util.logging.Level;
//import java.util.logging.Logger;
//
//import pl.edu.mimuw.cloudatlas.certification.ClientCert;
//import pl.edu.mimuw.cloudatlas.interpreter.Query;
//import pl.edu.mimuw.cloudatlas.main.FetchDataInterface;
//import pl.edu.mimuw.cloudatlas.model.ZMI;
//
//import com.sun.net.httpserver.HttpExchange;
//import com.sun.net.httpserver.HttpHandler;
//
//public class UninstallHandler implements HttpHandler {
//	private final static Logger LOGGER = Logger.getLogger(UninstallHandler.class
//			.getName());
//	private String postMessage;
//	FetchDataInterface stub;
//	private ClientCert webapp_cc;
//	
//	public UninstallHandler(FetchDataInterface stub, ClientCert webapp_cc) {
//		this.stub = stub;
//		this.setWebapp_cc(webapp_cc);
//		LOGGER.setLevel(Level.ALL);
//	}
//	private void parseQuery(String query, Map<String, String> parameters)
//			throws UnsupportedEncodingException {
//
//		if (query != null) {
//			String pairs[] = query.split("[&]");
//			for (String pair : pairs) {
//				String param[] = pair.split("[=]");
//				String key = null;
//				String val = null;
//				if (param.length > 0) {
//					key = URLDecoder.decode(param[0],
//							System.getProperty("file.encoding"));
//				}
//
//				if (param.length > 1) {
//					val = URLDecoder.decode(param[1],
//							System.getProperty("file.encoding"));
//				}
//				parameters.put(key, val);
//			}
//		}
//	}
//	
//
//
//	private void parsePostParameters(HttpExchange exchange) throws IOException {
//		if ("post".equalsIgnoreCase(exchange.getRequestMethod())) {
//			InputStreamReader isr = new InputStreamReader(
//					exchange.getRequestBody(), "utf-8");
//			BufferedReader br = new BufferedReader(isr);
//			String query = br.readLine();
//			LOGGER.logp(Level.INFO, "UninstallHandler", "parsePostParameters", "Parse query " + query);
//			Map<String, String> parameters = new HashMap<String, String>();
//			parseQuery(query, parameters);
//
//			//TODO UNINSTALL QUERY
////			SignedQueryUn sqr = stub_qs.uninstallQuery("&" + parameters.get("qname"));
////			if (sqr.isValid) {
////				stub.uninstallQuery(sqr);
////				postMessage = "Query Uninstalled";
////			} else {
////				postMessage = "Cannot uninstall query " + parameters.get("qname");
////			}
//		}
//	}
//	
//	private String showQueries(ZMI zmi, String zoneName) {
//		StringBuilder buf = new StringBuilder();
//		
//		buf.append("<h2>Uninstall Query "+zoneName+"</h2>");
//
//		List<Query> lq = zmi.getQueries();
//		
//		buf.append("<table><th>Query Name</th><th>Query Body</th><th>Active</th>");
//		for (Query q : lq) {
//			buf.append("<tr><td>" + q.getName()+ "</td><td>"+concatSelect(q.getAllSelect())+"</td><td>"+ (q.getIsActive() ? "YES" : "NO")+ "</td></tr>");
//		}
//		buf.append("</table>");
//		
//		return buf.toString();
//	}
//	
//	private String concatSelect(String[] selects) {
//		StringBuilder buf = new StringBuilder();
//		for(String s : selects) {
//			buf.append(s + "; ");
//		}
//		return buf.toString();
//	}
//	
//	@Override
//	public void handle(HttpExchange t) throws IOException {
//		StringBuilder buf = new StringBuilder();
//
//		buf.append("<html><head>");
//
//		String zoneName = t.getRequestURI().toASCIIString()
//				.replaceFirst("/uninstall/", "");
//		
//		postMessage = "";
//		parsePostParameters(t);
//		
//		ZMI zmi = stub.getZoneZMI(zoneName, webapp_cc);
//		//buf.append(addRefresh(5L));
//
//		buf.append("</head><body>");
//
//		buf.append("<h1>CloudAtlas Web Application</h1>");
//		
//		buf.append( showQueries(zmi, zoneName));
//
//		buf.append("<br>");
//		buf.append("<form action=\"http://127.0.0.1:18000/uninstall/" + zoneName
//				+ "\" method=\"post\" target=\"_blank\">"
//				+ "Query Name: &<input type=\"text\" name=\"qname\"><br>"
//				+ "<input type=\"submit\" value=\"Submit\">" + "</form>");
//
//
//		if (postMessage != null && postMessage != "") {
//			buf.append("<br><h3>" + postMessage + "</h3><br>");
//		}
//
//		String response = buf.toString();
//		t.sendResponseHeaders(200, response.length());
//		OutputStream os = t.getResponseBody();
//		os.write(response.getBytes());
//		os.close();
//
//	}
//	public ClientCert getWebapp_cc() {
//		return webapp_cc;
//	}
//	public void setWebapp_cc(ClientCert webapp_cc) {
//		this.webapp_cc = webapp_cc;
//	}
//}
