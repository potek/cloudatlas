package pl.edu.mimuw.cloudatlas.webapp;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

public class NumericValueHolder {

	private Map<String, Map<Long, Number>> holder;
	public String owner;
	public int limit;
	
	public int getSize() {
		return holder.size();
	}

	public NumericValueHolder(String owner, int limit) {
		this.limit = limit;
		this.owner = owner;
		holder = new HashMap<String, Map<Long, Number>>();
	}

	public void addValue(String name, Number val, Long timestamp) {
		if (holder.containsKey(name)) {
			if(holder.get(name).size() >= limit) {
				Long k = holder.get(name).entrySet().iterator().next().getKey();
				holder.get(name).remove(k);
			}
			holder.get(name).put(timestamp, val);
		} else {
			Map<Long, Number> m = new TreeMap<Long, Number>();
			m.put(timestamp, val);
			holder.put(name, m);
		}
	}

	public Iterator<Entry<String, Map<Long, Number>>> iterator() {
		return holder.entrySet().iterator();
	}

	public Map<Long, Number> getValues(String name) {
		if (holder.containsKey(name)) {
			return holder.get(name);
		} else {
			return null;
		}
	}

}
