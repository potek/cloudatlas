package pl.edu.mimuw.cloudatlas.webapp;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;


import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

public class ResourceHandler implements HttpHandler {

	private final static Logger LOGGER = Logger.getLogger(ResourceHandler.class.getName());
	
	@Override
	public void handle(HttpExchange t) {
		try {
			Headers h = t.getResponseHeaders();
			h.add("Content-Type", "application/pdf");

			String s = t.getRequestURI().toASCIIString().replaceFirst("/", "");

			File file = new File(s);
			if (file.exists()) {
				byte[] bytearray = new byte[(int) file.length()];
				FileInputStream fis = new FileInputStream(file);
				BufferedInputStream bis = new BufferedInputStream(fis);
				bis.read(bytearray, 0, bytearray.length);
				bis.close();
				// ok, we are ready to send the response.
				t.sendResponseHeaders(200, file.length());
				OutputStream os = t.getResponseBody();
				os.write(bytearray, 0, bytearray.length);
				os.close();

			} else {
				t.sendResponseHeaders(404, 0);
				OutputStream os = t.getResponseBody();
				os.close();
			}
		}

		catch (IOException e) {
			LOGGER.log(Level.WARNING, "IOException");
		}

	}

}
