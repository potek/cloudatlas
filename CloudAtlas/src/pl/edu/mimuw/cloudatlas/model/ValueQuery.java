package pl.edu.mimuw.cloudatlas.model;

import java.io.IOException;

import pl.edu.mimuw.cloudatlas.interpreter.Query;
import pl.edu.mimuw.cloudatlas.interpreter.Query.QueryState;

public class ValueQuery extends Value {
	private static final long serialVersionUID = -251324371160095896L;
	private Query value;
	
	public ValueQuery(Query query){
		this.value = query;
	}

	public Query getQuery(){
		return value;
	}
	
	public void setQuery(Query q) {
		this.value = q;
	}
	
	@Override
	public Type getType() {
		return TypePrimitive.QUERY;
	}

	@Override
	public boolean isNull() {
		return value == null;
	}

	@Override
	public boolean isCollection() {
		return false;
	}

	@Override
	public Value convertTo(Type to) {
		throw new UnsupportedConversionException(getType(), to);
	}

	@Override
	public Value getDefaultValue() {
		return new ValueQuery(new Query("", "", 0, 0, "", "", null));
	}
	
	@Override
	public String toString() {
		return value.toString();
	}

    private void writeObject(java.io.ObjectOutputStream stream)
            throws IOException {
    	stream.writeObject(getQuery());
    }

    private void readObject(java.io.ObjectInputStream stream)
            throws IOException, ClassNotFoundException {
    	Query value = (Query) stream.readObject();
    	setQuery(value);
    }

	public void setQueryState(QueryState qs) {
		this.value.setQueryState(qs);
	}

}
