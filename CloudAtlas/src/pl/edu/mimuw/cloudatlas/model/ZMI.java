/**
 * Copyright (c) 2014, University of Warsaw
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted
 * provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 * conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other materials provided
 * with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
 * WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package pl.edu.mimuw.cloudatlas.model;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.InflaterInputStream;

import pl.edu.mimuw.cloudatlas.certification.AccessControlCert;
import pl.edu.mimuw.cloudatlas.interpreter.Query;
import pl.edu.mimuw.cloudatlas.interpreter.RepetedQueryException;
import pl.edu.mimuw.cloudatlas.main.Pair;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import com.esotericsoftware.kryo.serializers.JavaSerializer;

/**
 * A zone management information. This object is a single node in a zone hierarchy. It stores zone attributes as well as
 * references to its father and sons in the tree.
 */
public class ZMI implements Cloneable, Serializable {

	private static final ThreadLocal<Kryo> kryoThreadLocal 
       = new ThreadLocal<Kryo>() {
    
    @Override
    protected Kryo initialValue() {
        Kryo kryo = new Kryo();
        kryo.register(AttributesMap.class);
        kryo.register(ValueSet.class, new JavaSerializer());
        kryo.register(ValueList.class, new JavaSerializer());

        return kryo;
    }
	};
	
	private static final long serialVersionUID = 3928329889859248865L;

	private AttributesMap attributes = new AttributesMap();
	private AccessControlCert acc;
	
	/**
import javax.management.Query;
	 * Creates a new ZMI with no father (the root zone) and empty sons list.
	 */
	public ZMI() {
		
	}
	
	public ArrayList<ZMI> getSons(ArrayList<Pair<ZMI, ArrayList<ZMI>>> hierarchyTree){
		String name = attributes.get("name").toString();
		int level = (int) (long) ((ValueInt) attributes.get("level")).getValue();
		if(level >= hierarchyTree.size()-1)
			return new ArrayList<ZMI>();
		if(hierarchyTree.get(level).getFirst().getName().equals(name)){
			ArrayList<ZMI> sons = new ArrayList<>();
			sons.addAll(hierarchyTree.get(level+1).getSecond());
			sons.add(hierarchyTree.get(level+1).getFirst());
			return sons;
		}
		return new ArrayList<ZMI>();
	}
	
	/**
	 * Gets a map of all the attributes stored in this ZMI.
	 * 
	 * @return map of attributes
	 */
	public AttributesMap getAttributes() {
		return attributes;
	}
	
	public void setACC(AccessControlCert acc){
		this.acc = acc;
	}
	
	public AccessControlCert getAcc(){
		return acc;
	}
	
	public List<Query> getActiveQueries(){
		return attributes.getActiveQueries();
	}
	
	public List<Query> getAllQueries(){
		return attributes.getAllQueries();
	}
	
	public List<Query> getNotCheckedYetQueries(){
		return attributes.getNotCheckedQueries();
	}
	
	public String getName(){
		return attributes.getName();
	}
	
	/**
	 * Prints recursively in a prefix order (starting from this ZMI) a whole tree with all the attributes.
	 * 
	 * @param stream a destination stream
	 * @see #toString()
	 */
	public void printAttributes(PrintStream stream) {
		for(Entry<Attribute, Value> entry : attributes)
			stream.println(entry.getKey() + " : " + entry.getValue().getType() + " = " + entry.getValue());
		stream.println();
	}
	
	public List<Query> getQueries() {
		LinkedList<Query> answer = new LinkedList<Query>();
		for(Entry<Attribute, Value> att : getAttributes()){
			if(Attribute.isQuery(att.getKey())){
				answer.add(((ValueQuery)att.getValue()).getQuery());
			}
		}		
		return answer;
	}
	
//	public void installQuery(String queryName, String querySelect, int minLevel, int maxLevel) throws RepetedQueryException {
//		Query q = new Query(queryName, querySelect, minLevel, maxLevel);
//		installQuery(q);
//	}
//	
//	public void installQuery(Query q){
//		this.getAttributes().addOrChange(q.getName(), new ValueQuery(q));
//	}
	
	
	
	/**
	 * Creates an independent copy of a whole hierarchy. A returned ZMI has the same reference as a father (but the
	 * father does not have a reference to it as a son). For the root zone, the copy is completely independent, since
	 * its father is <code>null</code>.
	 * 
	 * @return a deep copy of this ZMI
	 */
	@Override
	public ZMI clone() {
		ZMI result = new ZMI();
		result.attributes.add(attributes.clone());
		result.acc = acc.clone();
		return result;
	}
	
	/**
	 * Prints a textual representation of this ZMI. It contains only attributes of this node.
	 * 
	 * @return a textual representation of this object
	 * @see #printAttributes(PrintStream)
	 */
	@Override
	public String toString() {
		if(attributes != null)
			return (attributes.toString()+"\n");
		else return "null";
	}
	
//    private void writeObject(java.io.ObjectOutputStream stream)
//            throws IOException {
//    		
//    	stream.writeObject(attributes);
//    }
//
//    private void readObject(java.io.ObjectInputStream stream)
//            throws IOException, ClassNotFoundException {
//    	attributes = (AttributesMap) stream.readObject();
//    }
    
	private void writeObject(java.io.ObjectOutputStream stream)
			throws IOException {
		Kryo kryo = kryoThreadLocal.get();
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(
				16384);
		DeflaterOutputStream deflaterOutputStream = new DeflaterOutputStream(
				byteArrayOutputStream);
		Output output = new Output(deflaterOutputStream);
		kryo.writeObject(output, attributes);
		kryo.writeClassAndObject(output, acc);
		output.close();

		byte[] bytes = byteArrayOutputStream.toByteArray();
		stream.write(bytes);
	}

	private void readObject(java.io.ObjectInputStream stream)
			throws IOException, ClassNotFoundException {

		InputStream in = new InflaterInputStream(stream);

		Input input = new Input(in);
		Kryo kryo = kryoThreadLocal.get();
		attributes = kryo.readObject(input, AttributesMap.class);
		acc = (AccessControlCert)kryo.readClassAndObject(input);
	}


}