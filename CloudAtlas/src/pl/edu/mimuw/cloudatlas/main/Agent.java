package pl.edu.mimuw.cloudatlas.main;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

import pl.edu.mimuw.cloudatlas.certification.AFCert;
import pl.edu.mimuw.cloudatlas.certification.AccessControlCert;
import pl.edu.mimuw.cloudatlas.certification.ClientCert;
import pl.edu.mimuw.cloudatlas.certification.ZoneCert;
import pl.edu.mimuw.cloudatlas.common.CryptoManager;
import pl.edu.mimuw.cloudatlas.interpreter.Query;
import pl.edu.mimuw.cloudatlas.message.Message;
import pl.edu.mimuw.cloudatlas.message.MessageAnswerOnConnectRequest;
import pl.edu.mimuw.cloudatlas.message.MessageConnectRequest;
import pl.edu.mimuw.cloudatlas.message.MessageSendMessage;
import pl.edu.mimuw.cloudatlas.message.MessageAnswerOnConnectRequest.TypeOfAnswer;
import pl.edu.mimuw.cloudatlas.message.exportinfo.MessageExportInfo;
import pl.edu.mimuw.cloudatlas.model.Attribute;
import pl.edu.mimuw.cloudatlas.model.AttributesMap;
import pl.edu.mimuw.cloudatlas.model.TypePrimitive;
import pl.edu.mimuw.cloudatlas.model.Value;
import pl.edu.mimuw.cloudatlas.model.ValueContact;
import pl.edu.mimuw.cloudatlas.model.ValueInt;
import pl.edu.mimuw.cloudatlas.model.ValueQuery;
import pl.edu.mimuw.cloudatlas.model.ValueSet;
import pl.edu.mimuw.cloudatlas.model.ValueString;
import pl.edu.mimuw.cloudatlas.model.ValueTime;
import pl.edu.mimuw.cloudatlas.model.ZMI;
import pl.edu.mimuw.cloudatlas.module.Module;
import pl.edu.mimuw.cloudatlas.module.Module.ModuleType;
import pl.edu.mimuw.cloudatlas.module.ModuleCommunication;
import pl.edu.mimuw.cloudatlas.module.ModuleConnectRecipient;
import pl.edu.mimuw.cloudatlas.module.ModuleExecuteQueries;
import pl.edu.mimuw.cloudatlas.module.ModuleExportInfo;
import pl.edu.mimuw.cloudatlas.module.ModuleGossiper;
import pl.edu.mimuw.cloudatlas.module.ModuleNotFoundException;
import pl.edu.mimuw.cloudatlas.module.ModulePropagateQuery;
import pl.edu.mimuw.cloudatlas.module.ModuleTimer;

public class Agent extends Thread {
	private AgentConfiguration conf;
	private ArrayList<Module> modules;
	private ModuleCommunication communicationModule;
	private ModuleExportInfo exportInfoModule;
	private Set<ValueContact> fallbackContacts;
	private final static Logger LOGGER = Logger
			.getLogger(Agent.class.getName());
	private ArrayList<CertificatesInNode> certificates;
	private HashMap<String, PublicKey> keysToCA;
	private AccessControlCert accessControlCert;

	/*
	 * hierarchyTree - structure of all Agents the machine knows
	 * hierarchyTree[0] - level 0 - pair <root, NULL> (root doesn't have
	 * siblings) hierarchyTree[1] - level 1 - pair <root son - our ancestor,
	 * (rest root sons)> ... hierarchyTree[last] - level n - pair <singleton
	 * Zone, (its siblings)>
	 */
	private ArrayList<Pair<ZMI, ArrayList<ZMI>>> hierarchyTree;

	public Agent(ArrayList<Pair<ZMI, ArrayList<ZMI>>> hierarchyTree,
			String host, int port, AgentConfiguration conf,
			ArrayList<CertificatesInNode> certificates)
			throws UnknownHostException {
		this.hierarchyTree = hierarchyTree;
		this.certificates = certificates;
		this.keysToCA = new HashMap<String, PublicKey>();
		this.conf = (conf == null ? new AgentConfiguration() : conf);
		this.communicationModule = new ModuleCommunication(host, port, this,
				fallbackContacts);
		this.modules = new ArrayList<Module>();
		this.modules.add(new ModuleTimer("Timer", this));
		this.modules.add(new ModuleGossiper("Gossiper", this,
				this.conf.FIRST_GOSSIP_AFTER_TIME,
				this.conf.GOSSIP_PERIOD_BEGIN, this.conf.GOSSIP_PERIOD_END,
				this.conf.TYPE_OF_STRATEGY));
		this.modules.add(new ModuleExportInfo("FetchData", this));
		this.modules.add(new ModuleConnectRecipient("ConnectRecipient", this));
		this.modules.add(new ModuleExecuteQueries("Interpreter", this,
				this.conf.QUERY_EXECUTE_PERIOD));
		this.modules.add(new ModulePropagateQuery("PropagateQuery", this));
	}

	public ModuleCommunication getCommunicationModule() {
		return communicationModule;
	}

	public InetAddress getHost() {
		return communicationModule.getHost();
	}

	public int getPort() {
		return communicationModule.getPort();
	}

	public void addContact(String id, InetAddress host, int port)
			throws UnknownHostException {
		communicationModule.addContact(id, host, port);
	}

	public void addModule(Module m) {
		this.modules.add(m);
	}

	public ArrayList<Module> getModules() {
		return modules;
	}

	public int getNumberOfLevels() {
		return hierarchyTree.size();
	}

	public int getNumberOfSiblings(int level) {
		return hierarchyTree.get(level).getSecond().size();
	}

	public ZMI getZMIonLevel(int level) {
		return hierarchyTree.get(level).getFirst();
	}

	public ZMI getSiblingZMIonLevel(int level, int sibling) {
		return hierarchyTree.get(level).getSecond().get(sibling);
	}

	public ZMI getSiblingZMIonLevelName(int level, String name) {
		ArrayList<ZMI> siblings = hierarchyTree.get(level).getSecond();
		for (ZMI zmi : siblings) {
			if (name.equals(zmi.getName()))
				return zmi;
		}
		return null;
	}

	public ArrayList<ZMI> getSiblingsOnLevel(int level) {
		return hierarchyTree.get(level).getSecond();
	}

	public ZMI getMachineZMI() {
		return hierarchyTree.get(hierarchyTree.size() - 1).getFirst();
	}

	public ArrayList<Pair<ZMI, ArrayList<ZMI>>> getHierarchyTree() {
		return hierarchyTree;
	}

	public void messageToExportInfoModule(MessageExportInfo msg)
			throws ModuleNotFoundException {
		boolean exists = (exportInfoModule != null);

		Iterator<Module> it = modules.iterator();
		while (!exists && it.hasNext()) {
			Module m = it.next();
			if (m.getModuleType() == ModuleType.MODULE_EXPORT_INFO) {
				exportInfoModule = (ModuleExportInfo) m;
				exists = true;
			}
		}

		if (exists) {
			exportInfoModule.messageToModule(msg);
		} else {
			throw new ModuleNotFoundException("Module FetchData doesn't exists");
		}
	}

	@Override
	public void start() {
		Value contacts = getMachineZMI().getAttributes().getOrNull("contacts");
		boolean cannotAddAgent = false;
		if (contacts != null)
			if (((ValueSet) contacts).size() != 0)
				try {
					cannotAddAgent = connectToCloudAtlasNetwork();
				} catch (UnknownHostException e) {
					LOGGER.log(Level.WARNING, "Unknown Host - cannot add Agent!");
					cannotAddAgent = true;
				}

		if (!cannotAddAgent) {
			communicationModule.start();
			for (Module m : modules)
				m.start();
			super.start();
		}
	}

	public long getActualTime() {
		return (new Date().getTime());
	}

	public void updateTimestamp(ZMI zmiToUpdate) {
		zmiToUpdate.getAttributes().addOrChange("timestamp",
				new ValueTime(getActualTime()));
	}

	public void addOrChangeValue(String name, Value val, ClientCert cc) {

		PublicKey ca_publickey = findKeyToVerification(cc.getCAname());

		if (ca_publickey != null) {
			if (cc.validSignature(ca_publickey)) {
				ZMI myZMI = getMachineZMI();

				Boolean canAddOrChange = false;
				
				if (cc.canWrite(name)) {
					AccessControlCert myACC = myZMI.getAcc();

					canAddOrChange = myACC.canWrite(
							myZMI.getName(),
							cc.getCAname(), name);

				}

				if (canAddOrChange) {
					myZMI.getAttributes().addOrChange(name, val);
					this.updateTimestamp(myZMI);
				} else {
					LOGGER.log(Level.WARNING,
							"Client has no permission to write");
				}
			} else {
				LOGGER.log(Level.WARNING, "Client Cert validation failed ");
			}
		} else {
			LOGGER.log(Level.WARNING,
					"Cannot read public key of CA " + cc.getCAname());
		}

	}

	public boolean connectToCloudAtlasNetwork() throws UnknownHostException {
		String sender = getMachineZMI().getAttributes().get("owner").toString();
		MessageConnectRequest newRequest = new MessageConnectRequest(sender,
				communicationModule.getHost(), communicationModule.getPort());
		MessageSendMessage msg = new MessageSendMessage(sender, "", getName(),
				"", newRequest);
		LinkedBlockingQueue<Message> waitOnQueue = new LinkedBlockingQueue<Message>();

		Iterator<Value> contact_it = getIteratorThroughContact(((ValueSet) getMachineZMI()
				.getAttributes().get("contacts")));
		ValueContact v;

		ReceiverConnectRequest myReceiver = new ReceiverConnectRequest(
				waitOnQueue, communicationModule.getPort());
		myReceiver.start();
		synchronized (waitOnQueue) {
			while (waitOnQueue.isEmpty()) {
				System.err.println("Trying to connect - waiting for response");
				LOGGER.log(Level.INFO,
						"Trying to connect - waiting for response");

				if (contact_it.hasNext()) {
					v = (ValueContact) contact_it.next();
				} else {
					contact_it = getIteratorThroughContact(((ValueSet) getMachineZMI()
							.getAttributes().get("contacts")));
					v = (ValueContact) contact_it.next();
				}
				InetAddress address = v.getAddress();
				int destPort = v.getPort();
				ZoneCert zoneCert = findPropoerZoneCert(certificates, sender);
				
				newRequest.setZoneCert(zoneCert);
				try {

					byte[] message;
					ByteArrayOutputStream bos = new ByteArrayOutputStream();
					ObjectOutput out = new ObjectOutputStream(bos);
					out.writeObject((MessageSendMessage) msg);
					LOGGER.log(Level.INFO, "Send a message to: " + v.getName()
							+ " " + address + " " + destPort);
					message = bos.toByteArray();
					bos.close();

					// Initialize a datagram packet with data and address
					DatagramPacket packet = new DatagramPacket(message,
							message.length, address, destPort);

					// Create a datagram socket, send the packet through it,
					// close
					// it.
					DatagramSocket dsocket = new DatagramSocket();
					dsocket.send(packet);
					dsocket.close();
				} catch (Exception e) {
					LOGGER.logp(Level.SEVERE, "Agent",
							"connectToCloudAtlasNetwork", "", e);
				}

				try {
					(new WakeUper(waitOnQueue)).start();
					waitOnQueue.wait();
				} catch (InterruptedException e) {
					LOGGER.log(Level.WARNING, "Interupt Exception - connectToCloudAtlasNetwork()");
				}
			}
		}

		MessageAnswerOnConnectRequest msgRequest = (MessageAnswerOnConnectRequest) waitOnQueue
				.poll();

		if (msgRequest.getTypeOfAnswer() == TypeOfAnswer.REJECT_KEY_MISMATCH) {
			LOGGER.log(Level.WARNING,
					"Certificate and PublicKey mismatch. Cannot connect to CloudAtlas.");
			return true;
		} else if (msgRequest.getTypeOfAnswer() == TypeOfAnswer.REJECT_EXPIRED) {
			LOGGER.log(Level.WARNING,
					"Certificate expired. Cannot connect to CloudAtlas.");
			return true;
		} else if (msgRequest.getTypeOfAnswer() == TypeOfAnswer.CONFIRM_BUT_UNAUTHORIZED_HOST) {
			LOGGER.log(Level.WARNING,
					"DANGER! UNAUTHORIZED HOST. CONNECTION REFUSED");
			return true;
		}

		// msgRequest.getTypeOfAnswer() == TypeOfAnswer.CONFIRM
		connectTree(msgRequest.getTreeOfHierarchy());
		return false;
	}

	private Iterator<Value> getIteratorThroughContact(ValueSet contactSet) {
		LinkedList<Value> listWithContact = new LinkedList<Value>();

		ArrayList<LinkedList<ValueContact>> closenessOfContact = new ArrayList<LinkedList<ValueContact>>();
		Iterator<Value> contacts = contactSet.iterator();

		ValueContact v;
		int closeness;

		while (contacts.hasNext()) {
			v = (ValueContact) contacts.next();
			closeness = countCloseness(v);
			if (closenessOfContact.size() <= closeness) {
				int tmp = closeness + 1 - closenessOfContact.size();
				for (int i = 0; i < tmp; i++)
					closenessOfContact.add(new LinkedList<ValueContact>());
			}
			closenessOfContact.get(closeness).add(v);
		}

		for (LinkedList<ValueContact> ll : closenessOfContact) {
			for (ValueContact vc : ll) {
				for (int i = 0; i < 3; i++) {
					listWithContact.add(vc);
				}
			}
		}

		return listWithContact.iterator();
	}

	private int countCloseness(ValueContact v) {
		String myPath = getMachineZMI().getAttributes().get("owner").toString();
		String contactPath = v.getName().toString();

		if (myPath.equals(contactPath))
			return 0;

		String[] elementsMyPath = myPath.split("/");
		String[] elementsContactPath = contactPath.split("/");
		int closeness = elementsMyPath.length - 1;
		int level = 0;
		while (elementsContactPath.length > level
				&& elementsMyPath.length > level
				&& elementsContactPath[level].equals(elementsMyPath[level])) {
			level++;
			closeness--;
		}

		return closeness;
	}

	private ZoneCert findPropoerZoneCert(
			ArrayList<CertificatesInNode> certificates, String sender) {
		
		int myLevel = sender.split("/").length - 2;
		return certificates.get(myLevel).getZoneCertificate();
		
	}
	
	public void unionContactSet(ValueSet contacts, ValueSet newContacts) {
		for (Value v_new : newContacts) {
			ValueContact vc_new = (ValueContact) v_new;
			String vc_new_name = vc_new.getName().getName();
			Boolean canAdd = true;
			for (Value v_old : contacts) {
				ValueContact vc_old = (ValueContact) v_old;
				String vc_old_name = vc_old.getName().getName();
				if (vc_old_name.equals(vc_new_name)) {
					canAdd = false;
					break;
				}
			}
			if (canAdd) {
				contacts.add(vc_new);
			}
		}
	}

	public void connectTree(ArrayList<Pair<ZMI, ArrayList<ZMI>>> treeToConnect) {
		int i = -1;
		ZMI treeOnLevel, connectTreeOnLevel;
		String tmp1, tmp2;
		do {
			++i;
			treeOnLevel = hierarchyTree.get(i).getFirst();
			connectTreeOnLevel = treeToConnect.get(i).getFirst();
			tmp1 = treeOnLevel.getName();
			tmp2 = connectTreeOnLevel.getName();
			if (tmp1 == tmp2 || tmp1.equals(tmp2)) {
				hierarchyTree.get(i).setFirst(connectTreeOnLevel);
				hierarchyTree.get(i)
						.setSecond(treeToConnect.get(i).getSecond());
			}
		} while (tmp1 == tmp2 || tmp1.equals(tmp2));

		ArrayList<ZMI> newBrothers = new ArrayList<ZMI>();

		newBrothers.add(connectTreeOnLevel);
		for (ZMI zmi : treeToConnect.get(i).getSecond()) {
			if (!zmi.getName().equals(tmp1)) {
				newBrothers.add(zmi);
			}
		}

		hierarchyTree.get(i).setSecond(newBrothers);

		ValueSet contacts = (ValueSet) hierarchyTree.get(i).getFirst()
				.getAttributes().get("contacts");
		if (contacts == null) {
			contacts = new ValueSet(TypePrimitive.CONTACT);
		}
		for (Pair<ZMI, ArrayList<ZMI>> p : treeToConnect) {
			unionContactSet(contacts, (ValueSet) p.getFirst().getAttributes()
					.get("contacts"));
			for (ZMI zmi : p.getSecond()) {
				unionContactSet(contacts,
						(ValueSet) zmi.getAttributes().get("contacts"));
			}
		}

		hierarchyTree.get(i).getFirst().getAttributes()
				.addOrChange("contacts", contacts);

	}

	public void createNode(String name) throws Exception {
		LOGGER.log(Level.INFO, "Connect new node via gossiping");
		String[] levels = name.split("/");
		if (levels.length == 0) {
			LOGGER.log(Level.SEVERE, "Critical error - cannot create root node");
			throw new Exception();
		} else {
			String tmpName = "/";
			for (int i = 1; i < levels.length; i++) {
				tmpName += levels[i] + "/";
				if (getNodeByName(tmpName) == null) {
					ZMI node = new ZMI();
					node.getAttributes().add("level",
							new ValueInt(Long.valueOf(i)));
					node.getAttributes().add("name", new ValueString(tmpName));
					node.getAttributes().add("owner", new ValueString(""));
					node.getAttributes().add("timestamp", new ValueTime(0L));
					node.getAttributes().add("contacts",
							new ValueSet(TypePrimitive.CONTACT));
					hierarchyTree.get(i).getSecond().add(node);
				}
			}
		}
	}

	private class ReceiverConnectRequest extends Thread {
		LinkedBlockingQueue<Message> messageQueue;
		int port;

		public ReceiverConnectRequest(LinkedBlockingQueue<Message> m, int port) {
			this.messageQueue = m;
			this.port = port;
		}

		@Override
		public void run() {
			ServerSocket serverSocket = null;
			ObjectInputStream fromClient = null;
			ObjectOutputStream toClient = null;
			try {
				serverSocket = new ServerSocket(port);
				Socket socket = serverSocket.accept();
				toClient = new ObjectOutputStream(new BufferedOutputStream(
						socket.getOutputStream()));
				fromClient = new ObjectInputStream(new BufferedInputStream(
						socket.getInputStream()));
				MessageAnswerOnConnectRequest msgRequest = (MessageAnswerOnConnectRequest) fromClient
						.readObject();

				if (msgRequest.getTypeOfAnswer() == TypeOfAnswer.CONFIRM) {
					ZoneCert zoneCert = msgRequest.getZoneCert();
					if(validateZoneCert(zoneCert)){
						toClient.writeObject(new MessageAnswerOnConnectRequest(
								hierarchyTree, null));
						toClient.flush();
					}
					else{
						msgRequest.setAsInvalid();
					}
				}
				messageQueue.add(msgRequest);

				socket.close();
				serverSocket.close();

			} catch (IOException | ClassNotFoundException e) {
				LOGGER.logp(Level.SEVERE, "Agent",
						"connectToCloudAtlasNetwork",
						"Cannot create ServerSocket", e);

			}

		}

		private boolean validateZoneCert(ZoneCert zoneCert) {
			PublicKey keyToVerification = findKeyToVerification(zoneCert.getCAname());
			if(keyToVerification == null) 
				return false;
			return zoneCert.validSignature(keyToVerification);
		}
	}

	private class WakeUper extends Thread {
		LinkedBlockingQueue<Message> wakeUpQueue;

		public WakeUper(LinkedBlockingQueue<Message> wakeUpQueue) {
			this.wakeUpQueue = wakeUpQueue;
		}

		@Override
		public void run() {
			try {
				Thread.sleep(4000);
				synchronized (wakeUpQueue) {
					wakeUpQueue.notify();
				}
			} catch (InterruptedException e) {
				LOGGER.log(Level.WARNING, "Interupt Exception - WakeUper");
			}
		}
	}

	public void setFallbackContacts(Set<ValueContact> contacts) {
		fallbackContacts = contacts;
	}

	public AttributesMap getAttributesMap(String zoneName) {
		ZMI zmi = getNodeByName(zoneName);
		if (zmi != null) {
			return zmi.getAttributes();
		} else {
			return null;
		}
	}

	public Boolean addQuery(AFCert afc, String zoneName) {
		if (!validateClientCert(afc.getClient())) {
			LOGGER.log(
					Level.WARNING,
					"Authentication failed. Cannot validate Client Certificate. Query cannot be added");
			return false;
		}
		if (!afc.validSignature(afc.getClient().getPublickey())) {
			LOGGER.log(
					Level.WARNING,
					"Authentication failed. Cannot validate Aggregation Function Certificate. Query cannot be added");
			return false;
		}
		
		Query q = new Query(afc.getName(), afc.getCode(), afc.getMinLevel(),
				afc.getMaxLevel(), afc.getClient().getCAname(), zoneName, afc.getClient());
		
		getMachineZMI().getAttributes().addOrChange(afc.getName(),
				new ValueQuery(q));
		return true;
	}

	public void updateTimestampOnMyPath() {
		for (Pair<ZMI, ArrayList<ZMI>> pair : hierarchyTree) {
			updateTimestamp(pair.getFirst());
		}
	}

	public Boolean uninstallQuery(AFCert squ) {
		return false;
	}

	public void removeAttribute(String name) {
		this.getMachineZMI().getAttributes().remove(name);
	}

	public Set<String> getKnownZones() {
		Set<String> knownZones = new TreeSet<String>();
		for (Pair<ZMI, ArrayList<ZMI>> pair : hierarchyTree) {
			ZMI curZMI = pair.getFirst();
			if (curZMI.getName() != null) {
				knownZones.add(curZMI.getName());
			} else {
				LOGGER.log(Level.WARNING, "ZMI has no name");
			}

			ArrayList<ZMI> siblings = pair.getSecond();
			for (ZMI zmi : siblings) {
				if (zmi.getName() != null) {
					knownZones.add(zmi.getName());
				} else {
					LOGGER.log(Level.WARNING, "Sibling ZMI has no name");
				}
			}
		}
		return knownZones;
	}
	
//	@Override
//	public void run(){
//		while(true){
//			try {
//				Thread.sleep(10000);
//			} catch (InterruptedException e) {
//				LOGGER.log(Level.WARNING, "Interupt Exception - Agent - run()");
//			}
//			int level = 0;
//			for(Pair<ZMI, ArrayList<ZMI>> pair: hierarchyTree){
//				System.out.println("---------------\nLevel " + level);
//				System.out.println("Main:" + pair.getFirst()+"\n");
//				int son=1;
//				for(ZMI zmi: pair.getSecond()){
//					System.out.println("Son " + son);
//					System.out.println(zmi);
//					son++;
//				}
//				level++;
//			}
//		}
//	}

	public ZMI getNodeByName(String zoneName) {
		if (zoneName.equals("/"))
			return hierarchyTree.get(0).getFirst();
		else {
			int level = zoneName.split("/").length - 1;
			Pair<ZMI, ArrayList<ZMI>> pair = hierarchyTree.get(level);
			ZMI curZMI = pair.getFirst();
			if (zoneName.equals(curZMI.getName()))
				return curZMI;

			ArrayList<ZMI> siblings = pair.getSecond();
			for (ZMI zmi : siblings) {
				if (zoneName.equals(zmi.getName()))
					return zmi;
			}
		}
		return null;
	}

	public Value getAttributeOfZone(String zoneName, String attrName) {
		ZMI zmi = getNodeByName(zoneName);
		if (zmi != null) {
			updateTimestamp(zmi);
			return zmi.getAttributes().getOrNull(attrName);
		} else {
			return null;
		}
	}

	public ArrayList<CertificatesInNode> getCertificates() {
		return certificates;
	}

	public boolean containsKey(String nameOfCA) {
		return keysToCA.containsKey(nameOfCA);
	}

	public PublicKey getKeyToCA(String nameOfCA) {
		return keysToCA.get(nameOfCA);
	}

	public void putKeyToCA(String nameOfCA, PublicKey pk) {
		keysToCA.put(nameOfCA, pk);
	}

	public PublicKey findKeyToVerification(String nameOfCA) {
		if (this.containsKey(nameOfCA))
			return this.getKeyToCA(nameOfCA);

		PublicKey CApublicKey = null;
		try {
			CApublicKey = CryptoManager.getPublicKey(nameOfCA
					+ "public_key.der");
			this.putKeyToCA(nameOfCA, CApublicKey);
		} catch (Exception e) {
			LOGGER.log(Level.WARNING, "Cannot find key of CA");
		}

		return CApublicKey;
	}

	private Boolean validateClientCert(ClientCert cc) {
		PublicKey pk = findKeyToVerification(cc.getCAname());
		if (pk == null)
			return false;
		return cc.validSignature(pk);
	}

	public ZMI getExportZMI(String zoneName, ClientCert clientCert) {
		ZMI myZMI = getNodeByName(zoneName).clone();
		String zonename = myZMI.getName();
		if (validateClientCert(clientCert)) {
			AccessControlCert acc = myZMI.getAcc();
			AttributesMap am = myZMI.getAttributes();
			Iterator<Entry<Attribute, Value>> it = am.iterator();
			while (it.hasNext()) {
				Entry<Attribute, Value> entry = it.next();
				String attr_name = entry.getKey().getName();
				
				if (!clientCert.canRead(attr_name)	|| !acc.canRead(zonename, clientCert.getCAname(), attr_name)) {
					it.remove();
				}
			}
			return myZMI;
		}
		
		return null;
	}
}
