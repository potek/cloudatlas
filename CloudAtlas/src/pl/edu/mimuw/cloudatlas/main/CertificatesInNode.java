package pl.edu.mimuw.cloudatlas.main;

import java.security.PrivateKey;
import java.security.PublicKey;

import pl.edu.mimuw.cloudatlas.certification.AccessControlCert;
import pl.edu.mimuw.cloudatlas.certification.ZoneCert;

public class CertificatesInNode {
	
	// Node's name e.g. /uw/violet07/ or /pjwstk/
	String nameOfNode;
	String nameOfCA;
	
	// CA attributes
	PublicKey CApublicKey;
	
	// zone attributes
	ZoneCert zoneCertificate;
	
	// ZMI attributes
	PrivateKey zmiPrivateKey;
	PublicKey zmiPublicKey;
	
	// ACC certificate
	AccessControlCert accessControlCert;
	
	public CertificatesInNode(String nameNode, String nameCA, 
			PublicKey CApublicKey, ZoneCert zoneCertificate, AccessControlCert accessControlCert, 
			PrivateKey zmiPrivateKey, PublicKey zmiPublicKey){
		this.nameOfNode = nameNode;
		this.nameOfCA = nameCA;
		this.accessControlCert = accessControlCert;
		this.CApublicKey = CApublicKey;
		this.zmiPrivateKey = zmiPrivateKey;
		this.zmiPublicKey = zmiPublicKey;
		this.zoneCertificate = zoneCertificate;
	}
	
	public String getNameOfCA(){
		return nameOfCA;
	}
	
	// CA certification methods	
	public PublicKey getCApublicKey(){
		return CApublicKey;
	}
	
	// Zone certification methods
	public ZoneCert getZoneCertificate(){
		return zoneCertificate;
	}
	
	// ACC methods
	public AccessControlCert getAccessControlCertificate(){
		return accessControlCert;
	}
	
	// ZMI certification methods	
//	public PublicKey getZmiPublicKey(){
//		return zmiPublicKey;
//	}
	
	public PrivateKey getPrivateKey(){
		return zmiPrivateKey;
	}
	
}
