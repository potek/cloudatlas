package pl.edu.mimuw.cloudatlas.main;

import java.rmi.RemoteException;
import java.util.Set;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

import pl.edu.mimuw.cloudatlas.certification.AFCert;
import pl.edu.mimuw.cloudatlas.certification.ClientCert;
import pl.edu.mimuw.cloudatlas.message.exportinfo.MessageAnswerGetKnownZones;
import pl.edu.mimuw.cloudatlas.message.exportinfo.MessageAnswerGetZMI;
import pl.edu.mimuw.cloudatlas.message.exportinfo.MessageAnswerInstallQuery;
import pl.edu.mimuw.cloudatlas.message.exportinfo.MessageAnswerUninstallQuery;
import pl.edu.mimuw.cloudatlas.message.exportinfo.MessageExInfoAddAttribute;
import pl.edu.mimuw.cloudatlas.message.exportinfo.MessageExInfoGetKnownZones;
import pl.edu.mimuw.cloudatlas.message.exportinfo.MessageExInfoGetZMI;
import pl.edu.mimuw.cloudatlas.message.exportinfo.MessageExInfoInstallQuery;
import pl.edu.mimuw.cloudatlas.message.exportinfo.MessageExInfoSetFallbackContacts;
import pl.edu.mimuw.cloudatlas.message.exportinfo.MessageExInfoUninstallQuery;
import pl.edu.mimuw.cloudatlas.message.exportinfo.MessageExportInfo;
import pl.edu.mimuw.cloudatlas.message.exportinfo.MessageExportInfoAnswer;
import pl.edu.mimuw.cloudatlas.model.Value;
import pl.edu.mimuw.cloudatlas.model.ValueContact;
import pl.edu.mimuw.cloudatlas.model.ZMI;
import pl.edu.mimuw.cloudatlas.module.ModuleNotFoundException;

public class FetchDataComputer implements FetchDataInterface {
	private final static Logger LOGGER = Logger
			.getLogger(FetchDataComputer.class.getName());
	
	Agent myAgent;
	LinkedBlockingQueue<MessageExportInfoAnswer> queueToAnswer = new LinkedBlockingQueue<MessageExportInfoAnswer>();

	FetchDataComputer(Agent a) {
		LOGGER.setLevel(Level.ALL);
		this.myAgent = a;
	}

	@Override
	public void setFallbackContacts(Set<ValueContact> contacts, ClientCert cc)
			throws RemoteException {
		//LOGGER.log(Level.INFO, "FetchDataComputer setFallbackContacts "	+ contacts.toString());
		MessageExportInfo msg = new MessageExInfoSetFallbackContacts(
				queueToAnswer, contacts, cc);
		try {
			myAgent.messageToExportInfoModule(msg);
		} catch (ModuleNotFoundException e) {
			LOGGER.log(Level.WARNING, "Cannot set fallback contacts - module not found");
		}

		synchronized (queueToAnswer) {
			while (queueToAnswer.isEmpty()) {
				try {
					queueToAnswer.wait();
				} catch (InterruptedException e1) {
					LOGGER.log(Level.WARNING, "Interupt Exception - set fallback contacts");
				}
			}
			try {
				queueToAnswer.poll();
			} catch (Exception e) {
				LOGGER.log(Level.WARNING, "Exception - set fallback contacts - poll");
			}
		}
	}

	@Override
	public ZMI getZoneZMI(String zoneName, ClientCert cc) throws RemoteException {
		//LOGGER.log(Level.INFO, "FetchDataComputer getZoneAttributes " + zoneName);
		MessageExportInfo msg = new MessageExInfoGetZMI(queueToAnswer, zoneName, cc);
		try {
			myAgent.messageToExportInfoModule(msg);
		} catch (ModuleNotFoundException e) {
			LOGGER.log(Level.SEVERE, "Module Export Info not found", e);
		}

		synchronized (queueToAnswer) {
			while (queueToAnswer.isEmpty()) {
				try {
					queueToAnswer.wait();
				} catch (InterruptedException e1) {
					LOGGER.log(Level.WARNING, "Interupt Exception - get zone zmi");
				}
			}
			try {
				MessageAnswerGetZMI answer = (MessageAnswerGetZMI) queueToAnswer
						.poll();
				return answer.getZMI();

			} catch (Exception e) {
				LOGGER.log(Level.WARNING, "Exception - getZoneZMI - poll");
			}
		}
		return null;
	}

	@Override
	public void setAttributes(String attrName, Value attrVal, ClientCert cc) {
//		LOGGER.log(Level.INFO, "FetchDataComputer setAttributes " + attrName + " "
//				+ attrVal.toString());
		
		MessageExportInfo msg = new MessageExInfoAddAttribute(queueToAnswer,
				attrName, attrVal, cc);
		try {
			myAgent.messageToExportInfoModule(msg);
		} catch (ModuleNotFoundException e) {
			LOGGER.log(Level.SEVERE, "Module Export Info not found", e);
		}

		synchronized (queueToAnswer) {
			while (queueToAnswer.isEmpty()) {
				try {
					queueToAnswer.wait();
				} catch (InterruptedException e1) {
					LOGGER.log(Level.WARNING, "Interupt Exception - setAttributes");
				}
			}
			try {
				queueToAnswer.poll();

			} catch (Exception e) {
				LOGGER.log(Level.WARNING, "Exception - setAttributes - poll");
			}
		}
	}

	@Override
	public Set<String> getKnownZones(ClientCert cc) throws RemoteException {
		//LOGGER.log(Level.INFO, "FetchDataComputer getKnownZones");
		MessageExportInfo msg = new MessageExInfoGetKnownZones(queueToAnswer, cc);
		try {
			myAgent.messageToExportInfoModule(msg);
		} catch (ModuleNotFoundException e) {
			LOGGER.log(Level.SEVERE, "Module Export Info not found", e);
		}

		synchronized (queueToAnswer) {
			while (queueToAnswer.isEmpty()) {
				try {
					queueToAnswer.wait();
				} catch (InterruptedException e1) {
					LOGGER.log(Level.WARNING, "Interupt Exception - getKnownZones");
				}
			}
			try {
				MessageAnswerGetKnownZones answer = (MessageAnswerGetKnownZones) queueToAnswer
						.poll();
				return answer.getKnownZones();
				// Process answer
			} catch (Exception e) {
				LOGGER.log(Level.WARNING, "Exception - getKnownZones - poll");
			}
		}
		return null;
	}

	@Override
	public Boolean installQuery(AFCert afc, String zoneName) {
		//LOGGER.log(Level.INFO, "FetchDataComputer installQuery ");
		MessageExportInfo msg = new MessageExInfoInstallQuery(queueToAnswer, afc, zoneName);
		try {
			myAgent.messageToExportInfoModule(msg);
		} catch (ModuleNotFoundException e) {
			LOGGER.log(Level.SEVERE, "Module Export Info not found", e);
		}

		synchronized (queueToAnswer) {
			while (queueToAnswer.isEmpty()) {
				try {
					queueToAnswer.wait();
				} catch (InterruptedException e1) {
					LOGGER.log(Level.WARNING, "Interupt Exception - installQuery");
				}
			}
			try {
				MessageAnswerInstallQuery answer = (MessageAnswerInstallQuery) queueToAnswer
						.poll();
				return answer.getResult();
				// Process answer
			} catch (Exception e) {
				LOGGER.log(Level.WARNING, "Exception - installQuery - poll");
			}
		}
		return false;
	}

	@Override
	public Boolean uninstallQuery(AFCert afc) throws RemoteException {
		//LOGGER.log(Level.INFO, "FetchDataComputer UninstallQuery ");
		MessageExportInfo msg = new MessageExInfoUninstallQuery(queueToAnswer, afc);
		try {
			myAgent.messageToExportInfoModule(msg);
		} catch (ModuleNotFoundException e) {
			LOGGER.log(Level.SEVERE, "Module Export Info not found", e);
		}

		synchronized (queueToAnswer) {
			while (queueToAnswer.isEmpty()) {
				try {
					queueToAnswer.wait();
				} catch (InterruptedException e1) {
					LOGGER.log(Level.WARNING, "Interupt Exception - uninstallQuery");
				}
			}
			try {
				MessageAnswerUninstallQuery answer = (MessageAnswerUninstallQuery) queueToAnswer
						.poll();
				return answer.getResult();
				// Process answer
			} catch (Exception e) {
				LOGGER.log(Level.WARNING, "Exception - uninsallQuery - poll");
			}
		}
		return false;
	}

}
