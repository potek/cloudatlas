package pl.edu.mimuw.cloudatlas.main;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.InflaterInputStream;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;

import pl.edu.mimuw.cloudatlas.certification.AccessControlCert;
import pl.edu.mimuw.cloudatlas.certification.ZoneCert;
import pl.edu.mimuw.cloudatlas.certification.ZoneCertSerializer;
import pl.edu.mimuw.cloudatlas.common.CryptoManager;
import pl.edu.mimuw.cloudatlas.model.PathName;
import pl.edu.mimuw.cloudatlas.model.TypePrimitive;
import pl.edu.mimuw.cloudatlas.model.ValueContact;
import pl.edu.mimuw.cloudatlas.model.ValueInt;
import pl.edu.mimuw.cloudatlas.model.ValueSet;
import pl.edu.mimuw.cloudatlas.model.ValueString;
import pl.edu.mimuw.cloudatlas.model.ValueTime;
import pl.edu.mimuw.cloudatlas.model.ZMI;
import pl.edu.mimuw.cloudatlas.module.gossiper.Strategy.StrategyType;


public class Main {
	private final static Logger LOGGER = Logger
			.getLogger(Main.class.getName());
	
	public static void main(String[] args) {
		
		LOGGER.setLevel(Level.ALL);
		if (System.getSecurityManager() == null) {
			System.setSecurityManager(new SecurityManager());
		}

		Agent myAgent = null;
		try {
			myAgent = createAgent(args[0]);

			FetchDataComputer object = new FetchDataComputer(myAgent);
			FetchDataInterface stub = (FetchDataInterface) UnicastRemoteObject
					.exportObject(object, 0);
			Registry registry = LocateRegistry.getRegistry();
			registry.rebind("FetchData:"
					+ myAgent.getMachineZMI().getName(), stub);
			LOGGER.log(Level.INFO, "FetchData bound"
					+ "FetchData:"
					+ myAgent.getMachineZMI().getName());
		} catch (UnknownHostException e) {
			LOGGER.log(Level.SEVERE, "Cannot add agent: unknown host", e);
		} catch (NoSuchAlgorithmException e) {
			LOGGER.log(Level.SEVERE, "Cannot add agent: no such algorithm", e);
		} catch (InvalidKeySpecException e) {
			LOGGER.log(Level.SEVERE, "Cannot add agent: invalid key", e);
		} catch (IOException e) {
			LOGGER.log(Level.SEVERE, "Cannot add agent: IO Exception", e);
		}
		myAgent.start();
	}

	public static String getPropOrThrow(Properties prop, String name)
			throws IOException {
		if (prop.containsKey(name)) {
			String val = prop.getProperty(name); 
			if (val.trim() == "") {
				throw new IOException("ERROR: bad config file. " + name
						+ " is empty");
			}
			return val;
		} else {
			throw new IOException("ERROR: bad config file. No " + name
					+ " property in conf file"); 
		}
	}

	public static String getPropOrNull(Properties prop, String name) {
		if (prop.containsKey(name)) {
			String val = prop.getProperty(name); 
			if (val.trim() == "") {
				return null;
			}
			return val;
		} else {
			return null;
		}
	}

	public static ValueSet parseContacts(String contacts_str) {
		ValueSet vs = new ValueSet(TypePrimitive.CONTACT);
		if (contacts_str != null) {
			for (String cont : contacts_str.split(",")) {
				String[] c = cont.split(":");
				//String[] ip = c[1].split("\\.");
				try {
					ValueContact vc = createContact(c[0], c[1], Integer.parseInt(c[2]));
					vs.add(vc);
				} catch (Exception e) {
					LOGGER.log(Level.WARNING, "Cannot parse contact");
				}
			}
		}
		return vs;
	}

	public static Agent createAgent(String filename) throws IOException,
			NoSuchAlgorithmException, InvalidKeySpecException {
		Properties prop = new Properties();
		AgentConfiguration conf = new AgentConfiguration();
		InputStream input = null;

		try {
			input = new FileInputStream(filename);

			prop.load(input);

			String agentName = getPropOrThrow(prop, "name");
			String agentOwner = getPropOrThrow(prop, "owner");
			ValueSet contacts = parseContacts(getPropOrNull(prop, "contacts"));
			ValueSet members = parseContacts(getPropOrNull(prop, "members"));

			String agentHost = getPropOrThrow(prop, "host");
			int agentPort = Integer.parseInt(getPropOrThrow(prop, "port"));

			String publicKey_str = getPropOrThrow(prop, "pub-key");
			conf.publicKey = CryptoManager.getPublicKey(publicKey_str);

			String firstGossipStr = getPropOrNull(prop, "first-gossip-time");
			if (firstGossipStr != null) {
				conf.FIRST_GOSSIP_AFTER_TIME = TimeUnit.SECONDS.toMillis(Long
						.parseLong(firstGossipStr));
			}

			String gossipPeriodMinStr = getPropOrNull(prop, "gossip-period-min");
			if (gossipPeriodMinStr != null) {
				conf.GOSSIP_PERIOD_BEGIN = TimeUnit.SECONDS.toMillis(Long
						.parseLong(gossipPeriodMinStr));
			}

			String gossipPeriodMaxStr = getPropOrNull(prop, "gossip-period-max");
			if (gossipPeriodMaxStr != null) {
				conf.GOSSIP_PERIOD_END = TimeUnit.SECONDS.toMillis(Long
						.parseLong(gossipPeriodMaxStr));
			}

			String queryExecutePeriodStr = getPropOrNull(prop,
					"query-execute-period");
			if (queryExecutePeriodStr != null) {
				conf.QUERY_EXECUTE_PERIOD = TimeUnit.SECONDS.toMillis(Long
						.parseLong(queryExecutePeriodStr));
			}

			String strategyType = getPropOrNull(prop, "strategy");
			if (strategyType != null) {
				switch (strategyType) {
				case "RB_NORMAL":
					conf.TYPE_OF_STRATEGY = StrategyType.ROUND_ROBIN_NORMAL;
					break;
				case "RB_DEC":
					conf.TYPE_OF_STRATEGY = StrategyType.ROUND_ROBIN_DECREASING;
					break;
				case "RAND_NORMAL":
					conf.TYPE_OF_STRATEGY = StrategyType.RANDOM_NORMAL;
					break;
				case "RAND_DEC":
					conf.TYPE_OF_STRATEGY = StrategyType.ROUND_ROBIN_DECREASING;
					break;
				default:
					break;
				}
			}

			ArrayList<Pair<ZMI, ArrayList<ZMI>>> hierarchyTree = new ArrayList<Pair<ZMI, ArrayList<ZMI>>>();

			String[] hierarchy_str = agentName.split("/");
			String curName = "/";

			ZMI root = new ZMI();
			root.getAttributes().add("level", new ValueInt(0L));
			root.getAttributes().add("name", new ValueString(curName));
			root.getAttributes().add("owner", new ValueString(agentOwner));
			root.getAttributes().add("timestamp",
					new ValueTime(0L));

			root.getAttributes().add("contacts",
					new ValueSet(TypePrimitive.CONTACT));
			hierarchyTree.add(new Pair<ZMI, ArrayList<ZMI>>(root,
					new ArrayList<ZMI>()));

			ZMI parent = root;
			for (int i = 1; i < hierarchy_str.length; i++) {
				ZMI son = new ZMI();
				curName += hierarchy_str[i] + "/";

				son.getAttributes().add("level", new ValueInt(Long.valueOf(i)));
				son.getAttributes().add("name", new ValueString(curName));
				son.getAttributes().add("owner", new ValueString(agentOwner));
				son.getAttributes().add("contacts", contacts);
				son.getAttributes().add("timestamp", new ValueTime(0L));

				hierarchyTree.add(new Pair<ZMI, ArrayList<ZMI>>(son,
						new ArrayList<ZMI>()));
				parent = son;
			}

			parent.getAttributes().add("members", members);
			
			ArrayList<CertificatesInNode> certificates = new ArrayList<CertificatesInNode>();
			
			String CApath, zonePath, CAname;
			curName = "/";
			for(int i=1; i<hierarchy_str.length; i++){
				curName += hierarchy_str[i] + "/";
				CApath = getPropOrThrow(prop, ("CA-level"+i+"-path"));
				zonePath = CApath + "zones" + curName;
				PublicKey CApublicKey = CryptoManager.getPublicKey(CApath+"public_key.der");
				PublicKey zmiPublicKey = CryptoManager.getPublicKey(zonePath+"public_key.der");
				PrivateKey zmiPrivateKey = CryptoManager.getPrivateKey(zonePath+"private_key.der");
				
				File fileWithCert = new File(zonePath, "cert_zone.crt");
				Input in = new Input(new InflaterInputStream(new FileInputStream(fileWithCert)));
				Kryo kryo = new Kryo();
				kryo.register(ZoneCert.class, new ZoneCertSerializer(), 10);
				ZoneCert zoneCert = kryo.readObject(in, ZoneCert.class);
				
				File fileWithACC = new File(zonePath, "cert_access_control.crt");
				in = new Input(new InflaterInputStream(new FileInputStream(fileWithACC)));
				kryo = new Kryo();
//				
				AccessControlCert acc = kryo.readObject(in, AccessControlCert.class);
				
				hierarchyTree.get(i).getFirst().setACC(acc);
				
				CAname = zoneCert.getCAname();
				CertificatesInNode cert = new CertificatesInNode(curName, CAname, CApublicKey, 
						zoneCert, acc, zmiPrivateKey, zmiPublicKey);
				certificates.add(cert);
			}
			
			CApath = getPropOrThrow(prop, ("CA-level0-path"));
			File fileWithACC = new File(CApath+"zones/", "cert_access_control.crt");
			Input in = new Input(new InflaterInputStream(new FileInputStream(fileWithACC)));
			Kryo kryo = new Kryo();
			AccessControlCert acc = kryo.readObject(in, AccessControlCert.class);
			
			hierarchyTree.get(0).getFirst().setACC(acc);
			
			return new Agent(hierarchyTree, agentHost, agentPort, conf, certificates);
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					LOGGER.log(Level.WARNING, "Input-Output Exception");
				}
			}
		}
	}
	
//	private static String findCAname(String pathToCA){
//		String[] elements = pathToCA.split("/");
//		for(String str : elements){
//			if(str.length() > 3){
//				if(str.charAt(0) == 'C' && str.charAt(1) == 'A' && str.charAt(2) == '_'){
//					return str;
//				}
//			}			
//		}	
//		return null;
//	}

//	private static ValueContact createContact(String path, byte ip1, byte ip2,
//			byte ip3, byte ip4, int port) throws UnknownHostException {
//		return new ValueContact(new PathName(path),
//				InetAddress.getByAddress(new byte[] { ip1, ip2, ip3, ip4 }),
//				port);
//	}
	
	private static ValueContact createContact(String path, String host, 
			int port) throws UnknownHostException {
		return new ValueContact(new PathName(path), InetAddress.getByName(host), port);
	}
	
	
}