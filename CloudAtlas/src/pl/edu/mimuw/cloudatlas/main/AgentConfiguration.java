package pl.edu.mimuw.cloudatlas.main;

import java.security.PublicKey;

import pl.edu.mimuw.cloudatlas.module.gossiper.Strategy.StrategyType;

public class AgentConfiguration {
	public long FIRST_GOSSIP_AFTER_TIME = 5000L;
	public long GOSSIP_PERIOD_BEGIN = 5000L;
	public long GOSSIP_PERIOD_END = 20000L;
	public long QUERY_EXECUTE_PERIOD = 5000L;
	public StrategyType TYPE_OF_STRATEGY = StrategyType.RANDOM_NORMAL;
	public PublicKey publicKey;
	public String nameCA;
}
