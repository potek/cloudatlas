package pl.edu.mimuw.cloudatlas.main;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Set;

import pl.edu.mimuw.cloudatlas.certification.AFCert;
import pl.edu.mimuw.cloudatlas.certification.ClientCert;
import pl.edu.mimuw.cloudatlas.model.Value;
import pl.edu.mimuw.cloudatlas.model.ValueContact;
import pl.edu.mimuw.cloudatlas.model.ZMI;

public interface FetchDataInterface extends Remote {
//	public void exportLong(String name, Long val) throws RemoteException;
//	
//	public void exportDouble(String name, Double val) throws RemoteException;
//
//	public void exportString(String name, String val) throws RemoteException;
//	
//	public void exportStringSet(String name, Set<String> val) throws RemoteException;
	
	/**
	 * Setting the fallback contacts. These contacts are stored aside from the ZMIs, 
	 * in a dedicated set. Each invocation of the function overrides this set.
	 * @param contacts
	 */
	public void setFallbackContacts(Set<ValueContact> contacts, ClientCert cc) throws RemoteException;
	
	/**
	 * Returning the values of attributes of a given zone.
	 * @param zoneName
	 * @return
	 * @throws RemoteException
	 */
	public ZMI getZoneZMI(String zoneName, ClientCert cc) throws RemoteException;
	
	/**
	 * Setting the values of attributes of a given zone
	 * (this operation should be allowed only for the singleton zones).
	 * @param zoneName
	 * @throws RemoteException
	 */
	public void setAttributes(String attributeName, Value attributeVal, ClientCert cc) throws RemoteException;
	
	/**
	 * 
	 */
	public Set<String> getKnownZones(ClientCert cc) throws RemoteException;
	
	public Boolean installQuery(AFCert afc, String zoneName) throws RemoteException;
	
	public Boolean uninstallQuery(AFCert afc) throws RemoteException;
}