//package pl.edu.mimuw.cloudatlas.client;
//
//import java.net.InetAddress;
//import java.net.UnknownHostException;
//import java.rmi.NotBoundException;
//import java.rmi.RemoteException;
//import java.rmi.registry.LocateRegistry;
//import java.rmi.registry.Registry;
//import java.text.ParseException;
//import java.util.Arrays;
//import java.util.HashSet;
//import java.util.List;
//
//import pl.edu.mimuw.cloudatlas.main.FetchDataInterface;
//import pl.edu.mimuw.cloudatlas.model.PathName;
//import pl.edu.mimuw.cloudatlas.model.TypePrimitive;
//import pl.edu.mimuw.cloudatlas.model.Value;
//import pl.edu.mimuw.cloudatlas.model.ValueBoolean;
//import pl.edu.mimuw.cloudatlas.model.ValueContact;
//import pl.edu.mimuw.cloudatlas.model.ValueDuration;
//import pl.edu.mimuw.cloudatlas.model.ValueInt;
//import pl.edu.mimuw.cloudatlas.model.ValueList;
//import pl.edu.mimuw.cloudatlas.model.ValueSet;
//import pl.edu.mimuw.cloudatlas.model.ValueString;
//import pl.edu.mimuw.cloudatlas.model.ValueTime;
//
//public class InitViolet07 {
//
//	private static ValueContact createContact(String path, byte ip1, byte ip2,
//			byte ip3, byte ip4, int port) throws UnknownHostException {
//		return new ValueContact(new PathName(path),
//				InetAddress.getByAddress(new byte[] { ip1, ip2, ip3, ip4 }),
//				port);
//	}
//
//	public static void main(String[] args) {
//
//		Registry registry = null;
//		FetchDataInterface stub = null;
//
//		try {
//			registry = LocateRegistry.getRegistry(args[0]);
//			stub = (FetchDataInterface) registry
//					.lookup("FetchData:/uw/violet07/");
//
//			ValueContact khaki31Contact = createContact("/uw/khaki31/",
//					(byte) 127, (byte) 0, (byte) 0, (byte) 1, 35001);
//			ValueContact whatever01Contact = createContact(
//					"/pjwstk/whatever01/", (byte) 127, (byte) 0, (byte) 0,
//					(byte) 1, 42000);
//
//			List<Value> list = Arrays.asList(new Value[] { khaki31Contact,
//					whatever01Contact });
//
//			stub.setAttributes("contacts", new ValueSet(
//					new HashSet<Value>(list), TypePrimitive.CONTACT));
//			stub.setAttributes("cardinality", new ValueInt(1l));
//
//			stub.setAttributes("creation", new ValueTime(
//					"2011/11/09 20:8:13.123"));
//			stub.setAttributes("has_ups", new ValueBoolean(null));
//			list = Arrays.asList(new Value[] { new ValueString("tola"),
//					new ValueString("tosia"), });
//			stub.setAttributes("some_names", new ValueList(list,
//					TypePrimitive.STRING));
//			stub.setAttributes("expiry",
//					new ValueDuration(13l, 12l, 0l, 0l, 0l));
//
//		} catch (RemoteException e1) {
//			e1.printStackTrace();
//		} catch (NotBoundException e) {
//			e.printStackTrace();
//		} catch (UnknownHostException e) {
//			e.printStackTrace();
//		} catch (ParseException e) {
//			e.printStackTrace();
//		}
//	}
//}
