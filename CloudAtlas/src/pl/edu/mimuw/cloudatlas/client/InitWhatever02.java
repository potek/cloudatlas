//package pl.edu.mimuw.cloudatlas.client;
//
//import java.rmi.NotBoundException;
//import java.rmi.RemoteException;
//import java.rmi.registry.LocateRegistry;
//import java.rmi.registry.Registry;
//import java.text.ParseException;
//import java.util.Arrays;
//import java.util.List;
//
//import pl.edu.mimuw.cloudatlas.main.FetchDataInterface;
//import pl.edu.mimuw.cloudatlas.model.TypePrimitive;
//import pl.edu.mimuw.cloudatlas.model.Value;
//import pl.edu.mimuw.cloudatlas.model.ValueInt;
//import pl.edu.mimuw.cloudatlas.model.ValueList;
//import pl.edu.mimuw.cloudatlas.model.ValueString;
//import pl.edu.mimuw.cloudatlas.model.ValueTime;
//
//public class InitWhatever02 {
//	public static void main(String[] args) {
//
//		Registry registry = null;
//		FetchDataInterface stub = null;
//
//		try {
//			registry = LocateRegistry.getRegistry(args[0]);
//			stub = (FetchDataInterface) registry
//					.lookup("FetchData:/pjwstk/whatever02/");
//
//			stub.setAttributes("cardinality", new ValueInt(1L));
//			stub.setAttributes("creation", new ValueTime("2012/10/18 07:04:00.000"));
//			
//			List<Value> list = Arrays.asList(new Value[] {
//					new ValueString("odbc")
//			 });
//			stub.setAttributes("php_modules", new ValueList(list, TypePrimitive.STRING));
//
//		} catch (RemoteException e1) {
//			e1.printStackTrace();
//		} catch (NotBoundException e) {
//			e.printStackTrace();
//		} catch (ParseException e) {
//			e.printStackTrace();
//		}
//	}
//}
