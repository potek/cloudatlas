package pl.edu.mimuw.cloudatlas.client;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.management.ManagementFactory;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.InflaterInputStream;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;

import pl.edu.mimuw.cloudatlas.certification.ClientCert;
import pl.edu.mimuw.cloudatlas.main.FetchDataInterface;
import pl.edu.mimuw.cloudatlas.model.TypePrimitive;
import pl.edu.mimuw.cloudatlas.model.ValueDouble;
import pl.edu.mimuw.cloudatlas.model.ValueInt;
import pl.edu.mimuw.cloudatlas.model.ValueSet;
import pl.edu.mimuw.cloudatlas.model.ValueString;

public class DataSender {
	private final static Logger LOGGER = Logger.getLogger(DataSender.class
			.getName());

	static final String[] total_ram_cmd = { "/bin/sh", "-c",
			"grep MemTotal /proc/meminfo | awk \'{print $2}\'" };

	static final String[] proc_num_cmd = { "/bin/sh", "-c", "ps aux | wc -l" };

	static final String[] free_ram_cmd = { "/bin/sh", "-c",
			"grep MemFree /proc/meminfo | awk \'{print $2}\'" };

	static final String[] free_swap_cmd = { "/bin/sh", "-c",
			"grep SwapFree /proc/meminfo | awk \'{print $2}\'" };

	static final String[] total_swap_cmd = { "/bin/sh", "-c",
			"grep SwapTotal /proc/meminfo | awk \'{print $2}\'" };

	static final String[] num_processes_cmd = { "/bin/sh", "-c",
			"ps ax | wc -l " };

	static final String[] cpu_load_cmd = { "/bin/sh", "-c",
			"uptime | awk '{print $9}' | sed 's/.$//' | tr \",\" \".\"" };

	static final String[] num_cores_cmd = { "/bin/sh", "-c",
			"grep -c ^processor /proc/cpuinfo" };

	static final String[] kernel_ver_cmd = { "/bin/sh", "-c", "uname -r" };

	static final String[] logged_users_cmd = { "/bin/sh", "-c", "users | wc -w" };

	static final String[] dns_names_cmd = { "/bin/sh", "-c", "hostname -A" };

	public static String readBashScript(String[] cmd) {
		try {
			Process proc = Runtime.getRuntime().exec(cmd); // Whatever you want
															// to execute
			BufferedReader read = new BufferedReader(new InputStreamReader(
					proc.getInputStream()));
			try {
				proc.waitFor();
			} catch (InterruptedException e) {
				LOGGER.log(Level.SEVERE,
						"Cannot execute command " + Arrays.toString(cmd), e);
			}
			if (read.ready()) {
				return read.readLine();
			} else {
				throw new IOException("Error: Cannot read result of command: "
						+ cmd);
			}

		} catch (IOException e) {
			LOGGER.log(Level.SEVERE,
					"Cannot execute command " + Arrays.toString(cmd), e);
		}
		return "";
	}

	public static void main(String[] args) {
		LOGGER.setLevel(Level.ALL);
		Registry registry = null;
		FetchDataInterface stub = null;
		
		try {
			registry = LocateRegistry.getRegistry(args[0]);
			stub = (FetchDataInterface) registry.lookup("FetchData:" + args[1]);

			Path clientCertPath = Paths.get(args[2] + "/clients" + args[1] + "/DataSender");
			File fileWithCert = new File(clientCertPath.toString(), "cert_client.crt");
			
			System.out.println(fileWithCert.getAbsolutePath());
			Kryo kryo = new Kryo();
			Input in = new Input(
					new InflaterInputStream(new FileInputStream(fileWithCert)));
			//kryo.register(ClientCert.class);
			
			ClientCert clientCert = (ClientCert)kryo.readClassAndObject(in);
			
			System.out.println(clientCert);
			
			
			while (true) {
				// cpu_load 1min // also possible 5min and 15min -> change $8 to
				// $9 and $10 in cpu_load_cmd
				java.lang.management.OperatingSystemMXBean osmxb = ManagementFactory
						.getOperatingSystemMXBean();
				double cpu_load = osmxb.getSystemLoadAverage();
				// double cpu_load =
				// Double.parseDouble(readBashScript(cpu_load_cmd));

				// free_disk
				long free_disk = Runtime.getRuntime().freeMemory();

				// total_disk
				long total_disk = Runtime.getRuntime().totalMemory();

				// free_ram
				long free_ram = Long.parseLong(readBashScript(free_ram_cmd));

				// total_ram
				long total_ram = Long.parseLong(readBashScript(total_ram_cmd));

				// free_swap
				long free_swap = Long.parseLong(readBashScript(free_swap_cmd));

				// total_swap
				long total_swap = Long
						.parseLong(readBashScript(total_swap_cmd));

				// num_cores
				long num_cores = Long.parseLong(readBashScript(num_cores_cmd));

				// proc_num
				long proc_num = Long.parseLong(readBashScript(proc_num_cmd));

				// kernel_ver
				String kernel_ver = readBashScript(kernel_ver_cmd);

				// logged_users
				long logged_users = Long
						.parseLong(readBashScript(logged_users_cmd));

				// dns_names
				String[] dns_names_temp = readBashScript(dns_names_cmd).split(
						"\\s+");

				int size_limit = Math.min(dns_names_temp.length, 3);
				ValueSet dns_names = new ValueSet(TypePrimitive.STRING);
				for (int i = 0; i < size_limit; i++) {
					dns_names.add(new ValueString(dns_names_temp[i]));
				}

				try {
					stub.setAttributes("cpu_load", new ValueDouble(cpu_load), clientCert);

					stub.setAttributes("free_disk", new ValueInt(free_disk), clientCert);

					stub.setAttributes("total_disk", new ValueInt(total_disk), clientCert);

					stub.setAttributes("free_ram", new ValueInt(free_ram), clientCert);

					stub.setAttributes("total_ram", new ValueInt(total_ram), clientCert);

					stub.setAttributes("free_swap", new ValueInt(free_swap), clientCert);

					stub.setAttributes("total_swap", new ValueInt(total_swap), clientCert);

					stub.setAttributes("num_cores", new ValueInt(num_cores), clientCert);

					stub.setAttributes("proc_num", new ValueInt(proc_num), clientCert);

					stub.setAttributes("kernel_ver",
							new ValueString(kernel_ver), clientCert);

					stub.setAttributes("logged_users", new ValueInt(
							logged_users), clientCert);

					stub.setAttributes("dns_names", dns_names, clientCert);

					LOGGER.log(Level.INFO, "Data Sent");
				} catch (RemoteException e) {
					LOGGER.log(Level.SEVERE, "Cannot send data", e);
				}

				try {
					Thread.sleep(2000);
				} catch (InterruptedException e) {
					LOGGER.log(Level.WARNING, "Interupt Exception in main");
				}
			}
		} catch (Exception e) {
			LOGGER.log(Level.WARNING, "in main Exception");
		} 

	}
}
