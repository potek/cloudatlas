//package pl.edu.mimuw.cloudatlas.client;
//
//import java.net.InetAddress;
//import java.net.UnknownHostException;
//import java.rmi.NotBoundException;
//import java.rmi.RemoteException;
//import java.rmi.registry.LocateRegistry;
//import java.rmi.registry.Registry;
//import java.text.ParseException;
//import java.util.Arrays;
//import java.util.HashSet;
//import java.util.List;
//
//import pl.edu.mimuw.cloudatlas.main.FetchDataInterface;
//import pl.edu.mimuw.cloudatlas.model.PathName;
//import pl.edu.mimuw.cloudatlas.model.TypePrimitive;
//import pl.edu.mimuw.cloudatlas.model.Value;
//import pl.edu.mimuw.cloudatlas.model.ValueBoolean;
//import pl.edu.mimuw.cloudatlas.model.ValueContact;
//import pl.edu.mimuw.cloudatlas.model.ValueDuration;
//import pl.edu.mimuw.cloudatlas.model.ValueInt;
//import pl.edu.mimuw.cloudatlas.model.ValueList;
//import pl.edu.mimuw.cloudatlas.model.ValueSet;
//import pl.edu.mimuw.cloudatlas.model.ValueString;
//import pl.edu.mimuw.cloudatlas.model.ValueTime;
//
//public class InitKhaki31 {
//
//	private static ValueContact createContact(String path, byte ip1, byte ip2,
//			byte ip3, byte ip4, int port) throws UnknownHostException {
//		return new ValueContact(new PathName(path),
//				InetAddress.getByAddress(new byte[] { ip1, ip2, ip3, ip4 }),
//				port);
//	}
//
//	public static void main(String[] args) {
//
//		Registry registry = null;
//		FetchDataInterface stub = null;
//
//		try {
//			registry = LocateRegistry.getRegistry(args[0]);
//			stub = (FetchDataInterface) registry
//					.lookup("FetchData:/uw/khaki31/");
//
//			ValueContact violet07Contact = createContact("/uw/violet07/",
//					(byte) 127, (byte) 0, (byte) 0, (byte) 1, 35000);
//			ValueContact whatever02Contact = createContact(
//					"/pjwstk/whatever02/", (byte) 127, (byte) 0, (byte) 0,
//					(byte) 1, 43000);
//
//			List<Value> list = Arrays.asList(new Value[] { violet07Contact,
//					whatever02Contact });
//			stub.setAttributes("contacts", new ValueSet(
//					new HashSet<Value>(list), TypePrimitive.CONTACT));
//			stub.setAttributes("cardinality", new ValueInt(1L));
//			stub.setAttributes("creation", new ValueTime(
//					"2011/11/09 20:12:13.123"));
//			stub.setAttributes("has_ups", new ValueBoolean(false));
//
//			list = Arrays.asList(new Value[] { new ValueString("agatka"),
//					new ValueString("beatka"), new ValueString("celina"), });
//			stub.setAttributes("some_names", new ValueList(list,
//					TypePrimitive.STRING));
//
//			stub.setAttributes("expiry", new ValueDuration(-13l, -11l, 0l, 0l,
//					0l));
//
//		} catch (RemoteException e1) {
//			e1.printStackTrace();
//		} catch (NotBoundException e) {
//			e.printStackTrace();
//		} catch (UnknownHostException e) {
//			e.printStackTrace();
//		} catch (ParseException e) {
//			e.printStackTrace();
//		}
//	}
//}
