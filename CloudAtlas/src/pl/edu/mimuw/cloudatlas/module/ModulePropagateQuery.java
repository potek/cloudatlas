package pl.edu.mimuw.cloudatlas.module;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

import pl.edu.mimuw.cloudatlas.certification.AccessControlCert;
import pl.edu.mimuw.cloudatlas.interpreter.Query;
import pl.edu.mimuw.cloudatlas.interpreter.Query.QueryState;
import pl.edu.mimuw.cloudatlas.main.Agent;
import pl.edu.mimuw.cloudatlas.main.Pair;
import pl.edu.mimuw.cloudatlas.model.ValueInt;
import pl.edu.mimuw.cloudatlas.model.ValueQuery;
import pl.edu.mimuw.cloudatlas.model.ZMI;

import java.util.logging.Level;
import java.util.logging.Logger;

public class ModulePropagateQuery extends Module {

	private final long PROPAGATE_QUERY_PERIOD = 5000L;
	private HashMap<String, Query> queryMap;
	private final static Logger LOGGER = Logger
			.getLogger(ModulePropagateQuery.class.getName());

	private static final String[] keyWordsArray = { "min", "max", "sum", "AND",
			"OR", "NOT", "avg", "count", "first", "last", "random", "land",
			"lor", "lnot", "isNull", "now", "epoch", "size", "round", "floor",
			"ceil", "to_integer", "to_string", "to_boolean", "to_double",
			"to_list", "to_time", "to_set", "to_contact", "to_duration" };
	private static final HashSet<String> keyWords = new HashSet<String>(
			Arrays.asList(keyWordsArray));

	public ModulePropagateQuery(String name, Agent agent) {
		super(name, agent);
		this.typeOfModule = ModuleType.MODULE_PROPAGATE_QUERY;
		this.queryMap = new HashMap<String, Query>();
	}

	@Override
	public void processMessage() throws Exception {

	}

	@Override
	public void run() {
		new QueryRefresher().start();
		
		while (true) {
			try {
				Thread.sleep(PROPAGATE_QUERY_PERIOD);
			} catch (InterruptedException e) {
				LOGGER.log(Level.WARNING, "Interupt Exception");
			}

			for (Pair<ZMI, ArrayList<ZMI>> pair : agent.getHierarchyTree()) {
				findNeqQueries(pair.getFirst().getAllQueries());
				for (ZMI zmi : pair.getSecond()) {
					findNeqQueries(zmi.getAllQueries());
				}
			}

			for (Pair<ZMI, ArrayList<ZMI>> pair : agent.getHierarchyTree()) {
				installNewQueries(pair.getFirst());
				for (ZMI zmi : pair.getSecond()) {
					installNewQueries(zmi);
				}
			}

			for (Pair<ZMI, ArrayList<ZMI>> pair : agent.getHierarchyTree()) {
				List<Query> notCheckedQueries = pair.getFirst()
						.getNotCheckedYetQueries();
				if (notCheckedQueries.size() > 0)
					for (Query q : notCheckedQueries)
						countStatus(pair.getFirst(), q);
				for (ZMI zmi : pair.getSecond()) {
					notCheckedQueries = zmi.getNotCheckedYetQueries();
					if (notCheckedQueries.size() > 0)
						for (Query q : notCheckedQueries)
							countStatus(zmi, q);
				}
			}
		}
	}

	private void findNeqQueries(List<Query> queryList) {
		for (Query q : queryList) {
			if (!queryMap.containsKey(q.getName())) {
				queryMap.put(q.getName(), q);
			}
		}
	}

	private void installNewQueries(ZMI zmi) {
		for (Entry<String, Query> entry : queryMap.entrySet()) {
			if (zmi.getAttributes().getOrNull(entry.getKey()) == null) {
				Query newQuery = entry.getValue().copy();
				zmi.getAttributes().add(entry.getKey(),
						new ValueQuery(newQuery));
				agent.updateTimestampOnMyPath();
			}
		}
	}

	private void countStatus(ZMI zmi, Query q) {
		String name = zmi.getName();
		if (zmi.getAcc().certificateHasExpired()) {
			LOGGER.log(Level.INFO,
					"Lack of permission (ACC HAS EXPIRED) to install on level "
							+ name + " (" + q + ")");
			q.setQueryState(QueryState.NOT_ACCEPTED);
		} else {
			if (correctLevel(zmi, q) && enoughPermission(zmi, q)
					&& accPermission(zmi, q) && allAttributes(zmi, q) && ccPermission(zmi, q)) {
				LOGGER.log(Level.INFO, "Permission to install on level " + name
						+ " (" + q + ")");
				q.setQueryState(QueryState.INSTALLED);
			} else {
				q.setQueryState(QueryState.NOT_ACCEPTED);
			}
		}
	}

	private boolean ccPermission(ZMI zmi, Query q) {
		String zoneName = zmi.getName();
		return q.getCc().canInstallQuery(zoneName);
	}

	private boolean allAttributes(ZMI zmi, Query q) {
		LinkedList<String> write = new LinkedList<String>();
		for(String select : q.getAllSelect()) {
			LinkedList<String> read = new LinkedList<String>();
			getToReadAndWrite(select, read, write);
			for(String attr : read) {
				boolean atLeastOne = false;
				for(ZMI son : zmi.getSons(agent.getHierarchyTree())) {
					if(son.getAttributes().getOrNull(attr) != null) {
						atLeastOne = true;
						break;
					}
				}
				if(!atLeastOne) {
					LOGGER.log(Level.INFO,
							"Lack of permission (NO SONS ATTRIBUTE "+ attr +") to install on level "
									+ zmi.getName() + " (" + q + ")");
					return false;
				}
			}
		}
		return true;
	}

	private boolean accPermission(ZMI zmi, Query q) {
		String myZoneName = q.getOwner();
		String whereZoneName = zmi.getName();
		String CAname = q.getCAname();
		
		boolean tmp = zmi.getAcc().canInstallQuery(myZoneName, whereZoneName,
				CAname);
		if (!tmp) {
			LOGGER.log(Level.INFO,
					"Lack of permission (ACC FORBIDDEN) to install on level "
							+ whereZoneName + " (" + q + ")");
		}
		return tmp;
	}

	private boolean correctLevel(ZMI zmi, Query q) {
		int minLevel = q.getMinLevel();
		int maxLevel = q.getMaxLevel();
		int level = (int) (long) ((ValueInt) zmi.getAttributes().get("level"))
				.getValue();
		if (level < minLevel || level > maxLevel) {
			String name = zmi.getName();
			LOGGER.log(Level.INFO,
					"Lack of permission (LEVEL FORBIDDEN) to install on level "
							+ name + " (" + q + ")");
			return false;
		} else
			return true;
	}

	private boolean enoughPermission(ZMI zmi, Query q) {

		String[] queries = q.getAllSelect();
		LinkedList<String> attributesToRead = new LinkedList<String>();
		LinkedList<String> attributesToWrite = new LinkedList<String>();
		for (String select : queries) {
			getToReadAndWrite(select, attributesToRead, attributesToWrite);
		}
		AccessControlCert acc = zmi.getAcc();
		String zoneName = zmi.getName();
		String CAname = q.getCAname();

		for (String readAtt : attributesToRead) {
			if (!acc.canRead(zoneName, CAname, readAtt)) {
				String name = zmi.getName();
				LOGGER.log(Level.INFO,
						"Lack of permission (CANNOT READ) to install on level "
								+ name + " (" + q + ")");
				return false;
			}
		}
		for (String writeAtt : attributesToRead) {
			if (!acc.canWrite(zoneName, CAname, writeAtt)) {
				String name = zmi.getName();
				LOGGER.log(Level.INFO,
						"Lack of permission (CANNOT WRITE) to install on level "
								+ name + " (" + q + ")");
				return false;
			}
		}
		return true;
	}

	private void getToReadAndWrite(String select, LinkedList<String> read,
			LinkedList<String> write) {
		String[] noSelect = select.split("SELECT");
		String[] orderBy = noSelect[1].split("ORDER BY");
		String[] where = orderBy[0].split("WHERE");

		// System.out.println("ORDER BY: |" + orderBy[1]+"|" );
		// System.out.println("WHERE: |" + where[1]+"|");
		// System.out.println("RESZTA: |" + where[0] + "|");

		String[] attributes = where[0].split(",");

		for (String a : attributes) {
			String[] tmp = a.split("AS");
			getRead(tmp[0].trim(), read);
			if (tmp.length > 1)
				getWrite(tmp[1].trim(), write);
		}
	}

	private void getRead(String a, LinkedList<String> ll) {
		String[] tmp = a.split("\\W");
		for (String b : tmp) {
			String c = b.trim();
			if (!keyWords.contains(b.trim()) && !c.matches("-?\\d+(\\.\\d+)?") && c.length() > 0)
				ll.add(b.trim());
		}
	}

	private void getWrite(String a, LinkedList<String> ll) {
		if(a.length() > 0)
			ll.add(a.trim());
	}
	
	private class QueryRefresher extends Thread{
		
		public static final long REFRESH_PERIOD = 5000L;
		
		@Override
		public void run(){
			while(true){
				try {
					Thread.sleep(REFRESH_PERIOD);
				} catch (InterruptedException e) {
					LOGGER.log(Level.WARNING, "Interput Exception - QueryRefresher");
				}
				
				ArrayList<Pair<ZMI, ArrayList<ZMI>>> hierarchyTree = agent.getHierarchyTree();
				for(Pair<ZMI, ArrayList<ZMI>> pair : hierarchyTree){
					refreshQueryStatus(pair.getFirst());
					for(ZMI zmi: pair.getSecond()){
						refreshQueryStatus(zmi);
					}
				}
			}
			
		}
		
		private void refreshQueryStatus(ZMI zmi){
			List<Query> queries = zmi.getAllQueries();
			for(Query q: queries){
				countStatus(zmi, q);
			}
		}
	}

}
