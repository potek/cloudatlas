package pl.edu.mimuw.cloudatlas.module;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

import pl.edu.mimuw.cloudatlas.interpreter.InsideQueryException;
import pl.edu.mimuw.cloudatlas.interpreter.Interpreter;
import pl.edu.mimuw.cloudatlas.interpreter.InterpreterException;
import pl.edu.mimuw.cloudatlas.interpreter.Query;
import pl.edu.mimuw.cloudatlas.interpreter.QueryResult;
import pl.edu.mimuw.cloudatlas.interpreter.query.Yylex;
import pl.edu.mimuw.cloudatlas.interpreter.query.parser;
import pl.edu.mimuw.cloudatlas.main.Agent;
import pl.edu.mimuw.cloudatlas.main.Pair;
import pl.edu.mimuw.cloudatlas.message.BadTypeOfMessageException;
import pl.edu.mimuw.cloudatlas.message.Message;
import pl.edu.mimuw.cloudatlas.message.Message.MessageType;
import pl.edu.mimuw.cloudatlas.message.MessageSendMessage;
import pl.edu.mimuw.cloudatlas.message.MessageTimerWait;
import pl.edu.mimuw.cloudatlas.model.ZMI;

public class ModuleExecuteQueries extends Module {

	private long queryExecutePeriod;
	private LinkedBlockingQueue<Message> queueToSleep;
	private final static Logger LOGGER = Logger.getLogger(ModuleExecuteQueries.class
			.getName());

	public ModuleExecuteQueries(String name, Agent agent,
			long queryExecutePeriod) {
		super(name, agent);
		new Receiver().start();
		this.queueToSleep = new LinkedBlockingQueue<Message>();
		this.queryExecutePeriod = queryExecutePeriod;
		this.typeOfModule = ModuleType.MODULE_INTERPRETER;
		LOGGER.setLevel(Level.ALL);
	}

	@Override
	public void processMessage() throws Exception {
		Message newMessage = readMessage();
		if (newMessage.getTypeOfMessage() == MessageType.TIMER_END_OF_WAITING) {
			synchronized (queueToSleep) {
				queueToSleep.add(newMessage);
				queueToSleep.notify();
			}
		} else
			throw new BadTypeOfMessageException("Interpreter receive: "
					+ newMessage.getTypeOfMessage() + ".");
	}

	@Override
	public void run() {
		while (true) {
			goToSleep(queryExecutePeriod);
			startExecutingQuery();
		}
	}

	public void goToSleep(long sleepingTime) {
		String sender = agent.getMachineZMI().getAttributes().get("owner")
				.toString();
		MessageTimerWait msg = new MessageTimerWait(sleepingTime, sender,
				getName());
		MessageSendMessage msgSend = new MessageSendMessage(sender, sender,
				getName(), "", msg);
		agent.getCommunicationModule().messageToModule(msgSend);

		 synchronized (queueToSleep) {
			 try {
				 queueToSleep.wait();
			 } catch (InterruptedException e1) {
				 LOGGER.log(Level.WARNING, "Interupt Exception");
			 }
				
			 if(!queueToSleep.isEmpty()){
				 try {
					 queueToSleep.poll();
				 } catch (Exception e) {
					 LOGGER.log(Level.WARNING, "Exception queueToSleep poll");
				 }
			 }
		 }
	}

	public void startExecutingQuery() {
		ArrayList<Pair<ZMI, ArrayList<ZMI>>> treeOfHierarchy = agent
				.getHierarchyTree();
		for (int i = treeOfHierarchy.size() - 1; i >= 0; i--) {
			executeQueries(treeOfHierarchy.get(i).getFirst());
		}
	}

	public void executeQueries(ZMI zmi) {
		Query actualQuery=null;
		try {
			List<Query> queriesToExecute = zmi.getActiveQueries();
			for (Query query : queriesToExecute) {
				actualQuery = query;
				LOGGER.log(Level.INFO, "Executing query: " + query + " at level: " + zmi.getName());
				for (String s : query.getAllSelect()) {
					Interpreter interpreter = new Interpreter(zmi, agent.getHierarchyTree());
					Yylex lex = new Yylex(
							new ByteArrayInputStream(s.getBytes()));
					List<QueryResult> result = interpreter
							.interpretProgram((new parser(lex)).pProgram());
					for (QueryResult r : result) {
						zmi.getAttributes().addOrChange(r.getName(),
								r.getValue());
					}
				}
			}
		} catch (Exception e) {
			LOGGER.log(Level.WARNING, "Query: " + actualQuery + " cannot be executed.");
		}
	}

	private class Receiver extends Thread {
		@Override
		public void run() {
			while (true) {
				synchronized (input) {
					while (input.isEmpty()) {
						try {
							input.wait();
						} catch (InterruptedException e1) {
							LOGGER.log(Level.WARNING, "Interupt Exception Receiver");
						}
					}
					if (!input.isEmpty()) {
						try {
							processMessage();
						} catch (Exception e) {
							LOGGER.log(Level.WARNING, "Exception processMessage - Receiver");
						}
					}
				}
			}
		}
	}
}