package pl.edu.mimuw.cloudatlas.module;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

import pl.edu.mimuw.cloudatlas.certification.ZMICert;
import pl.edu.mimuw.cloudatlas.certification.ZoneCert;
import pl.edu.mimuw.cloudatlas.common.CryptoManager;
import pl.edu.mimuw.cloudatlas.main.Agent;
import pl.edu.mimuw.cloudatlas.main.Pair;
import pl.edu.mimuw.cloudatlas.message.BadTypeOfMessageException;
import pl.edu.mimuw.cloudatlas.message.Message;
import pl.edu.mimuw.cloudatlas.message.Message.MessageType;
import pl.edu.mimuw.cloudatlas.message.MessageGossipConfirm;
import pl.edu.mimuw.cloudatlas.message.MessageGossipInvitation;
import pl.edu.mimuw.cloudatlas.message.MessageGossipInvitationAnswer;
import pl.edu.mimuw.cloudatlas.message.MessageSendMessage;
import pl.edu.mimuw.cloudatlas.message.MessagePackageZMI;
import pl.edu.mimuw.cloudatlas.message.MessageTimerWait;
import pl.edu.mimuw.cloudatlas.message.TimestampsTree;
import pl.edu.mimuw.cloudatlas.message.Package;
import pl.edu.mimuw.cloudatlas.model.Attribute;
import pl.edu.mimuw.cloudatlas.model.AttributesMap;
import pl.edu.mimuw.cloudatlas.model.Value;
import pl.edu.mimuw.cloudatlas.model.ZMI;
import pl.edu.mimuw.cloudatlas.module.gossiper.Gossip;
import pl.edu.mimuw.cloudatlas.module.gossiper.Gossip.TimeSample;
import pl.edu.mimuw.cloudatlas.module.gossiper.Strategy;
import pl.edu.mimuw.cloudatlas.module.gossiper.Strategy.StrategyType;
import pl.edu.mimuw.cloudatlas.module.gossiper.StrategyRandomDecreasing;
import pl.edu.mimuw.cloudatlas.module.gossiper.StrategyRandomNormal;
import pl.edu.mimuw.cloudatlas.module.gossiper.StrategyRoundRobinDecreasing;
import pl.edu.mimuw.cloudatlas.module.gossiper.StrategyRoundRobinNormal;

public class ModuleGossiper extends Module implements Runnable {

	final static int GOSSIP_CLEANER_IN_ROUND = 10;
	final static int TIME_TO_OVERDUE_GOSSIP = 60000; // One minute
	private final static Logger LOGGER = Logger.getLogger(ModuleGossiper.class
			.getName());

	private HashMap<String, Gossip> actualGossips;
	private Strategy strategy;
	private Random generator;

	private long firstGossipAfter, periodBegin, periodEnd;
	private LinkedBlockingQueue<Message> queueToSleep;

	public ModuleGossiper(String id, Agent a, long firstGossipAfter,
			long periodBegin, long periodEnd, StrategyType TYPE_OF_STRATEGY) {
		super(id, a);
		LOGGER.setLevel(Level.ALL);
		this.typeOfModule = ModuleType.MODULE_GOSSIPER;
		actualGossips = new HashMap<String, Gossip>();
		queueToSleep = new LinkedBlockingQueue<Message>();

		switch (TYPE_OF_STRATEGY) {
		case RANDOM_DECREASING:
			this.strategy = new StrategyRandomDecreasing();
			break;
		case RANDOM_NORMAL:
			this.strategy = new StrategyRandomNormal();
			break;
		case ROUND_ROBIN_DECREASING:
			this.strategy = new StrategyRoundRobinDecreasing();
			break;
		case ROUND_ROBIN_NORMAL:
			this.strategy = new StrategyRoundRobinNormal();
			break;
		}
		this.periodBegin = periodBegin;
		this.periodEnd = periodEnd;
		this.firstGossipAfter = firstGossipAfter;
		generator = new Random();
	}

	@Override
	public void run() {
		LOGGER.log(Level.INFO, "Run Module Gossiper");
		new GossipReceiver().start();
		goToSleep(firstGossipAfter);
		int gossipRound = 0;
		while (true) {
			inviteToGossip();
			gossipRound++;
			goToSleep(periodBegin, periodEnd);
			if (gossipRound == GOSSIP_CLEANER_IN_ROUND) {
				gossipRound = 0;
				cleanGossip();
			}

		}
	}

	public void inviteToGossip() {
		LOGGER.logp(Level.FINE, "ModuleGossiper", "inviteToGossip",
				"Invite to gossip");
		Pair<Integer, Integer> nextGossip = strategy.getNext(agent
				.getHierarchyTree());

		// check if there are more then 0 siblings
		if (nextGossip.getSecond() >= 0) {

			ZMI me = agent.getZMIonLevel(nextGossip.getFirst());
			ZMI responder = agent.getSiblingZMIonLevel(nextGossip.getFirst(),
					nextGossip.getSecond());

			String sender = agent.getMachineZMI().getAttributes().get("owner")
					.toString();
			String receiver = responder.getAttributes().get("owner").toString();
			LOGGER.logp(Level.FINE, "ModuleGossiper", "inviteToGossip",
					"Invite to gossip " + sender + " -> " + receiver);

			String uniqueGossipId;

			// unique id of the gossip
			do {
				uniqueGossipId = sender + generator.nextInt();
			} while (actualGossips.containsKey(uniqueGossipId));
			
			TimestampsTree timestampsTree = new TimestampsTree(agent, nextGossip.getFirst());

			actualGossips.put(uniqueGossipId, new Gossip(nextGossip.getFirst(),
					receiver, "", me, responder, agent.getActualTime(), timestampsTree));

			// create gossip invitation
			MessageGossipInvitation msgGosInv = new MessageGossipInvitation(
					nextGossip.getFirst(), sender, getName(), uniqueGossipId,
					timestampsTree);
			MessageSendMessage msgSendMsg = new MessageSendMessage(sender,
					receiver, getName(), "", msgGosInv);

			// sending invitation
			agent.getCommunicationModule().messageToModule(msgSendMsg);
		}
	}

	@Override
	public void processMessage() throws Exception {
		Message newMessage = readMessage();
		if (newMessage.getTypeOfMessage() == MessageType.GOSSIP_INVITATION) {
			processInvitation((MessageGossipInvitation) newMessage);
		} else if (newMessage.getTypeOfMessage() == MessageType.GOSSIP_INVITATION_ANSWER) {
			processAnswerOnInvitation((MessageGossipInvitationAnswer) newMessage);
		} else if (newMessage.getTypeOfMessage() == MessageType.GOSSIP_CONFIRM) {
			processConfirmGossip((MessageGossipConfirm) newMessage);
		} else if (newMessage.getTypeOfMessage() == MessageType.GOSSIP_ZMI_PACKAGE) {
			processReceivedPackage((MessagePackageZMI) newMessage);
		} else if (newMessage.getTypeOfMessage() == MessageType.TIMER_END_OF_WAITING) {
			synchronized (queueToSleep) {
				queueToSleep.add(newMessage);
				queueToSleep.notify();
			}
		} else
			throw new BadTypeOfMessageException("Gossiper receive: "
					+ newMessage.getTypeOfMessage() + ".");
	}

	public void goToSleep(long sleepingTime) {
		String sender = agent.getMachineZMI().getAttributes().get("owner")
				.toString();
		MessageTimerWait msg = new MessageTimerWait(sleepingTime, sender,
				getName());
		MessageSendMessage msgSend = new MessageSendMessage(sender, sender,
				getName(), "", msg);
		agent.getCommunicationModule().messageToModule(msgSend);

		synchronized (queueToSleep) {
			while (queueToSleep.isEmpty())
				try {
					queueToSleep.wait();
				} catch (InterruptedException e1) {
					LOGGER.log(Level.SEVERE, "Interruption error", e1);
				}

			if (!queueToSleep.isEmpty()) {
				queueToSleep.poll();
			}
		}
	}

	public void goToSleep(long sleepingPeriodBegin, long sleepeingPeriodEnd) {
		long sleepingTime = generator
				.nextInt((int) (sleepeingPeriodEnd - sleepingPeriodBegin))
				+ sleepingPeriodBegin;
		goToSleep(sleepingTime);
	}

	// dostałem zaproszenie do plotkowania, odpowiadam na nie
	public void processInvitation(MessageGossipInvitation msg) {
		LOGGER.logp(Level.FINE, "ModuleGossiper", "inviteToGossip",
				"Got nvitation from " + msg.getSender());
		int level = msg.getLevel();
		String uniqueGossipId = msg.getUID();
		TimestampsTree hisTimestampsTree = msg.getTimestampsTree();
		TimestampsTree myTimestampsTree = new TimestampsTree(agent, level);
		String sender = msg.getSender();
		String moduleSender = msg.getModuleSender();

		ZMI me = agent.getZMIonLevel(level);
		ZMI responder = findSibling(sender, level);

		// zapamiętuję rozmowę
		actualGossips.put(uniqueGossipId, new Gossip(level, sender,
				moduleSender, me, responder, agent.getActualTime(),
				myTimestampsTree, hisTimestampsTree));

		// utworzenie odpowiedzi na zaproszenie zawierającej znaczniki czasowe
		MessageGossipInvitationAnswer msgGosInvAns = new MessageGossipInvitationAnswer(
				uniqueGossipId, getName(), myTimestampsTree, msg.getT1A(), msg.getT1B());
		String myName = agent.getMachineZMI().getAttributes().get("owner")
				.toString();
		MessageSendMessage msgSendMsg = new MessageSendMessage(myName, sender,
				getName(), moduleSender, msgGosInvAns);

		// wysłanie odpowiedzi
		agent.getCommunicationModule().messageToModule(msgSendMsg);

		TimeSample ts = actualGossips.get(uniqueGossipId).getTimeSample();
		ts.setT1A(msg.getT1A());
		ts.setT1B(msg.getT1B());

	}

	public void processAnswerOnInvitation(MessageGossipInvitationAnswer msg) {
		String uniqueGossipId = msg.getUID();
		TimestampsTree hisTimestampsTree = msg.getTimestampsTree();

		Gossip actualGossip = actualGossips.get(uniqueGossipId);

		if (actualGossip != null) {

			actualGossip.setHisTimestampTree(hisTimestampsTree);			
			
			TimeSample ts = actualGossip.getTimeSample();
			ts.setT1A(msg.getT1A());
			ts.setT1B(msg.getT1B());
			ts.setT2B(msg.getT2B());
			ts.setT2A(msg.getT2A());

			actualGossip.setModulePartner(msg.getModuleSender());

			String myName = agent.getMachineZMI().getAttributes().get("owner")
					.toString();

			MessageGossipConfirm msgGosConf = new MessageGossipConfirm(
					uniqueGossipId, msg.getT2A(), msg.getT2B());
			MessageSendMessage msgSend = new MessageSendMessage(myName,
					actualGossip.getPartner(), getName(),
					actualGossip.getModulePartner(), msgGosConf);
			agent.getCommunicationModule().messageToModule(msgSend);

			long rtd = ts.getT2A() - ts.getT1A() - ts.getT2B() + ts.getT1B();
			long dT = ts.getT2B() + (long) (0.5 * (double) rtd) - ts.getT2A();

			sendZMI(uniqueGossipId, dT, myName,
					actualGossip.getPartner(), getName(),
					actualGossip.getModulePartner());
			
		}
	}

	public void processConfirmGossip(MessageGossipConfirm msg) {
		String uniqueGossipID = msg.getUID();

		Gossip actualGossip = actualGossips.get(uniqueGossipID);
		TimeSample ts = actualGossip.getTimeSample();
		ts.setT2A(msg.getT2A());
		ts.setT2B(msg.getT2B());

		ZMI me = actualGossip.getMyZMI();

		long rtd = ts.getT2A() - ts.getT1A() - ts.getT2B() + ts.getT1B();
		long dT = ts.getT2B() + (long) (0.5 * (double) rtd) - ts.getT2A();

		sendZMI(uniqueGossipID, dT*(-1), me.getAttributes().get("owner").toString(),
				actualGossip.getPartner(), getName(),
				actualGossip.getModulePartner());
		
	}
	
	public void processReceivedPackage(MessagePackageZMI msg) throws Exception{
		PublicKey keyToVerification = agent.findKeyToVerification(msg.getZMIcertificate().getZoneCert().getCAname());
		boolean correctValidationOfCertificate = validateCertificate(msg.getZMIcertificate(), keyToVerification);
		
		if(!correctValidationOfCertificate){
			LOGGER.log(Level.WARNING, "Authentication failed. Cannot validate ZMIcertificate. "
					+ "Gossip request rejected.");
		}
		else if(msg.getZMIcertificate().certificateHasExpired()){
			LOGGER.log(Level.WARNING, "Authentication success, but certificate HAS EXPIRED. "
					+ "Gossip request rejected.");
		}
		else
		{			
			LOGGER.log(Level.INFO, "Authentication success. Gossip request authorized.");
			ArrayList<Package> packagesReceived = msg.getZMIcertificate().getPackageZMI();
			for(Package packageZMI: packagesReceived){
				String name = packageZMI.getName();
				HashMap<Attribute, Value> attributes = packageZMI.getElements();
				ZMI zmiToReplace = agent.getNodeByName(name);
				if(zmiToReplace == null){
					agent.createNode(name);
					zmiToReplace = agent.getNodeByName(name);
				}
				for(Entry<Attribute, Value> entry: attributes.entrySet()){
					if(entry.getValue().isCollection())
						zmiToReplace.getAttributes().concatenateCollection(entry);
					else
						zmiToReplace.getAttributes().addOrChange(entry);
				}
			}
		}
	}
	
	private boolean validateCertificate(ZMICert zmiCertificate, PublicKey keyToVerification){
		ZoneCert zoneCertificate = zmiCertificate.getZoneCert();
		
		if(!zoneCertificate.validSignature(keyToVerification))
			return false;
		return zmiCertificate.validSignature(zoneCertificate.getPublickey());
	}
	
	public ZMI findSibling(String sender, int level) {
		String[] levels = sender.trim().split("/");
		String levelName = "";

		for (int i = 0; i <= level; i++) {
			levelName += levels[i] + "/";
		}

		return agent.getSiblingZMIonLevelName(level, levelName);
	}
	
	public void sendZMI(String uid, long dT, String sender, String receiver, 
			String moduleSender, String moduleReceiver){
		ArrayList<Package> packagesToSend = preaparePackages(uid, dT);
		
		int gossipLevel = actualGossips.get(uid).getLevel();
		
		MessageSendMessage msgSend;
		MessagePackageZMI msgPack;
		
		long dateCreation = new Date().getTime();
		
		Long id = 0L, creation = dateCreation, expiry = dateCreation+1000*60;
		ZoneCert zoneCert = agent.getCertificates().get(gossipLevel-1).getZoneCertificate();		
		
		ZMICert zmiCertificate = new ZMICert(id, creation, expiry, packagesToSend, zoneCert);
		// nie robimy generateSignature na zmiCert - to zrobi ModuleCommunication gdy pogrupuje pakiety
		
		msgPack = new MessagePackageZMI(uid, sender, moduleSender, zmiCertificate);
		msgSend = new MessageSendMessage(sender,receiver, moduleSender, moduleReceiver, msgPack);
		agent.getCommunicationModule().messageToModule(msgSend);
		
	}
	
	private ArrayList<Package> preaparePackages(String uid, long dT) {
		Gossip actualGossip = actualGossips.get(uid);
		actualGossip.updateMyTimestampsTree(dT);
		ArrayList<Package> answer = new ArrayList<Package>();
		TimestampsTree my = actualGossip.getMyTimestampsTree();
		TimestampsTree his = actualGossip.getHisTimestampsTree();
		for(int i=0; i<my.size(); i++){
			preparePackagesForLevel(my.getLevel(i), his.getLevel(i), answer);
		}	
//		String info = "-----\nPlotkowanie pomiedzy: " + 
//				agent.getMachineZMI().getName() + " <-> " + actualGossip.getPartner() +
//				"\nPakiety:\n";
//		for(Package p: answer){
//			info += "\t -> " + p + "\n";
//		}
//		LOGGER.log(Level.INFO, info);
		return answer;
	}
	
	private void preparePackagesForLevel(ArrayList<Pair<String, Long>> my,
			ArrayList<Pair<String, Long>> his, ArrayList<Package> answer) {
		
		for(Pair<String, Long> node: my){
			Pair<String, Long> hisNode = findNode(node.getFirst(), his);
			if(hisNode == null){
				packZMI(node.getFirst(), answer);
			}
			else{
				// check if his timestamp is younger than my
				if(hisNode.getSecond() < node.getSecond()){
					packZMI(node.getFirst(), answer);
				}
			}
		}
	}

	private Pair<String, Long> findNode(String name, ArrayList<Pair<String, Long>> hisNodes) {
		for(Pair<String, Long> node: hisNodes){
			if(node.getFirst().equals(name))
				return node;
		}
		return null;
	}

	private void packZMI(String name, ArrayList<Package> answer) {
		ZMI zmi = agent.getNodeByName(name);
		
		final int sizeOfPackage = 10;
		AttributesMap attributesMap = zmi.getAttributes();
		
		int inPackage = 0;
		Package packageZMI = new Package(name);
		
		for (Entry<Attribute, Value> e : attributesMap) {
			if (e.getValue().isCollection()) {
				Package myPackage = new Package(name);
				myPackage.addElement(e);
				answer.add(myPackage);
			} else {
				inPackage++;
				packageZMI.addElement(e);
				if (inPackage == sizeOfPackage) {
					answer.add(packageZMI);
					inPackage = 0;
					packageZMI = new Package(name);
				}
			}
		}

		if (inPackage > 0) {
			answer.add(packageZMI);
		}		
	}
	
	public void cleanGossip() {
		long actualTime = agent.getActualTime();
		Iterator<Map.Entry<String, Gossip>> iter = actualGossips.entrySet()
				.iterator();
		while (iter.hasNext()) {
			Map.Entry<String, Gossip> entry = iter.next();
			if (entry.getValue().getDateOfStart() + TIME_TO_OVERDUE_GOSSIP < actualTime)
				iter.remove();
		}
	}

	private class GossipReceiver extends Thread {

		public GossipReceiver() {

		}

		@Override
		public void run() {
			while (true) {
				synchronized (input) {
					while (input.isEmpty()) {
						try {
							input.wait();
						} catch (InterruptedException e1) {
							LOGGER.log(Level.SEVERE, "Interruption error", e1);
						}
					}
					if (!input.isEmpty()) {
						try {
							processMessage();
						} catch (Exception e) {
							LOGGER.log(Level.SEVERE, "Bad type of message", e);
						}
					}
				}
			}
		}
	}
}
