package pl.edu.mimuw.cloudatlas.module;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

import pl.edu.mimuw.cloudatlas.main.Agent;
import pl.edu.mimuw.cloudatlas.message.exportinfo.MessageExportInfo;
import pl.edu.mimuw.cloudatlas.message.exportinfo.MessageExportInfoAnswer;

public class ModuleExportInfo extends Module implements Runnable{
	
	private final static Logger LOGGER = Logger.getLogger(ModuleExportInfo.class.getName());

	public ModuleExportInfo(String id, Agent a) {
		super(id, a);
		this.typeOfModule = ModuleType.MODULE_EXPORT_INFO;
	}
	
	@Override
	public void run(){	
		while(true){			
			synchronized (input) {
				while (input.isEmpty()){
					try {
						input.wait();
					} catch (InterruptedException e1) {
						LOGGER.log(Level.WARNING, "Interupt Exception");
					}
		        }		   
				if(!input.isEmpty()){
					try {
						processMessage();				
					} catch (Exception e) {
						LOGGER.log(Level.WARNING, "Exception - processMessage()");
					}
				}     
			}
		}
	}
	

	@Override
	public void processMessage() throws Exception{
		MessageExportInfo newMessage = (MessageExportInfo) readMessage();
		MessageExportInfoAnswer answer = newMessage.process(this.agent);
		LinkedBlockingQueue<MessageExportInfoAnswer> queueToAnswer = newMessage.getQueueToAnswer();
		synchronized (queueToAnswer) {
			queueToAnswer.add(answer);
			queueToAnswer.notify();
		}
	}		
}
