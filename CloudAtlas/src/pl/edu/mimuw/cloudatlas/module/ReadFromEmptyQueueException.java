package pl.edu.mimuw.cloudatlas.module;

@SuppressWarnings("serial")
public class ReadFromEmptyQueueException extends Exception{
	public ReadFromEmptyQueueException(String moduleName){
		super(moduleName);
	}
}