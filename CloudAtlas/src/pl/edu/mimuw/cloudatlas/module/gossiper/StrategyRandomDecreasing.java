package pl.edu.mimuw.cloudatlas.module.gossiper;

import java.util.ArrayList;
import java.util.Random;

import pl.edu.mimuw.cloudatlas.main.Pair;
import pl.edu.mimuw.cloudatlas.model.ZMI;

public class StrategyRandomDecreasing extends Strategy{

	private Random generator;
	
	public StrategyRandomDecreasing() {
		super(StrategyType.RANDOM_DECREASING);
		generator = new Random();
	}

	@Override
	public Pair<Integer, Integer> getNext(
			ArrayList<Pair<ZMI, ArrayList<ZMI>>> tree) {
		int levels = tree.size();
		int possibilities = (int)Math.pow(2, (levels-1))-1;
		int choose = generator.nextInt(possibilities)+1;
		int log = (int) Math.floor(Math.log(choose));
		int gossipLevel = levels-log-1;		
		int numberOfSiblings = tree.get(gossipLevel).getSecond().size();
		if(numberOfSiblings == 0)
			return new Pair<Integer, Integer>(-1, -1);
		else{
			int siblingToGossip = generator.nextInt(numberOfSiblings);
			return new Pair<Integer, Integer>(gossipLevel, siblingToGossip);
		}
	}

}
