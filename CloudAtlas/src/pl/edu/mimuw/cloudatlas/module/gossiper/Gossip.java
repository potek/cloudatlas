package pl.edu.mimuw.cloudatlas.module.gossiper;

import pl.edu.mimuw.cloudatlas.message.TimestampsTree;
import pl.edu.mimuw.cloudatlas.model.ZMI;

public class Gossip {
	private int level;
	private String partner;
	private String modulePartner;
	private ZMI me;
	private ZMI responder;
	private TimeSample timeSample;
	private long dateOfStart;
	private TimestampsTree myTimestampsTree;
	private TimestampsTree hisTimestampsTree;
	
	public Gossip(int level, String partner, String modulePartner, ZMI me, ZMI responder, long dateOfStart, TimestampsTree myTT){
		this.level = level;
		this.partner = partner;
		this.modulePartner = modulePartner;
		this.me = me;
		this.responder = responder;
		this.timeSample = new TimeSample();
		this.dateOfStart = dateOfStart;
		this.myTimestampsTree = myTT;
	}
	
	public Gossip(int level, String partner, String modulePartner, ZMI me, ZMI responder, long dateOfStart, TimestampsTree myTT, TimestampsTree hisTT){
		this(level, partner, modulePartner, me, responder,dateOfStart, myTT);
		this.hisTimestampsTree = hisTT;
	}
	
	public int getLevel(){
		return level;
	}
	
	public ZMI getMyZMI(){
		return me;
	}
	
	public ZMI getResponderZMI(){
		return responder;
	}
	
	public String getPartner(){
		return partner;
	}
	
	public TimeSample getTimeSample(){
		return timeSample;
	}

	public String getModulePartner(){
		return modulePartner;
	}
	
	public void setModulePartner(String modulePartner){
		this.modulePartner = modulePartner;
	}
	
	public TimestampsTree getHisTimestampsTree(){
		return hisTimestampsTree;
	}
	
	public TimestampsTree getMyTimestampsTree(){
		return myTimestampsTree;
	}
	
	public void setHisTimestampTree(TimestampsTree hisTT){
		this.hisTimestampsTree = hisTT;
	}
	
	public void updateMyTimestampsTree(long dT){
		myTimestampsTree.updateTimestampsTree(dT);
	}
	
	public long getDateOfStart(){
		return dateOfStart;
	}
	
	public String toString(){
		return "Gossip: L:" + level + " Partner: " + partner + "->" + modulePartner;
	}
	
	public class TimeSample{
		private long T1A;
		private long T2A;
		private long T1B;
		private long T2B;
		
		public TimeSample(){
		}
		
		public void setT1A(long T1A){
			this.T1A = T1A;
		}
		
		public void setT2A(long T2A){
			this.T2A = T2A;
		}
		
		public void setT1B(long T1B){
			this.T1B = T1B;
		}
		
		public void setT2B(long T2B){
			this.T2B = T2B;
		}
		
		public long getT1A(){
			return T1A;
		}
		
		public long getT2A(){
			return T2A;
		}
		
		public long getT1B(){
			return T1B;
		}
		
		public long getT2B(){
			return T2B;
		}
	}
}
