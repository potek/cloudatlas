package pl.edu.mimuw.cloudatlas.module.gossiper;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Random;

import pl.edu.mimuw.cloudatlas.main.Pair;
import pl.edu.mimuw.cloudatlas.model.ZMI;

public class StrategyRoundRobinNormal extends Strategy{

	private Random generator;
	private LinkedList<Integer> memory;

	public StrategyRoundRobinNormal() {
		super(StrategyType.ROUND_ROBIN_NORMAL);
		generator = new Random();
		memory = new LinkedList<Integer>();
	}

	@Override
	public Pair<Integer, Integer> getNext(
			ArrayList<Pair<ZMI, ArrayList<ZMI>>> tree) {
		int gossipLevel;
		if(memory.size()==0){
			int size = tree.size();
			for(int i=1; i<size; i++)
				memory.add(i);
		}		
		gossipLevel = memory.poll();
		int numberOfSiblings = tree.get(gossipLevel).getSecond().size();
		if(numberOfSiblings == 0)
			return new Pair<Integer, Integer>(-1, -1);
		else{
			int siblingToGossip = generator.nextInt(numberOfSiblings);
			return new Pair<Integer, Integer>(gossipLevel, siblingToGossip);
		}
	}

}
