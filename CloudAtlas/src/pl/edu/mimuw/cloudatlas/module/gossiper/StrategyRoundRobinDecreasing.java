package pl.edu.mimuw.cloudatlas.module.gossiper;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Random;

import pl.edu.mimuw.cloudatlas.main.Pair;
import pl.edu.mimuw.cloudatlas.model.ZMI;

public class StrategyRoundRobinDecreasing extends Strategy{

	private Random generator;
	private LinkedList<Integer> memory;
	
	public StrategyRoundRobinDecreasing() {
		super(StrategyType.ROUND_ROBIN_DECREASING);
		generator = new Random();
		memory = new LinkedList<Integer>();
	}

	@Override
	public Pair<Integer, Integer> getNext(
			ArrayList<Pair<ZMI, ArrayList<ZMI>>> tree) {
		int gossipLevel;
		if(memory.size()==0){
			int levels = tree.size();
			int possibilities = (int)Math.pow(2, (levels-1))-1;
			for(int i=1; i<=possibilities; i++){
				memory.add(levels-((int) Math.floor(Math.log(i)))-1);
			}
		}		
		gossipLevel = memory.poll();
		int numberOfSiblings = tree.get(gossipLevel).getSecond().size();
		if(numberOfSiblings == 0)
			return new Pair<Integer, Integer>(-1, -1);
		else{
			int siblingToGossip = generator.nextInt(numberOfSiblings);
			return new Pair<Integer, Integer>(gossipLevel, siblingToGossip);
		}
	}

}
