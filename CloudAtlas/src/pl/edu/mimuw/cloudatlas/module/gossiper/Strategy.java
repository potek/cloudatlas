package pl.edu.mimuw.cloudatlas.module.gossiper;

import java.util.ArrayList;

import pl.edu.mimuw.cloudatlas.main.Pair;
import pl.edu.mimuw.cloudatlas.model.ZMI;

public abstract class Strategy {
	protected StrategyType strategyType;
	
	public Strategy(StrategyType st){
		this.strategyType = st;
	}
	
	public static enum StrategyType {
		ROUND_ROBIN_NORMAL,
		ROUND_ROBIN_DECREASING,
		RANDOM_NORMAL,
		RANDOM_DECREASING
	}
	
	public abstract Pair<Integer, Integer> getNext(ArrayList<Pair<ZMI, ArrayList<ZMI>>> tree);
}
