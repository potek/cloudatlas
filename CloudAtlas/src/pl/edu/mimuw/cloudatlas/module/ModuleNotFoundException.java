package pl.edu.mimuw.cloudatlas.module;

@SuppressWarnings("serial")
public class ModuleNotFoundException extends Exception{
	public ModuleNotFoundException(String moduleName){
		super(moduleName);
	}
}
