package pl.edu.mimuw.cloudatlas.module;

import pl.edu.mimuw.cloudatlas.main.Agent;
import pl.edu.mimuw.cloudatlas.message.BadTypeOfMessageException;
import pl.edu.mimuw.cloudatlas.message.Message;
import pl.edu.mimuw.cloudatlas.message.Message.MessageType;
import pl.edu.mimuw.cloudatlas.message.MessageSendMessage;
import pl.edu.mimuw.cloudatlas.message.MessageTimerEndOfWaiting;
import pl.edu.mimuw.cloudatlas.message.MessageTimerWait;

import java.util.logging.Level;
import java.util.logging.Logger;

public class ModuleTimer extends Module{
	
	private final static Logger LOGGER = Logger.getLogger(ModuleTimer.class.getName());
	
	public ModuleTimer(String id, Agent a){
		super(id, a);
		this.typeOfModule = ModuleType.MODULE_TIMER;
	}
	
	@Override
	public void run(){
		while(true){			
			synchronized (input) {
		        while (input.isEmpty())
					try {
						input.wait();
					} catch (InterruptedException e1) {
						LOGGER.log(Level.WARNING, "Interupt Exception");
					}
		        
				if(!input.isEmpty()){
					try {
						processMessage();
					} catch (Exception e) {
						LOGGER.log(Level.WARNING, "Exception - processMessage()");
					}
				}     
			}
		}
	}
	
	
	@Override
	public void processMessage() throws Exception{
		if(!input.isEmpty()){
			Message newMessage = readMessage();
			if(newMessage.getTypeOfMessage() == MessageType.TIMER_WAIT){
				waitAndInform((MessageTimerWait)newMessage);
			}
			else{
				throw new BadTypeOfMessageException("Timer needs message typeof " + MessageType.TIMER_WAIT +
													". Timer receive: " + newMessage.getTypeOfMessage()+".");
			}
		}
	}
	
	public void waitAndInform(MessageTimerWait mtw){
		new Waiter(mtw).start();
	}
	
	private class Waiter extends Thread{

		private String sender;
		private String moduleSender;
		private long waitMS;
		
		public Waiter(MessageTimerWait mtw){
			this.sender = mtw.getSender();
			this.moduleSender = mtw.getModuleSender();
			this.waitMS = mtw.getWaitMS();
		}
		
		public void run(){
			try {
				Thread.sleep(waitMS);
			} catch (InterruptedException e) {
				LOGGER.log(Level.WARNING, "Interupt Exception - Waiter - run()");

			}
			
			MessageTimerEndOfWaiting mteow = new MessageTimerEndOfWaiting();
			String myOwner = agent.getMachineZMI().getAttributes().get("owner").toString();
			MessageSendMessage tmp = new MessageSendMessage(myOwner, sender, getName(), moduleSender, mteow);
			ModuleTimer.this.getAgent().getCommunicationModule().messageToModule(tmp);
		}
	}

}
