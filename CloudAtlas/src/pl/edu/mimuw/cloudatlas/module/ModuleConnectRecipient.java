package pl.edu.mimuw.cloudatlas.module;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.security.PublicKey;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import pl.edu.mimuw.cloudatlas.certification.ZoneCert;
import pl.edu.mimuw.cloudatlas.main.Agent;
import pl.edu.mimuw.cloudatlas.main.CertificatesInNode;
import pl.edu.mimuw.cloudatlas.main.Pair;
import pl.edu.mimuw.cloudatlas.message.Message;
import pl.edu.mimuw.cloudatlas.message.Message.MessageType;
import pl.edu.mimuw.cloudatlas.message.MessageAnswerOnConnectRequest;
import pl.edu.mimuw.cloudatlas.message.MessageAnswerOnConnectRequest.TypeOfAnswer;
import pl.edu.mimuw.cloudatlas.message.MessageConfirmReceive;
import pl.edu.mimuw.cloudatlas.message.MessageConnectNewNode;
import pl.edu.mimuw.cloudatlas.message.MessageConnectRequest;
import pl.edu.mimuw.cloudatlas.message.MessageSendMessage;
import pl.edu.mimuw.cloudatlas.model.PathName;
import pl.edu.mimuw.cloudatlas.model.TypePrimitive;
import pl.edu.mimuw.cloudatlas.model.Value;
import pl.edu.mimuw.cloudatlas.model.ValueContact;
import pl.edu.mimuw.cloudatlas.model.ValueInt;
import pl.edu.mimuw.cloudatlas.model.ValueSet;
import pl.edu.mimuw.cloudatlas.model.ZMI;

public class ModuleConnectRecipient extends Module{
	private final static Logger LOGGER = Logger.getLogger(ModuleConnectRecipient.class
			.getName());
	final long TIME_TO_RETRANSMISSION = 5000L;
	
	public ModuleConnectRecipient(String name, Agent agent) {
		super(name, agent);
		LOGGER.setLevel(Level.ALL);
		this.typeOfModule = ModuleType.MODULE_CONNECT_RECIPIENT;
	}

	@Override
	public void run(){	
		while(true){			
			synchronized (input) {
				while (input.isEmpty()){
					try {
						input.wait();
					} catch (InterruptedException e1) {
						LOGGER.log(Level.WARNING, "Interupt Exception - run()");
					}
		        }		   
				if(!input.isEmpty()){
					try {
						processMessage();
					} catch (Exception e) {
						LOGGER.log(Level.WARNING, "Exception - processMessage()");
					}
				}     
			}
		}
	}
	
	@Override
	public void processMessage() throws Exception {
		Message msg = readMessage();
		if(msg.getTypeOfMessage() == MessageType.CONNECT_REQUEST){
			processRequestForConnect((MessageConnectRequest) msg);
		}
		else if(msg.getTypeOfMessage() == MessageType.CONNECT_NEW_NODE){
			processNewNodeConnection((MessageConnectNewNode) msg);
		}
	}
	
	private void addNewContact(ValueSet contacts, ValueContact new_contact) {
		Boolean canAdd = true;
		String new_name = new_contact.getName().getName();
		for(Value v : contacts) {
			ValueContact vc = (ValueContact) v;
			String vc_name = vc.getName().getName();
			if(new_name.equals(vc_name)) {
				canAdd = false;
				break;
			}
		}
		if(canAdd)
			contacts.add(new_contact);
	}
	
	public void processRequestForConnect(MessageConnectRequest msg) throws UnknownHostException{
		
		PublicKey publicKey = agent.findKeyToVerification(msg.getZoneCert().getCAname());
		
		if(msg.getZoneCert().validSignature(publicKey)){
			if(msg.getZoneCert().certificateHasExpired()){
				LOGGER.log(Level.WARNING, "Authentication success, but certificate HAS EXPIRED. "
						+ "Connect request rejected.");
				
				MessageAnswerOnConnectRequest answer = new MessageAnswerOnConnectRequest(TypeOfAnswer.REJECT_EXPIRED);
				try {
					Socket socket = new Socket(msg.getHost(), msg.getPort());
					ObjectOutputStream toServer = new ObjectOutputStream(
							new BufferedOutputStream(socket.getOutputStream()));
		
					toServer.writeObject(answer);
					toServer.flush();
					LOGGER.log(Level.INFO, "Send answer for request for connect to " + msg.getHost() + " " + msg.getPort());
					
					socket.close();							
				} catch (IOException e) {
					LOGGER.log(Level.SEVERE,"Cannot create socket - bad host(" + msg.getHost() + ") or port(" + msg.getPort() + ").", e);
				}
			}
			else{
				LOGGER.log(Level.INFO, "Authentication success. New node will be connected.");
			
				ArrayList<Pair<ZMI, ArrayList<ZMI>>> treeOfHierarchy = agent.getHierarchyTree();
				
				ValueSet contacts = (ValueSet) agent.getMachineZMI().getAttributes().getOrNull("contacts");
				if(contacts == null){
					contacts = new ValueSet(TypePrimitive.CONTACT);
				}
		
				addNewContact(contacts, new ValueContact(new PathName(msg.getSender()), msg.getHost(), msg.getPort()));
				agent.getHierarchyTree().get(0).getFirst().getAttributes().addOrChange("contacts", contacts);				
				
				int level = (int)(long)((ValueInt) agent.getMachineZMI().getAttributes().get("level")).getValue();
				ZoneCert zoneCert = agent.getCertificates().get(level-1).getZoneCertificate();
				MessageAnswerOnConnectRequest answer = new MessageAnswerOnConnectRequest(treeOfHierarchy, zoneCert);
		
				try {
					Socket socket = new Socket(msg.getHost(), msg.getPort());
					
					ObjectOutputStream toServer = new ObjectOutputStream(
							new BufferedOutputStream(socket.getOutputStream()));
					ObjectInputStream fromServer = null;
		
					toServer.writeObject(answer);
					toServer.flush();
					LOGGER.log(Level.INFO, "Send answer for request for connect to " + msg.getHost() + " " + msg.getPort());
					
					fromServer = new ObjectInputStream(
							new BufferedInputStream(socket.getInputStream()));
					MessageAnswerOnConnectRequest msgFromReply = (MessageAnswerOnConnectRequest)fromServer.readObject();
					
					socket.close();			
			        
				    connectAndPropagateTree(msgFromReply.getTreeOfHierarchy());
					
				} catch (IOException | ClassNotFoundException e) {
					LOGGER.log(Level.SEVERE,"Cannot create socket - bad host(" + msg.getHost() + ") or port(" + msg.getPort() + ").");
				}
			}
		}
		else
		{
			LOGGER.log(Level.WARNING,"Authentication failed. Certificate doesn't match to te PublicKey. "
					+ "Connect request rejected.");
			
			MessageAnswerOnConnectRequest answer = new MessageAnswerOnConnectRequest(TypeOfAnswer.REJECT_KEY_MISMATCH);
			try {
				Socket socket = new Socket(msg.getHost(), msg.getPort());
				ObjectOutputStream toServer = new ObjectOutputStream(
						new BufferedOutputStream(socket.getOutputStream()));
	
				toServer.writeObject(answer);
				toServer.flush();
				LOGGER.log(Level.INFO, "Send answer for request for connect to " + msg.getHost() + " " + msg.getPort());
				
				socket.close();							
			} catch (IOException e) {
				LOGGER.log(Level.SEVERE,"Cannot create socket - bad host(" + msg.getHost() + ") or port(" + msg.getPort() + ").", e);
			}
		}
	}
	
	public void connectAndPropagateTree(ArrayList<Pair<ZMI, ArrayList<ZMI>>> receivedTree){
		int i=-1;
		ZMI treeOnLevel, connectTreeOnLevel;
		String tmp1, tmp2;
		do{
			++i;
			treeOnLevel = agent.getHierarchyTree().get(i).getFirst();
			connectTreeOnLevel = receivedTree.get(i).getFirst();
			tmp1 = treeOnLevel.getName();
			tmp2 = connectTreeOnLevel.getName();
		} while(tmp1==tmp2 || tmp1.equals(tmp2));
		
		ValueSet contacts = (ValueSet) agent.getHierarchyTree().get(i).getFirst().getAttributes().get("contacts");
		if(contacts == null){
			contacts = new ValueSet(TypePrimitive.CONTACT);
		}
		for(Pair<ZMI, ArrayList<ZMI>> p: receivedTree){
			agent.unionContactSet(contacts, (ValueSet) p.getFirst().getAttributes().get("contacts"));
			for(ZMI zmi: p.getSecond()){
				agent.unionContactSet(contacts, (ValueSet) zmi.getAttributes().get("contacts"));
			}
		}
		
		agent.getHierarchyTree().get(i).getFirst().getAttributes().addOrChange("contacts", contacts);	
		
		boolean nodeAlreadyExists = false;
		for(ZMI zmi: agent.getHierarchyTree().get(i).getSecond()){
			if(zmi.getName().equals(tmp2)){
				nodeAlreadyExists = true;
				break;
			}
		}
		if(!nodeAlreadyExists)
		{
			propagateOnLevel(i, connectTreeOnLevel);
			
			agent.getHierarchyTree().get(i).getSecond().add(connectTreeOnLevel);
			
			int lastLevel = (int)((long)(((ValueInt)(agent.getMachineZMI().getAttributes().get("level"))).getValue()));
			
			for(int j=i+1; j<=lastLevel; j++){
				propagateOnLevel(j, connectTreeOnLevel);
			}
		}
	}
	
	public void propagateOnLevel(int level, ZMI node){
		ArrayList<Pair<String, Boolean>> brotherReceive = new ArrayList<Pair<String, Boolean>>();
		for(ZMI zmi : agent.getHierarchyTree().get(level).getSecond()){
			brotherReceive.add(new Pair<String, Boolean>(zmi.getAttributes().get("owner").toString(), false));
			MessageConnectNewNode msgNewNode = new MessageConnectNewNode(
					agent.getMachineZMI().getAttributes().get("owner").toString(), getName(), node);
			MessageSendMessage msg = new MessageSendMessage(agent.getMachineZMI().getAttributes().get("owner").toString(), 
					zmi.getAttributes().get("owner").toString(), getName(), "", msgNewNode);
			agent.getCommunicationModule().messageToModule(msg);
		}
		
		if(brotherReceive.size() != 0){
			try {
				boolean receiveAll = false;
				while(!receiveAll)
				{
					receiveAll = true;
					goToSleep(TIME_TO_RETRANSMISSION);
					ArrayList<Message> archiveMessage = new ArrayList<Message>();

					while(!input.isEmpty()){
						Message msg = readMessage();
						if(msg.getTypeOfMessage() == MessageType.CONFIRM_RECEIVE){
							MessageConfirmReceive msgConf = (MessageConfirmReceive) msg;
							for(Pair<String, Boolean> brother : brotherReceive){
								if(brother.getFirst().equals(msgConf.getSender()))
									brother.setSecond(true);
							}
						}
						else{
							archiveMessage.add(msg);
						} 
					}
					
					for(Message m: archiveMessage)
						this.messageToModule(m);
					
					for(Pair<String, Boolean> brother : brotherReceive){
						if(!brother.getSecond()){
							receiveAll = false;
							MessageConnectNewNode msgNewNode = new MessageConnectNewNode(
									agent.getMachineZMI().getAttributes().get("owner").toString(), getName(), node);
							MessageSendMessage msg = new MessageSendMessage(agent.getMachineZMI().getAttributes().get("owner").toString(), 
									brother.getFirst(), getName(), "", msgNewNode);
							agent.getCommunicationModule().messageToModule(msg);
						}
					}
				}
				
			} catch (Exception e) {
				LOGGER.log(Level.WARNING, "Exception - propagate on level");
			}
		}
	}
	
	public void processNewNodeConnection(MessageConnectNewNode msg){
		String sender = agent.getMachineZMI().getAttributes().get("owner").toString();
		MessageConfirmReceive msgConf = new MessageConfirmReceive(sender);
		MessageSendMessage msgSend = new MessageSendMessage(sender, msg.getSender(), getName(), msg.getModuleSender(), msgConf);
		agent.getCommunicationModule().messageToModule(msgSend);
		ZMI node = msg.getNode();
		int level = (int)((long)(((ValueInt)(node.getAttributes().get("level"))).getValue()));
		
		agent.getHierarchyTree().get(level).getSecond().add(node);
		
		int lastLevel = (int)((long)(((ValueInt)(agent.getMachineZMI().getAttributes().get("level"))).getValue()));
		for(int j=level+1; j<=lastLevel; j++)
			propagateOnLevel(j, node);	
	}

}
