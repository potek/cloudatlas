package pl.edu.mimuw.cloudatlas.module;

import java.util.LinkedList;
import java.util.concurrent.LinkedBlockingQueue;

import pl.edu.mimuw.cloudatlas.main.Agent;
import pl.edu.mimuw.cloudatlas.message.Message;
import pl.edu.mimuw.cloudatlas.message.Message.MessageType;
import pl.edu.mimuw.cloudatlas.message.MessageSendMessage;
import pl.edu.mimuw.cloudatlas.message.MessageTimerWait;
import java.util.logging.Level;
import java.util.logging.Logger;

public abstract class Module extends Thread implements Runnable{
	protected LinkedBlockingQueue<Message> input;
	protected Agent agent;
	protected ModuleType typeOfModule;
	private final static Logger LOGGER = Logger.getLogger(Module.class.getName());
	
	public Module(String name, Agent agent){
		super(name);
		this.agent = agent;
		this.input = new LinkedBlockingQueue<Message>();
	}
	
	public static enum ModuleType {
		MODULE_TIMER,
		MODULE_COMMUNICATION,
		MODULE_EXAMPLE,
		MODULE_GOSSIPER,
		MODULE_EXPORT_INFO,
		MODULE_CONNECT_RECIPIENT,
		MODULE_INTERPRETER,
		MODULE_PROPAGATE_QUERY
	}
	
	public void messageToModule(Message m){
		synchronized(input){
			input.add(m);
			input.notify();
		}
	}

	public Message readMessage() throws ReadFromEmptyQueueException{
		if(input.isEmpty())
			throw new ReadFromEmptyQueueException(""+typeOfModule);
		else
			return input.poll();
	}
	
	public MessageType getTypeOfMessage(){
		return input.peek().getTypeOfMessage();
	}
	
	public Agent getAgent(){
		return agent;
	}
	
	public abstract void processMessage() throws Exception;
	
	public ModuleType getModuleType(){
		return typeOfModule;
	}
	
	public void goToSleep(long sleepingTime) {
		String sender = agent.getMachineZMI().getAttributes().get("owner")
				.toString();
		MessageTimerWait msg = new MessageTimerWait(sleepingTime, sender,
				getName());
		MessageSendMessage msgSend = new MessageSendMessage(sender, sender,
				getName(), "", msg);
		agent.getCommunicationModule().messageToModule(msgSend);

		synchronized (input) {
			boolean receiveEndOfWaiting = false;
			while(input.isEmpty()){
				try {
					input.wait();
				} catch (InterruptedException e1) {
					LOGGER.log(Level.WARNING, "Interupt Exception - Module");
				}
			}
			
			LinkedList<Message> tmpQueue = new LinkedList<Message>();
			
			while(!receiveEndOfWaiting){
				
				if(getTypeOfMessage() == MessageType.TIMER_END_OF_WAITING){
					try {
						input.poll();
						receiveEndOfWaiting = true;
					} catch (Exception e) {
						LOGGER.log(Level.WARNING, "Exception - input.poll()");
					}
				}
				else{
					tmpQueue.add(input.poll());
					while(input.isEmpty()){
						try {
							input.wait();
						} catch (InterruptedException e1) {
							LOGGER.log(Level.WARNING, "Interupt Exception - goToSleep fun");
						}
					}
				}
			}
			for(Message m: tmpQueue){
				messageToModule(m);
			}

		}
	}
	
}
