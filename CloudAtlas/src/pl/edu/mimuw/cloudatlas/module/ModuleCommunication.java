package pl.edu.mimuw.cloudatlas.module;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import pl.edu.mimuw.cloudatlas.certification.ZMICert;
import pl.edu.mimuw.cloudatlas.main.Agent;
import pl.edu.mimuw.cloudatlas.main.Pair;
import pl.edu.mimuw.cloudatlas.message.BadTypeOfMessageException;
import pl.edu.mimuw.cloudatlas.message.Message;
import pl.edu.mimuw.cloudatlas.message.Message.MessageType;
import pl.edu.mimuw.cloudatlas.message.MessageGossipInvitation;
import pl.edu.mimuw.cloudatlas.message.MessageGossipInvitationAnswer;
import pl.edu.mimuw.cloudatlas.message.MessagePackageZMI;
import pl.edu.mimuw.cloudatlas.message.MessageSendMessage;
import pl.edu.mimuw.cloudatlas.model.Value;
import pl.edu.mimuw.cloudatlas.model.ValueContact;
import pl.edu.mimuw.cloudatlas.model.ValueSet;
import pl.edu.mimuw.cloudatlas.model.ZMI;
import pl.edu.mimuw.cloudatlas.message.Package;

public class ModuleCommunication extends Module implements Runnable{
	
	private final int SIZE_OF_BUFFER = 8000;
	private final int SIZE_OF_SEND_DATA = 7900;
	
	private InetAddress host;
	private int port;
	private HashMap<String, Pair<InetAddress, Integer> > myContacts; 
	private Set<ValueContact> fallbackContacts;
	private final static Logger LOGGER = Logger
			.getLogger(ModuleCommunication.class.getName());
	
	public ModuleCommunication(String host, int port, Agent a, Set<ValueContact> fallbackContacts) throws UnknownHostException{
		super("CommunicationModule", a);
		this.typeOfModule = ModuleType.MODULE_COMMUNICATION;
		this.myContacts = new HashMap<String, Pair<InetAddress, Integer> >();
		this.host = InetAddress.getByName(host);
		this.port = port;
		this.fallbackContacts = fallbackContacts;
	}
	
	public void addContact(String id, InetAddress host, int port) throws UnknownHostException{
		myContacts.put(id, new Pair<InetAddress, Integer>(host, port));
	}

	@Override
	public void run(){
		new Receiver(port).start();
	    
		while(true){
			
			synchronized (input) {
		        while (input.isEmpty())
					try {
						input.wait();
					} catch (InterruptedException e1) {
						LOGGER.log(Level.WARNING, "Interupt Exception - Receiver");
					}
		        
				if(!input.isEmpty()){
					try {
						processMessage();
					} catch (Exception e) {
						LOGGER.log(Level.WARNING, "Exception - Receiver - processMessage()");
					}
				}     
			}
		}
	}
	
	public InetAddress getHost(){
		return host;
	}
	
	public int getPort(){
		return port;
	}
	
	@Override
	public void processMessage() throws Exception {
		if(!input.isEmpty()){
			Message newMessage = readMessage();
			if(newMessage.getTypeOfMessage() == MessageType.SEND_MESSAGE){
								
				if(((MessageSendMessage)newMessage).getReceiver().equals(newMessage.getSender())){
					new Receiver().pass(((MessageSendMessage) newMessage));
				}
				else
				{
					Pair<InetAddress, Integer> receiver = myContacts.get(((MessageSendMessage)newMessage).getReceiver());
					if(receiver == null){
						receiver = findReceiver(((MessageSendMessage)newMessage).getReceiver());
					}
					
					if(receiver != null){
					
						InetAddress destHost = receiver.getFirst();
						int destPort = receiver.getSecond();
					    try {
					    	byte[] message;
					    	DatagramPacket packet;
					    	DatagramSocket dsocket;
					    	
					        // Get the internet address of the specified host
					        InetAddress address = destHost;
					    	
					    	// normal send a message (not concentrate on maximization usage of buffer)
					    	if(((MessageSendMessage)newMessage).getAttachedMessage().getTypeOfMessage()!=MessageType.GOSSIP_ZMI_PACKAGE){
					    		ByteArrayOutputStream bos = new ByteArrayOutputStream();
					    		ObjectOutput out = new ObjectOutputStream(bos);   
								out.writeObject((MessageSendMessage)newMessage);
								message = bos.toByteArray();
								bos.close();
			
						        // Initialize a datagram packet with data and address
						        packet = new DatagramPacket(message, message.length, address, destPort);
			
						        // Create a datagram socket, send the packet through it, close it.
						       dsocket = new DatagramSocket();
						        if(((MessageSendMessage)newMessage).getAttachedMessage().getTypeOfMessage()==MessageType.GOSSIP_INVITATION){
						        	((MessageGossipInvitation)(((MessageSendMessage)newMessage).getAttachedMessage())).setT1A(agent.getActualTime());
						        }
						        else if(((MessageSendMessage)newMessage).getAttachedMessage().getTypeOfMessage()==MessageType.GOSSIP_INVITATION_ANSWER){
						        	((MessageGossipInvitationAnswer)(((MessageSendMessage)newMessage).getAttachedMessage())).setT2B(agent.getActualTime());
						        }
						        
						        dsocket.send(packet);
						        dsocket.close();	
					    	}
					    	// maximization usage of buffer - only for large data
					    	else{
					    		MessagePackageZMI msg = (MessagePackageZMI)(((MessageSendMessage)newMessage).getAttachedMessage());
					    		ArrayList<Package> packagesToSend = msg.getZMIcertificate().getPackageZMI();
					    		int packagesSent = 0, allPackages = packagesToSend.size(), last = 0;
					    		byte[] tmpMessage;
					    		message = new byte[0];
					    		while(last < allPackages){
					    			tmpMessage = prepareMessage(packagesSent, last, packagesToSend, 
					    					(MessageSendMessage)newMessage, msg);
					    			if(tmpMessage.length > SIZE_OF_SEND_DATA){
					    				packet = new DatagramPacket(message, message.length, address, destPort);
					    				dsocket = new DatagramSocket();
					    				dsocket.send(packet);
								        dsocket.close();
								        packagesSent = last;
					    			}
					    			else{
					    				message = tmpMessage;
					    				last++;					    				
					    			}
					    		}
			    				packet = new DatagramPacket(message, message.length, address, destPort);
			    				dsocket = new DatagramSocket();
			    				dsocket.send(packet);
						        dsocket.close();
					    	}
					    } 
					    catch (Exception e) 
					    {
					    	LOGGER.log(Level.WARNING, "Exception - processMessage - send data");
					    }
					}
					else{
						LOGGER.log(Level.WARNING, "Cannot determine receiver. The message:"+
								((MessageSendMessage)newMessage).getAttachedMessage() +" will be discarded.");
					}
				}
			}
			else{
				throw new BadTypeOfMessageException("Communication module needs message typeof " + 
						MessageType.SEND_MESSAGE + ". Communication module receive: " +
						newMessage.getTypeOfMessage()+".");
			}
		}		
	}
	
	public byte[] prepareMessage(int start, int end, ArrayList<Package> packagesList, 
			MessageSendMessage msgSend, MessagePackageZMI msgZMI) throws IOException{
	
		MessageSendMessage msgSendMsg;
		MessagePackageZMI msgPack;
		String sender = msgSend.getSender();
		String moduleSender = msgSend.getModuleSender();
		String receiver = msgSend.getReceiver();
		String moduleReceiver = msgSend.getModuleReceiver();
		ArrayList<Package> packagesToSend = new ArrayList<Package>();
		for(int i=start; i<=end; i++)
			packagesToSend.add(packagesList.get(i));
		
		ZMICert cert = msgZMI.getZMIcertificate();
		String uid = msgZMI.getUID();
		int level = (int) (long) cert.getZoneCert().getLevel();
		
		long id = new Random().nextLong();
		ZMICert zmiCert = new ZMICert(id, cert.getCreation(), cert.getExpiry(), packagesToSend, cert.getZoneCert());
		zmiCert.generateSignature(agent.getCertificates().get(level-1).getPrivateKey(), true);
		// TEST ZMI CERT
		//zmiCert.setID(0);
		
		msgPack = new MessagePackageZMI(uid, sender, moduleSender, zmiCert);
		msgSendMsg = new MessageSendMessage(sender,receiver, moduleSender, moduleReceiver, msgPack);
		
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		ObjectOutput out = new ObjectOutputStream(bos);   
		out.writeObject(msgSendMsg);
		byte[] message = bos.toByteArray();
		bos.close();
		
		return message;
	}	


	public Pair<InetAddress, Integer> findReceiver(String receiver) throws UnknownHostException{
		Pair<InetAddress, Integer> answer = new Pair<InetAddress, Integer>(InetAddress.getByName("127.0.0.1"), 0);
		int numberOfLevels = agent.getNumberOfLevels();
		int searchOnLevel = 0;
		
		while(searchOnLevel < numberOfLevels){
			ZMI searchedZMI = agent.getZMIonLevel(searchOnLevel);
			if(findContact(searchedZMI, receiver, answer))
				return answer;
			else{
				ArrayList<ZMI> brothersZMIonThisLevel = agent.getSiblingsOnLevel(searchOnLevel);
				for(ZMI zmi : brothersZMIonThisLevel){
					if(findContact(zmi, receiver, answer)){
						myContacts.put(receiver, answer);
						return answer;
					}
				}				
			}
			searchOnLevel++;
		}
			
		if(fallbackContacts != null)
			for(ValueContact v : fallbackContacts){
				if(receiver.equals(v.getName().getName())){
					answer.setFirst(v.getAddress());
					answer.setSecond(v.getPort());
					return answer;
				}
			}
		
		return null;
	}
	
	public boolean findContact(ZMI zmi, String receiver, Pair<InetAddress, Integer> answer){
		ValueSet contacts = (ValueSet) zmi.getAttributes().get("contacts");
		for(Value v : contacts){
			ValueContact tmp = (ValueContact) v;
			if(receiver.equals(tmp.getName().getName())){
				answer.setFirst(tmp.getAddress());
				answer.setSecond(tmp.getPort());
				return true;
			}
		}		
		return false;
	}
	
	private class Receiver extends Thread{
		private int port;
		
		public Receiver(){
			
		}
		
		public Receiver(int port){
			this.port = port;
		}
		
		public void run(){
		    try {
		        // Create a socket to listen on the port.
		        @SuppressWarnings("resource")
				DatagramSocket dsocket = new DatagramSocket(port);

		        // Create a buffer to read datagrams into. If a
		        // packet is larger than this buffer, the
		        // excess will simply be discarded!
		        byte[] buffer = new byte[SIZE_OF_BUFFER];

		        // Create a packet to receive data into the buffer
		        DatagramPacket packet = new DatagramPacket(buffer, buffer.length);

		        // Now loop forever, waiting to receive packets and printing them.
		        while (true) {
		          // Wait to receive a datagram

		          dsocket.receive(packet);
		          
		          
		          long actualTime = agent.getActualTime();

		          // Convert the contents to a string, and display them
		          
		          ByteArrayInputStream bis = new ByteArrayInputStream(buffer);

		          ObjectInput in = null;
		          in = new ObjectInputStream(bis);

		          MessageSendMessage o = (MessageSendMessage) in.readObject();
		          
		          if(o.getAttachedMessage().getTypeOfMessage()==MessageType.GOSSIP_INVITATION){
		        	  ((MessageGossipInvitation)(o.getAttachedMessage())).setT1B(actualTime);
		          }
		          else if(o.getAttachedMessage().getTypeOfMessage()==MessageType.GOSSIP_INVITATION_ANSWER){
		        	  ((MessageGossipInvitationAnswer)(o.getAttachedMessage())).setT2A(actualTime);
		          }

		          bis.close();
		          if (in != null) {
		              in.close();
		          }
		          // Reset the length of the packet before reusing it.
		          packet.setLength(buffer.length);
		          
		          pass(o);
		        }
		      } catch (Exception e) {
		    	  LOGGER.log(Level.WARNING, "Exception - run()");
		      }
		}
		
		public void pass(MessageSendMessage toPass) throws BadTypeOfMessageException{
			String moduleToPass = toPass.getModuleReceiver();
			if(moduleToPass.equals("")){
				Message attachedMessage = toPass.getAttachedMessage();
				if(attachedMessage.getTypeOfMessage() == MessageType.TIMER_WAIT){
					passToSpecialModule(attachedMessage, ModuleType.MODULE_TIMER);
				}
				else if(attachedMessage.getTypeOfMessage() == MessageType.GOSSIP_INVITATION){
					passToSpecialModule(attachedMessage, ModuleType.MODULE_GOSSIPER);
				}
				else if(attachedMessage.getTypeOfMessage() == MessageType.GOSSIP_INVITATION_ANSWER){
					passToSpecialModule(attachedMessage, ModuleType.MODULE_GOSSIPER);
				}
				else if(attachedMessage.getTypeOfMessage() == MessageType.CONNECT_REQUEST){
					passToSpecialModule(attachedMessage, ModuleType.MODULE_CONNECT_RECIPIENT);
				}
				else if(attachedMessage.getTypeOfMessage() == MessageType.CONNECT_NEW_NODE){
					passToSpecialModule(attachedMessage, ModuleType.MODULE_CONNECT_RECIPIENT);
				}
				else if(attachedMessage.getTypeOfMessage() == MessageType.CONFIRM_RECEIVE){
					passToSpecialModule(attachedMessage, ModuleType.MODULE_CONNECT_RECIPIENT);
				}
				else if(attachedMessage.getTypeOfMessage() == MessageType.SEND_QUERY_MAGAZINE){
					passToSpecialModule(attachedMessage, ModuleType.MODULE_GOSSIPER);
				}
				
				else{
					throw new BadTypeOfMessageException("Receiver Module Communication: Unexpected type of message: " 
							+ attachedMessage.getTypeOfMessage());
				}
			}
			else{
				ArrayList<Module> myModules = getAgent().getModules();
				int numberOfModules = myModules.size();
				int i=0;
				boolean moduleNotFound = true;
				while(i < numberOfModules && moduleNotFound){
					if(myModules.get(i).getName().equals(moduleToPass)){
						moduleNotFound = false;
						myModules.get(i).messageToModule(toPass.getAttachedMessage());
					}
					i++;
				}
				if(moduleNotFound){
					LOGGER.log(Level.SEVERE, "Agent doesn't have a module name of :" + moduleToPass + ". The message will be discarded.");
				}
			}
		}
		
		public void passToSpecialModule(Message msg, ModuleType modType){
			ArrayList<Module> myModules = getAgent().getModules(); 
			int numberOfModules = myModules.size();
			int i=0;
			boolean moduleNotFound = true;
			while(i < numberOfModules && moduleNotFound){
				if(myModules.get(i).getModuleType() == modType){
					moduleNotFound = false;
					myModules.get(i).messageToModule(msg);
				}
				i++;
			}
			if(moduleNotFound){
				LOGGER.log(Level.SEVERE, "Agent doesn't have a module type of :" + modType + ". The message will be discarded.");
			}
		}
	}
}
