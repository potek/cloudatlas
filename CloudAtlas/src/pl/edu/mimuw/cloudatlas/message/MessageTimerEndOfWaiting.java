package pl.edu.mimuw.cloudatlas.message;

import java.io.IOException;
import java.io.Serializable;

@SuppressWarnings("serial")
public class MessageTimerEndOfWaiting extends Message implements Serializable{
	
	public MessageTimerEndOfWaiting(){
		super("", "", MessageType.TIMER_END_OF_WAITING);
	}
	
    private void writeObject(java.io.ObjectOutputStream stream)
            throws IOException {
    }

    private void readObject(java.io.ObjectInputStream stream)
            throws IOException, ClassNotFoundException {
    }
	
}
