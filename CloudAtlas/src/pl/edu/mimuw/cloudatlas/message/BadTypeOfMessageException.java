package pl.edu.mimuw.cloudatlas.message;

@SuppressWarnings("serial")
public class BadTypeOfMessageException extends Exception{
	public BadTypeOfMessageException(String message){
		super("Bad typeof message: " + message);
	}
}
