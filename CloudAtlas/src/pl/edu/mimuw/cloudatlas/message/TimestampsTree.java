package pl.edu.mimuw.cloudatlas.message;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;

import pl.edu.mimuw.cloudatlas.main.Agent;
import pl.edu.mimuw.cloudatlas.main.Pair;
import pl.edu.mimuw.cloudatlas.model.ValueTime;
import pl.edu.mimuw.cloudatlas.model.ZMI;

public class TimestampsTree implements Serializable {
	
	private static final long serialVersionUID = 3237760054214146501L;
	private ArrayList<ArrayList<Pair<String, Long>>> timestampsTree;
	
	public TimestampsTree(Agent agent, int startGossipOnLevel){
		ArrayList<Pair<ZMI, ArrayList<ZMI>>> hierarchyTree = agent.getHierarchyTree();
		timestampsTree = new ArrayList<ArrayList<Pair<String, Long>>>();
		
		int actualLevel = 0;
		
		while(actualLevel <= startGossipOnLevel){
			ArrayList<Pair<String, Long>> levelTimestamps = new ArrayList<Pair<String, Long>>();
			Pair<ZMI, ArrayList<ZMI>> level = hierarchyTree.get(actualLevel);
			
			String name = level.getFirst().getName();
			ValueTime valueTimestamp = (ValueTime) level.getFirst().getAttributes().get("timestamp");
			Long timestamp = (valueTimestamp == null) ? 0 : valueTimestamp.getValue();
			levelTimestamps.add(new Pair<String, Long>(name, timestamp));
			for(ZMI brother : level.getSecond()){
				name = brother.getName();
				valueTimestamp = (ValueTime) brother.getAttributes().get("timestamp");
				timestamp = (valueTimestamp == null) ? 0 : valueTimestamp.getValue();				
				levelTimestamps.add(new Pair<String, Long>(name, timestamp));
			}			
			timestampsTree.add(levelTimestamps);
			actualLevel++;
		}
	}	
	
	public void updateTimestampsTree(long dT){
		for(ArrayList<Pair<String, Long>> level : timestampsTree){
			for(Pair<String, Long> element : level){
				element.setSecond(element.getSecond()+dT);
			}
		}
	}
	
	public ArrayList<Pair<String, Long>> getLevel(int level){
		return timestampsTree.get(level);
	}
	
	public int size(){
		return timestampsTree.size();
	}
	
    private void writeObject(java.io.ObjectOutputStream stream)
            throws IOException {
    	stream.writeObject(timestampsTree.size());
    	for(ArrayList<Pair<String, Long>> element: timestampsTree){
    		stream.writeObject(element.size());
    		for(Pair<String, Long> pair : element){
    			stream.writeObject(pair.getFirst());
    			stream.writeObject(pair.getSecond());
    		}
    	}
    }

    private void readObject(java.io.ObjectInputStream stream)
            throws IOException, ClassNotFoundException {
    	int size = (int) stream.readObject();
    	timestampsTree = new ArrayList<ArrayList<Pair<String, Long>>>();
    	for(int i=0; i<size; i++){
    		int nmbElem = (int) stream.readObject();
    		ArrayList<Pair<String, Long>> tmp = new ArrayList<Pair<String, Long>>();
    		for(int j=0; j<nmbElem; j++){
    			String name = (String) stream.readObject();
    			Long timestamp = (Long) stream.readObject();
    			tmp.add(new Pair<String, Long>(name, timestamp));
    		}
    		timestampsTree.add(tmp);
    	}
    }
}
