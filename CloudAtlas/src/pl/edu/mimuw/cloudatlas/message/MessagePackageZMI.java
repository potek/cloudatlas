package pl.edu.mimuw.cloudatlas.message;

import java.io.IOException;
import java.io.Serializable;

import pl.edu.mimuw.cloudatlas.certification.ZMICert;

public class MessagePackageZMI extends Message implements Serializable{

	private static final long serialVersionUID = 3813404304498972247L;
	private String uniqueGossipId;
	private ZMICert zmiCertificate;
	
	public MessagePackageZMI(String uid, String sender, String moduleSender, ZMICert zmiCertificate){ 
		super(sender, moduleSender, MessageType.GOSSIP_ZMI_PACKAGE);
		this.uniqueGossipId = uid;
		this.zmiCertificate = zmiCertificate;
	}
	
	public String getUID(){
		return uniqueGossipId;
	}
	
	public ZMICert getZMIcertificate(){
		return zmiCertificate;
	}
	
    private void writeObject(java.io.ObjectOutputStream stream)
            throws IOException {
    	stream.writeObject(zmiCertificate);
    	stream.writeObject(uniqueGossipId);
    }

    private void readObject(java.io.ObjectInputStream stream)
            throws IOException, ClassNotFoundException {
    	zmiCertificate = (ZMICert) stream.readObject();
    	uniqueGossipId = (String) stream.readObject();
    }
}
