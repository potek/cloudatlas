package pl.edu.mimuw.cloudatlas.message;

import java.io.IOException;
import java.io.Serializable;

import pl.edu.mimuw.cloudatlas.model.ZMI;

@SuppressWarnings("serial")
public class MessageConnectNewNode extends Message implements Serializable{

	ZMI node;
	
	public MessageConnectNewNode(String sender, String moduleSender, ZMI node){
		super(sender, moduleSender, MessageType.CONNECT_NEW_NODE);
		this.node = node;
	}
	
	public ZMI getNode(){
		return node;
	}
		
    private void writeObject(java.io.ObjectOutputStream stream)
            throws IOException {
    	
    	stream.writeObject(node);
    }

    private void readObject(java.io.ObjectInputStream stream)
            throws IOException, ClassNotFoundException {
    	
    	node = (ZMI) stream.readObject();
    }
}
