package pl.edu.mimuw.cloudatlas.message;

import java.io.IOException;
import java.io.Serializable;

@SuppressWarnings("serial")
public abstract class Message implements Serializable{
	
	protected String sender;
	protected String moduleSender;
	
	public static enum MessageType {
		TIMER_WAIT, 
		TIMER_END_OF_WAITING, 
		SEND_MESSAGE,
		GOSSIP_INVITATION,
		GOSSIP_INVITATION_ANSWER,
		GOSSIP_ZMI_PACKAGE,
		GOSSIP_CONFIRM,
		CONNECT_REQUEST,
		ANSWER_ON_CONNECT_REQUEST,
		EXPORT_INFO,
		EXPORT_INFO_ANSWER,
		CONNECT_NEW_NODE,
		CONFIRM_RECEIVE,
		SEND_QUERY_MAGAZINE
	}
	
	protected MessageType typeOfMessage;
	
	public Message(String sender, String moduleSender, MessageType msgType){
		this.sender = sender;
		this.moduleSender = moduleSender;
		this.typeOfMessage = msgType;
	}
	
	public MessageType getTypeOfMessage(){
		return typeOfMessage;
	}
	
	public String getSender(){
		return sender;
	}
	
	public String getModuleSender(){
		return moduleSender;
	}
	
    private void writeObject(java.io.ObjectOutputStream stream)
            throws IOException {
    	stream.writeObject(sender);
    	stream.writeObject(moduleSender);
    	stream.writeObject(typeOfMessage);
    }

    private void readObject(java.io.ObjectInputStream stream)
            throws IOException, ClassNotFoundException {
    	sender = (String) stream.readObject();
    	moduleSender = (String) stream.readObject();
    	typeOfMessage = (MessageType) stream.readObject();
    }
}
