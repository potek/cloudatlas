package pl.edu.mimuw.cloudatlas.message;

import java.io.IOException;
import java.io.Serializable;

@SuppressWarnings("serial")
public class MessageConfirmReceive extends Message implements Serializable{
	
	public MessageConfirmReceive(String sender){
		super(sender, "", MessageType.CONFIRM_RECEIVE);
	}
	
    private void writeObject(java.io.ObjectOutputStream stream)
            throws IOException {
    }

    private void readObject(java.io.ObjectInputStream stream)
            throws IOException, ClassNotFoundException {
    }
	
}