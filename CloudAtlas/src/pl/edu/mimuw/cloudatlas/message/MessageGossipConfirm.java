package pl.edu.mimuw.cloudatlas.message;

import java.io.IOException;
import java.io.Serializable;

@SuppressWarnings("serial")
public class MessageGossipConfirm extends Message implements Serializable{

	private String uniqueGossipId;
	private long T2A, T2B;
	
	public MessageGossipConfirm(String uid, long T2A, long T2B){
		super("", "", MessageType.GOSSIP_CONFIRM);
		this.uniqueGossipId = uid;
		this.T2A = T2A;
		this.T2B = T2B;
	}
	
	public String getUID(){
		return uniqueGossipId;
	}
	
	public long getT2A(){
		return T2A;
	}
	
	public long getT2B(){
		return T2B;
	}
	
    private void writeObject(java.io.ObjectOutputStream stream)
            throws IOException {
    	stream.writeObject(uniqueGossipId);
    	stream.writeObject(T2A);
    	stream.writeObject(T2B);
    }

    private void readObject(java.io.ObjectInputStream stream)
            throws IOException, ClassNotFoundException {
    	uniqueGossipId = (String) stream.readObject();
    	T2A = (long) stream.readObject();
    	T2B = (long) stream.readObject();
    }
}
