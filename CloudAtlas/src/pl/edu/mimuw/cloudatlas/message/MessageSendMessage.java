package pl.edu.mimuw.cloudatlas.message;

import java.io.IOException;
import java.io.Serializable;

@SuppressWarnings("serial")
public class MessageSendMessage extends Message implements Serializable{

	private String receiver;
	private String moduleReceiver;
	private Message attachedMessage;
	
	public MessageSendMessage(String sender, String receiver, String moduleSender, String moduleReceiver, Message message){
		super(sender, moduleSender, MessageType.SEND_MESSAGE);
		this.receiver = receiver;
		this.attachedMessage = message;
		this.moduleReceiver = moduleReceiver;
	}
	
	public String getReceiver(){
		return receiver;
	}
	
	public String getModuleReceiver(){
		return moduleReceiver;
	}
	
	public Message getAttachedMessage(){
		return attachedMessage;
	}
	
	public String toString(){
		return "Sender: " + sender + "\nReceiver: " + receiver + "\nMS: " + moduleSender + "\nMR: "+moduleReceiver + 
				"\nAttachedMessage:\n" + attachedMessage.toString();
	}
	
    private void writeObject(java.io.ObjectOutputStream stream)
            throws IOException {
    	stream.writeObject(receiver);
    	stream.writeObject(moduleReceiver);
    	stream.writeObject(attachedMessage);
    }

    private void readObject(java.io.ObjectInputStream stream)
            throws IOException, ClassNotFoundException {

    	receiver = (String) stream.readObject();
    	moduleReceiver = (String) stream.readObject();
    	attachedMessage = (Message) stream.readObject();
    }
	
}