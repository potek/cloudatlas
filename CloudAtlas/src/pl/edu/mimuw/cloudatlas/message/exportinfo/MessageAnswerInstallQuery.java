package pl.edu.mimuw.cloudatlas.message.exportinfo;

public class MessageAnswerInstallQuery extends MessageExportInfoAnswer{
	
	Boolean result;
	private static final long serialVersionUID = -943351616133205689L;

	public MessageAnswerInstallQuery(Boolean result){
		super(ExportInfoAnswerType.INSTALL_QUERY);
		this.result = result;
	}

	public Boolean getResult() {
		return result;
	}
}