package pl.edu.mimuw.cloudatlas.message.exportinfo;

import java.util.concurrent.LinkedBlockingQueue;

import pl.edu.mimuw.cloudatlas.certification.ClientCert;
import pl.edu.mimuw.cloudatlas.main.Agent;
import pl.edu.mimuw.cloudatlas.model.ZMI;

public class MessageExInfoGetZMI extends MessageExportInfo {

	private static final long serialVersionUID = -5416680276427197012L;

	private String zoneName;
	private ClientCert clientCert;

	public MessageExInfoGetZMI(
			LinkedBlockingQueue<MessageExportInfoAnswer> queueToAnswer,
			String zoneName, ClientCert clientCert) {
		super(ExportInfoType.GET_ZMI, queueToAnswer);
		this.zoneName = zoneName;
		this.setClientCert(clientCert);
	}

	@Override
	public MessageExportInfoAnswer process(Agent a) {
		ZMI zmi = a.getExportZMI(zoneName, clientCert);
		return new MessageAnswerGetZMI(zmi);
	}

	public ClientCert getClientCert() {
		return clientCert;
	}

	public void setClientCert(ClientCert clientCert) {
		this.clientCert = clientCert;
	}
}
