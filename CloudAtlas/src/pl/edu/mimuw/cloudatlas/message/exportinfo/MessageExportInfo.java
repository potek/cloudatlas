package pl.edu.mimuw.cloudatlas.message.exportinfo;

import java.util.concurrent.LinkedBlockingQueue;

import pl.edu.mimuw.cloudatlas.main.Agent;
import pl.edu.mimuw.cloudatlas.message.Message;

public abstract class MessageExportInfo extends Message {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3301543927998238206L;

	public static enum ExportInfoType {
		GET_ZMI, ADD_OR_CHANGE_ATTRIBUTE, REMOVE_ATTRIBUTE, GET_ATTRIBUTE, INSTALL_QUERY, UNINSTALL_QUERY, SET_FALLBACKCONTACTS, GET_KNOWN_ZONES
	}

	protected ExportInfoType typeOfExportInfoMessage;
	private LinkedBlockingQueue<MessageExportInfoAnswer> queueToAnswer;

	public MessageExportInfo(ExportInfoType exportInfoType,
			LinkedBlockingQueue<MessageExportInfoAnswer> queueToAnswer) {
		super("RMI_question", "", MessageType.EXPORT_INFO);
		this.typeOfExportInfoMessage = exportInfoType;
		this.queueToAnswer = queueToAnswer;
	}

	public ExportInfoType getTypeOfExportInfoMessage() {
		return typeOfExportInfoMessage;
	}

	public LinkedBlockingQueue<MessageExportInfoAnswer> getQueueToAnswer() {
		return queueToAnswer;
	}

	public abstract MessageExportInfoAnswer process(Agent a);
}
