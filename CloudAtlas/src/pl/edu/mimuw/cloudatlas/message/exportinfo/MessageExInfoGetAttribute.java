package pl.edu.mimuw.cloudatlas.message.exportinfo;

import java.util.concurrent.LinkedBlockingQueue;

import pl.edu.mimuw.cloudatlas.main.Agent;
import pl.edu.mimuw.cloudatlas.model.Value;

public class MessageExInfoGetAttribute extends MessageExportInfo {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8702495885783631640L;
	private String zoneName;
	private String attrName;

	public MessageExInfoGetAttribute(
			LinkedBlockingQueue<MessageExportInfoAnswer> queueToAnswer,
			String zoneName,
			String attrName) {
		super(ExportInfoType.GET_ATTRIBUTE, queueToAnswer);
		this.zoneName = zoneName;
		this.attrName = attrName;
	}

	@Override
	public MessageExportInfoAnswer process(Agent a) {
		Value v = a.getAttributeOfZone(zoneName, attrName);
		return new MessageAnswerGetAttribute(v);
	}
}