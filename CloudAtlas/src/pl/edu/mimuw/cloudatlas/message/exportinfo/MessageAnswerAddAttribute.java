package pl.edu.mimuw.cloudatlas.message.exportinfo;

public class MessageAnswerAddAttribute extends MessageExportInfoAnswer{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2727115471156383038L;

	public MessageAnswerAddAttribute(){
		super(ExportInfoAnswerType.ADD_OR_CHANGE_ATTRIBUTE);
	}
}