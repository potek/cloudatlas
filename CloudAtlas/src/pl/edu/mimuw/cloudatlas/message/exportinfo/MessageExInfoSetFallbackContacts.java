package pl.edu.mimuw.cloudatlas.message.exportinfo;

import java.util.Set;
import java.util.concurrent.LinkedBlockingQueue;

import pl.edu.mimuw.cloudatlas.certification.ClientCert;
import pl.edu.mimuw.cloudatlas.main.Agent;
import pl.edu.mimuw.cloudatlas.model.ValueContact;

public class MessageExInfoSetFallbackContacts extends MessageExportInfo {
	private static final long serialVersionUID = -7472093704517620978L;

	private Set<ValueContact> contacts;
	private ClientCert clientCert;

	public MessageExInfoSetFallbackContacts(
			LinkedBlockingQueue<MessageExportInfoAnswer> queueToAnswer,
			Set<ValueContact> contacts, ClientCert clientCert) {
		super(ExportInfoType.SET_FALLBACKCONTACTS, queueToAnswer);
		this.contacts = contacts;
		this.setClientCert(clientCert);
	}

	public Set<ValueContact> getContacts() {
		return contacts;
	}

	@Override
	public MessageExportInfoAnswer process(Agent a) {
		a.setFallbackContacts(this.contacts);
		return new MessageAnswerSetFallbackContacts();
	}

	public ClientCert getClientCert() {
		return clientCert;
	}

	public void setClientCert(ClientCert clientCert) {
		this.clientCert = clientCert;
	}
}