package pl.edu.mimuw.cloudatlas.message.exportinfo;

public class MessageAnswerRemoveAttribute extends MessageExportInfoAnswer{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8508592255676227881L;

	public MessageAnswerRemoveAttribute(){
		super(ExportInfoAnswerType.REMOVE_ATTRIBUTE);
	}
}