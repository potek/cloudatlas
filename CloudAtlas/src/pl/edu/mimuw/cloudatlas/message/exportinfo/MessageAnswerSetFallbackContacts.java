package pl.edu.mimuw.cloudatlas.message.exportinfo;

public class MessageAnswerSetFallbackContacts extends MessageExportInfoAnswer{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8540652948190990737L;

	public MessageAnswerSetFallbackContacts(){
		super(ExportInfoAnswerType.SET_FALLCONTEST);
	}
}