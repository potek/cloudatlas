package pl.edu.mimuw.cloudatlas.message.exportinfo;

import java.util.concurrent.LinkedBlockingQueue;

import pl.edu.mimuw.cloudatlas.certification.ClientCert;
import pl.edu.mimuw.cloudatlas.main.Agent;
import pl.edu.mimuw.cloudatlas.model.Value;

public class MessageExInfoAddAttribute extends MessageExportInfo {

	private static final long serialVersionUID = -1509159388466429717L;
	private String name;
	private Value value;
	private ClientCert clientCert;

	public MessageExInfoAddAttribute(
			LinkedBlockingQueue<MessageExportInfoAnswer> queueToAnswer,
			String name, Value value, ClientCert clientCert) {
		super(ExportInfoType.ADD_OR_CHANGE_ATTRIBUTE, queueToAnswer);
		this.name = name;
		this.value = value;
		this.setClientCert(clientCert);
	}

	public String getName() {
		return name;
	}

	public Value getValue() {
		return value;
	}

	@Override
	public MessageExportInfoAnswer process(Agent a) {
		a.addOrChangeValue(name, value, clientCert);
		return new MessageAnswerAddAttribute();
	}

	public ClientCert getClientCert() {
		return clientCert;
	}

	public void setClientCert(ClientCert clientCert) {
		this.clientCert = clientCert;
	}

}