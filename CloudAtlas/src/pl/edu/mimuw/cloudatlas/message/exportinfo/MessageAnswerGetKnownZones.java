package pl.edu.mimuw.cloudatlas.message.exportinfo;

import java.util.Set;

public class MessageAnswerGetKnownZones extends MessageExportInfoAnswer {
	private static final long serialVersionUID = -8896200961717779288L;

	Set<String> knownZones;
	
	public MessageAnswerGetKnownZones(Set<String> knownZones) {
		super(ExportInfoAnswerType.GET_KNOWN_ZONES);
		// TODO Auto-generated constructor stub
		this.knownZones = knownZones;
	}
	
	public Set<String> getKnownZones() {
		return knownZones;
	}

}
