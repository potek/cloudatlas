package pl.edu.mimuw.cloudatlas.message.exportinfo;

import java.util.concurrent.LinkedBlockingQueue;

import pl.edu.mimuw.cloudatlas.certification.AFCert;
import pl.edu.mimuw.cloudatlas.main.Agent;

public class MessageExInfoUninstallQuery extends MessageExportInfo {
	private static final long serialVersionUID = -7018518093191278943L;

	private AFCert signedQueryUn;

	public MessageExInfoUninstallQuery(
			LinkedBlockingQueue<MessageExportInfoAnswer> queueToAnswer,
			AFCert squ) {
		super(ExportInfoType.UNINSTALL_QUERY, queueToAnswer);
		this.signedQueryUn = squ;
	}

	public AFCert getQuery() {
		return signedQueryUn;
	}

	@Override
	public MessageExportInfoAnswer process(Agent a) {
		Boolean ret = a.uninstallQuery(signedQueryUn);
		return new MessageAnswerUninstallQuery(ret);
	}

}