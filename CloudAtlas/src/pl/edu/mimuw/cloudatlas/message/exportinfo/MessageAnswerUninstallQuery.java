package pl.edu.mimuw.cloudatlas.message.exportinfo;

public class MessageAnswerUninstallQuery extends MessageExportInfoAnswer{

	private static final long serialVersionUID = -3015253364764732147L;

	private Boolean result;
	
	public MessageAnswerUninstallQuery(Boolean result){
		super(ExportInfoAnswerType.UNINSTALL_QUERY);
		this.result = result;
	}

	public Boolean getResult() {
		return result;
	}
}