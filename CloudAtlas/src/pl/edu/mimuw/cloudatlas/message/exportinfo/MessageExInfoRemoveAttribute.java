package pl.edu.mimuw.cloudatlas.message.exportinfo;

import java.util.concurrent.LinkedBlockingQueue;

import pl.edu.mimuw.cloudatlas.main.Agent;

public class MessageExInfoRemoveAttribute extends MessageExportInfo{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 9031133764550877310L;
	private String name;
	
	public MessageExInfoRemoveAttribute(LinkedBlockingQueue<MessageExportInfoAnswer> queueToAnswer, String name){
		super(ExportInfoType.REMOVE_ATTRIBUTE, queueToAnswer);	
		this.name = name;
	}
	
	public String getName(){
		return name;
	}

	@Override
	public MessageExportInfoAnswer process(Agent a) {
		a.removeAttribute(name);
		return new MessageAnswerRemoveAttribute();
	}

}