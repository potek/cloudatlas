package pl.edu.mimuw.cloudatlas.message.exportinfo;

import java.util.concurrent.LinkedBlockingQueue;

import pl.edu.mimuw.cloudatlas.certification.ClientCert;
import pl.edu.mimuw.cloudatlas.main.Agent;

public class MessageExInfoGetKnownZones extends MessageExportInfo {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1261810618616060L;
	private ClientCert clientCert;
	
	public MessageExInfoGetKnownZones(LinkedBlockingQueue<MessageExportInfoAnswer> queueToAnswer, ClientCert clientCert) {
		super(ExportInfoType.GET_KNOWN_ZONES, queueToAnswer);
		this.setClientCert(clientCert);
	}


	@Override
	public MessageExportInfoAnswer process(Agent a) {
		return new MessageAnswerGetKnownZones(a.getKnownZones());
	}


	public ClientCert getClientCert() {
		return clientCert;
	}


	public void setClientCert(ClientCert clientCert) {
		this.clientCert = clientCert;
	}

}
