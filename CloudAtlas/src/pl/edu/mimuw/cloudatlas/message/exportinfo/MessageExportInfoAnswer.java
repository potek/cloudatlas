package pl.edu.mimuw.cloudatlas.message.exportinfo;

import pl.edu.mimuw.cloudatlas.message.Message;

public abstract class MessageExportInfoAnswer extends Message {
		
	/**
	 * 
	 */
	private static final long serialVersionUID = -6792137840695018075L;

	public static enum ExportInfoAnswerType {
		GET_ZMI,
		ADD_OR_CHANGE_ATTRIBUTE,
		REMOVE_ATTRIBUTE,
		GET_ATTRIBUTE,
		INSTALL_QUERY,
		UNINSTALL_QUERY,
		SET_FALLCONTEST,
		GET_KNOWN_ZONES
	}
	
	protected ExportInfoAnswerType typeOfExportInfoAnswerMessage;
	
	public MessageExportInfoAnswer(ExportInfoAnswerType exportInfoType){
		super("RMI_question", "", MessageType.EXPORT_INFO_ANSWER);
		this.typeOfExportInfoAnswerMessage = exportInfoType;
	}	
	
	public ExportInfoAnswerType getTypeOfExportInfoAnswerMessage(){
		return typeOfExportInfoAnswerMessage;
	}
	
}
