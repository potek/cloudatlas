package pl.edu.mimuw.cloudatlas.message.exportinfo;

import java.util.concurrent.LinkedBlockingQueue;

import pl.edu.mimuw.cloudatlas.certification.AFCert;
import pl.edu.mimuw.cloudatlas.main.Agent;

public class MessageExInfoInstallQuery extends MessageExportInfo {
	
	private static final long serialVersionUID = 3114505433028796966L;
	private AFCert query;
	private String zoneName;

	public MessageExInfoInstallQuery(
			LinkedBlockingQueue<MessageExportInfoAnswer> queueToAnswer,
			AFCert query, String zoneName) {
		super(ExportInfoType.INSTALL_QUERY, queueToAnswer);
		this.query = query;
		this.zoneName = zoneName;
	}

	public AFCert getSignedQuery() {
		return query;
	}

	@Override
	public MessageExportInfoAnswer process(Agent a) {
		return new MessageAnswerInstallQuery(a.addQuery(query, zoneName));
	}

	public String getZoneName() {
		return zoneName;
	}

	public void setZoneName(String zoneName) {
		this.zoneName = zoneName;
	}
 
}