package pl.edu.mimuw.cloudatlas.message.exportinfo;

import pl.edu.mimuw.cloudatlas.model.ZMI;

public class MessageAnswerGetZMI extends MessageExportInfoAnswer{
	
	private static final long serialVersionUID = 1070459949999496194L;
	
	private ZMI zmi;
	
	public MessageAnswerGetZMI(ZMI zmi){
		super(ExportInfoAnswerType.GET_ZMI);
		this.zmi = zmi;
	}
	
	public ZMI getZMI(){
		return zmi;
	}
}