package pl.edu.mimuw.cloudatlas.message.exportinfo;

import pl.edu.mimuw.cloudatlas.model.Value;

public class MessageAnswerGetAttribute extends MessageExportInfoAnswer{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -144042218518291900L;
	private Value value;
	
	public MessageAnswerGetAttribute(Value v){
		super(ExportInfoAnswerType.GET_ATTRIBUTE);
		this.value = v;
	}
	
	public Value getAttribute(){
		return value;
	}
}