package pl.edu.mimuw.cloudatlas.message;

import java.io.IOException;
import java.io.Serializable;

@SuppressWarnings("serial")
public class MessageTimerWait extends Message implements Serializable{

	private long waitMS;
	
	public MessageTimerWait(long waitMS, String sender, String moduleSender){
		super(sender, moduleSender, MessageType.TIMER_WAIT);
		this.waitMS = waitMS;
	}

	public String toString(){
		return ("MessageTimerWait: "+waitMS + " " + sender + " " + moduleSender);
	}
	
	public String getSender(){
		return sender;
	}
	
	public String getModuleSender(){
		return moduleSender;
	}
	
	public long getWaitMS(){
		return waitMS;
	}
	
    private void writeObject(java.io.ObjectOutputStream stream)
            throws IOException {
    	stream.writeObject(waitMS);
    }

    private void readObject(java.io.ObjectInputStream stream)
            throws IOException, ClassNotFoundException {

    	waitMS = (long) stream.readObject();
    }
}

