package pl.edu.mimuw.cloudatlas.message;

import java.io.IOException;
import java.io.Serializable;

@SuppressWarnings("serial")
public class MessageGossipInvitation extends Message implements Serializable{

	private int level;
	private String uniqueGossipId;
	private TimestampsTree timestampsTree;
	private long T1A, T1B;
	
	public MessageGossipInvitation(int level, String sender, String moduleSender, String uid, TimestampsTree tt){
		super(sender, moduleSender, MessageType.GOSSIP_INVITATION);
		this.level = level;
		this.uniqueGossipId = uid;
		this.timestampsTree = tt;
	}
	
	public int getLevel(){
		return level;
	}
	
	public String getUID(){
		return uniqueGossipId;
	}
	
	public TimestampsTree getTimestampsTree(){
		return timestampsTree;
	}
	
	public void setT1A(long T1A){
		this.T1A = T1A;
	}
	
	public long getT1A(){
		return T1A;
	}
	
	public void setT1B(long T1B){
		this.T1B = T1B;
	}
	
	public long getT1B(){
		return T1B;
	}
	
    private void writeObject(java.io.ObjectOutputStream stream)
            throws IOException {
    	stream.writeObject(level);
    	stream.writeObject(uniqueGossipId);
    	stream.writeObject(timestampsTree);
    	stream.writeObject(T1A);
    	stream.writeObject(T1B);
    }

    private void readObject(java.io.ObjectInputStream stream)
            throws IOException, ClassNotFoundException {
    	level = (int) stream.readObject();
    	uniqueGossipId = (String) stream.readObject();
    	timestampsTree = (TimestampsTree) stream.readObject();
    	T1A = (long) stream.readObject();
    	T1B = (long) stream.readObject();
    }
}
