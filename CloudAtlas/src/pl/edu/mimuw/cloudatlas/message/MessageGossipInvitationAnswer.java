package pl.edu.mimuw.cloudatlas.message;

import java.io.IOException;
import java.io.Serializable;

public class MessageGossipInvitationAnswer extends Message implements Serializable{

	private static final long serialVersionUID = 6910875526669868900L;
	private String uniqueGossipId;
	private long T1A, T1B, T2B, T2A;
	private TimestampsTree timestampsTree;
	
	public MessageGossipInvitationAnswer(String uid, String moduleSender, TimestampsTree tt, long T1A, long T1B){
		super("", moduleSender, MessageType.GOSSIP_INVITATION_ANSWER);
		this.uniqueGossipId = uid;
		this.timestampsTree = tt;
		this.moduleSender = moduleSender;
		this.T1A = T1A;
		this.T1B = T1B;
	}
	
	public String getUID(){
		return uniqueGossipId;
	}
	
	public TimestampsTree getTimestampsTree(){
		return timestampsTree;
	}
	
	public void setT2B(long T2B){
		this.T2B = T2B;
	}
	
	public void setT2A(long T2A){
		this.T2A = T2A;
	}
	
	public long getT1A(){
		return T1A;
	}
	
	public long getT2A(){
		return T2A;
	}
	
	public long getT1B(){
		return T1B;
	}
	
	public long getT2B(){
		return T2B;
	}
	
    private void writeObject(java.io.ObjectOutputStream stream)
            throws IOException {
    	stream.writeObject(uniqueGossipId);
    	stream.writeObject(timestampsTree);
    	stream.writeObject(T1A);
    	stream.writeObject(T2A);
    	stream.writeObject(T1B);
    	stream.writeObject(T2B);
    }

    private void readObject(java.io.ObjectInputStream stream)
            throws IOException, ClassNotFoundException {
    	uniqueGossipId = (String) stream.readObject();
    	timestampsTree = (TimestampsTree) stream.readObject();
    	T1A = (long) stream.readObject();
    	T2A = (long) stream.readObject();
    	T1B = (long) stream.readObject();
    	T2B = (long) stream.readObject();
    }
}
