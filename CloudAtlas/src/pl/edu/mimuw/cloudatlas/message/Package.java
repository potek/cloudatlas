package pl.edu.mimuw.cloudatlas.message;

import java.io.IOException;
import java.io.Serializable;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;

import pl.edu.mimuw.cloudatlas.model.Attribute;
import pl.edu.mimuw.cloudatlas.model.Value;

public class Package implements Serializable {

	private static final long serialVersionUID = 1065455975648683179L;

	HashMap<Attribute, Value> elements;
	String name;

	public Package(String name) {
		this.name = name;
		this.elements = new HashMap<Attribute, Value>();
	}

	public void addElement(Entry<Attribute, Value> e) {
		elements.put(e.getKey(), e.getValue());
	}

	public String getName() {
		return name;
	}

	public HashMap<Attribute, Value> getElements() {
		return elements;
	}

	private void writeObject(java.io.ObjectOutputStream stream)
			throws IOException {
		stream.writeObject(name);
		stream.writeObject(elements.size());
		for (Entry<Attribute, Value> entry : elements.entrySet()) {
			stream.writeObject(entry.getKey());
			stream.writeObject(entry.getValue());
		}
	}

	private void readObject(java.io.ObjectInputStream stream)
			throws IOException, ClassNotFoundException {
		name = (String) stream.readObject();
		elements = new HashMap<Attribute, Value>();
		int size = (int) stream.readObject();
		for (int i = 0; i < size; i++) {
			Attribute key = (Attribute) stream.readObject();
			Value value = (Value) stream.readObject();
			elements.put(key, value);
		}
	}

	@Override
	public String toString() {
		String answer;
		answer = name + " = {";
		for (Entry<Attribute, Value> e : elements.entrySet()) {
			answer += e.getKey().toString() + ", ";
		}
		answer += "}";
		return answer;
	}

	public String serialize() {
		StringBuilder sb = new StringBuilder();
		SortedSet<Map.Entry<Attribute, Value>> sortedset = new TreeSet<Map.Entry<Attribute, Value>>(
				new Comparator<Map.Entry<Attribute, Value>>() {
					@Override
					public int compare(Map.Entry<Attribute, Value> e1,
							Map.Entry<Attribute, Value> e2) {
						return e1.getKey().getName()
								.compareTo(e2.getKey().getName());
					}
				});

		sortedset.addAll(elements.entrySet());

		for(Map.Entry<Attribute, Value> entry : sortedset) {
			sb.append(entry.getKey().toString());
		}
		return sb.toString();
	}
}
