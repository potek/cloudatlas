package pl.edu.mimuw.cloudatlas.message;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;

import pl.edu.mimuw.cloudatlas.certification.ZoneCert;
import pl.edu.mimuw.cloudatlas.main.Pair;
import pl.edu.mimuw.cloudatlas.model.ZMI;

public class MessageAnswerOnConnectRequest extends Message implements Serializable{

	private static final long serialVersionUID = 4969536352601417803L;
	ArrayList<Pair<ZMI, ArrayList<ZMI>>> treeOfHierarchy;
	TypeOfAnswer typeOfAnswer;
	ZoneCert zoneCert;
	
	public enum TypeOfAnswer {
		CONFIRM,
		REJECT_EXPIRED,
		REJECT_KEY_MISMATCH,
		CONFIRM_BUT_UNAUTHORIZED_HOST
	}
	
	public MessageAnswerOnConnectRequest(TypeOfAnswer typeOfAnswer){
		super("", "", MessageType.ANSWER_ON_CONNECT_REQUEST);
		this.treeOfHierarchy = new ArrayList<Pair<ZMI,ArrayList<ZMI>>>();
		this.typeOfAnswer = typeOfAnswer;
		this.zoneCert = null;
	}
	
	public MessageAnswerOnConnectRequest(ArrayList<Pair<ZMI, ArrayList<ZMI>>> treeOfHierarchy, ZoneCert zoneCert){
		super("", "", MessageType.ANSWER_ON_CONNECT_REQUEST);
		this.treeOfHierarchy = treeOfHierarchy;
		typeOfAnswer = TypeOfAnswer.CONFIRM;
		this.zoneCert = zoneCert;
	}
	
	public ArrayList<Pair<ZMI, ArrayList<ZMI>>> getTreeOfHierarchy(){
		return treeOfHierarchy;
	}
	
	public void setAsInvalid(){
		this.typeOfAnswer = TypeOfAnswer.CONFIRM_BUT_UNAUTHORIZED_HOST;
	}
		
	public TypeOfAnswer getTypeOfAnswer(){
		return typeOfAnswer;
	}
	
	public ZoneCert getZoneCert(){
		return zoneCert;
	}
	
    private void writeObject(java.io.ObjectOutputStream stream)
            throws IOException {
    	
    	stream.writeObject(typeOfAnswer);
    	stream.writeObject(zoneCert);
    	stream.writeObject(treeOfHierarchy.size());
    	for(Pair<ZMI, ArrayList<ZMI>> p : treeOfHierarchy){
    		stream.writeObject(p.getFirst());
    		stream.writeObject(p.getSecond().size());
    		for(ZMI zmi: p.getSecond()){
    			stream.writeObject(zmi);
    		}
    	}
    }

    private void readObject(java.io.ObjectInputStream stream)
            throws IOException, ClassNotFoundException {
    	
    	typeOfAnswer = (TypeOfAnswer) stream.readObject();
    	zoneCert = (ZoneCert) stream.readObject();
    	treeOfHierarchy = new ArrayList<Pair<ZMI, ArrayList<ZMI>>>();
    	int size = (int) stream.readObject();
    	for(int i=0; i<size; i++){
    		ZMI first = (ZMI) stream.readObject();
    		int numBro = (int) stream.readObject();
    		ArrayList<ZMI> brothers = new ArrayList<ZMI>();
    		for(int j=0; j<numBro; j++){
    			brothers.add((ZMI) stream.readObject());
    		}
    		treeOfHierarchy.add(new Pair<ZMI, ArrayList<ZMI>>(first, brothers));
    	}
    }
    
    @Override
    public String toString(){
    	String answer = "";
    	int i=0;
    	for(Pair<ZMI, ArrayList<ZMI>> p: treeOfHierarchy){
    		answer += "Level: " + i + "\n";
    		answer += "\tMain: " + p.getFirst() + "\n";
    		answer += "\tSiblings: \n";
    		int j=1;
    		for(ZMI zmi: p.getSecond()){
    			answer += "\t\tSib" + j + ": " + zmi + "\n";
    			j++;
    		}
    		i++;
    		answer += "\n----------------------------------------------\n\n";
    	}
    	
    	return answer;
    }
}
