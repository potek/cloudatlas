package pl.edu.mimuw.cloudatlas.message;

import java.io.IOException;
import java.io.Serializable;
import java.net.InetAddress;
import java.net.UnknownHostException;

import pl.edu.mimuw.cloudatlas.certification.ZoneCert;

@SuppressWarnings("serial")
public class MessageConnectRequest extends Message implements Serializable{

	InetAddress host;
	int port;
	ZoneCert zoneCert;
	
	public MessageConnectRequest(String sender, InetAddress host, int port) throws UnknownHostException{
		super(sender, "", MessageType.CONNECT_REQUEST);
		this.host = host;
		this.port = port;
	}
	
	public InetAddress getHost(){
		return host;
	}
	
	public int getPort(){
		return port;
	}
	
	public ZoneCert getZoneCert(){
		return zoneCert;
	}
	
	public void setZoneCert(ZoneCert zoneCert){
		this.zoneCert = zoneCert;
	}
		
    private void writeObject(java.io.ObjectOutputStream stream)
            throws IOException {
    	stream.writeObject(host);
    	stream.writeObject(port);
    	stream.writeObject(zoneCert);
    }

    private void readObject(java.io.ObjectInputStream stream)
            throws IOException, ClassNotFoundException {
    	host = (InetAddress) stream.readObject();
    	port = (int) stream.readObject();
    	zoneCert = (ZoneCert) stream.readObject();
    }
}
