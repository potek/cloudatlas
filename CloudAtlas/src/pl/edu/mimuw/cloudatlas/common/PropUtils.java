package pl.edu.mimuw.cloudatlas.common;

import java.io.IOException;
import java.util.Properties;

public class PropUtils {
	public static String getPropOrNull(Properties prop, String name) {
		if (prop.containsKey(name)) {
			String val = prop.getProperty(name);
			if (val.trim() == "") {
				return null;
			}
			return val;
		} else {
			return null;
		}
	}
	public static String getPropOrThrow(Properties prop, String name)
			throws IOException {
		if (prop.containsKey(name)) {
			String val = prop.getProperty(name); 
			if (val.trim() == "") {
				throw new IOException("ERROR: bad config file. " + name
						+ " is empty");
			}
			return val;
		} else {
			throw new IOException("ERROR: bad config file. No " + name
					+ " property in conf file"); 
		}
	}
}

