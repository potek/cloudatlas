package pl.edu.mimuw.cloudatlas.common;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.xml.bind.DatatypeConverter;

public class CryptoManager {
	private final static String ENCRYPTION_ALGORITHM = "RSA";
	private final static int NUM_KEY_BITS = 1024;
	
	private final static Logger LOGGER = Logger.getLogger(CryptoManager.class.getName());

	public static PrivateKey getPrivateKey(String filename) throws IOException,
			NoSuchAlgorithmException, InvalidKeySpecException {
		File f = new File(filename);
		try (FileInputStream fis = new FileInputStream(f);
				DataInputStream dis = new DataInputStream(fis);) {
			byte[] keyBytes = new byte[(int) f.length()];

			dis.readFully(keyBytes);
			dis.close();

			PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(keyBytes);
			KeyFactory kf = KeyFactory.getInstance(ENCRYPTION_ALGORITHM);
			return kf.generatePrivate(spec);
		}
	}

	public static PublicKey getPublicKey(String filename) throws IOException,
			NoSuchAlgorithmException, InvalidKeySpecException {
		File f = new File(filename);
		try (FileInputStream fis = new FileInputStream(f);
				DataInputStream dis = new DataInputStream(fis);) {
			byte[] keyBytes = new byte[(int) f.length()];

			dis.readFully(keyBytes);
			dis.close();

			X509EncodedKeySpec spec = new X509EncodedKeySpec(keyBytes);
			KeyFactory kf = KeyFactory.getInstance(ENCRYPTION_ALGORITHM);
			return kf.generatePublic(spec);
		}
	}

	public static void writeKey(String filename, Key key) throws IOException {
		byte[] key_byte = key.getEncoded();
		try (FileOutputStream keyfos = new FileOutputStream(filename)) {
			keyfos.write(key_byte);
			keyfos.close();
		}
	}

	public static KeyPair genKey() throws NoSuchAlgorithmException {
		KeyPairGenerator keyGenerator = KeyPairGenerator
				.getInstance(ENCRYPTION_ALGORITHM);
		keyGenerator.initialize(NUM_KEY_BITS);
		KeyPair keyPair = keyGenerator.generateKeyPair();

		return keyPair;
	}

	public static byte[] toByte(Serializable o) throws IOException {

		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		ObjectOutput out = null;
		try {
			bos.reset();
			out = new ObjectOutputStream(bos);
			out.writeObject(o);
			return bos.toByteArray();
		} finally {
			try {
				if (out != null) {
					out.close();
				}
			} catch (IOException ex) {
				// ignore close exception
			}
			try {
				bos.close();
			} catch (IOException ex) {
				// ignore close exception
			}
		}
	}

	public static byte[] calcHash(byte[] input, String DIGEST_ALGORITHM) {
		byte[] digest = null;
		try {
			MessageDigest digestGenerator = MessageDigest
					.getInstance(DIGEST_ALGORITHM);
			digest = digestGenerator.digest(input);
		} catch (NoSuchAlgorithmException e) {
			LOGGER.log(Level.WARNING, "in calcHash NoSuchAlgorithmException");
		}
		return digest;
	}

	public static byte[] signRSA(byte[] input, PrivateKey privateKey) {
		byte[] result = null;

		Cipher signCipher;
		try {
			signCipher = Cipher.getInstance(ENCRYPTION_ALGORITHM);
			signCipher.init(Cipher.ENCRYPT_MODE, privateKey);
			result = signCipher.doFinal(input);
		} catch (Exception e) {
			LOGGER.log(Level.WARNING, "Exception in signRSA");
		}
		return result;
	}

	public static byte[] verifyRSA(byte[] input, PublicKey publicKey) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
		byte[] result = null;

		Cipher verifyCipher = Cipher.getInstance(ENCRYPTION_ALGORITHM);
		verifyCipher.init(Cipher.DECRYPT_MODE, publicKey);
		result = verifyCipher.doFinal(input);

		return result;

	}

	public static Boolean validateHash(byte[] h1, byte[] h2) {
		return Arrays.equals(h1, h2);
	}

}
