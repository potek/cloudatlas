package pl.edu.mimuw.cloudatlas.certification;

import java.security.PublicKey;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.Serializer;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;

public class ZoneCertSerializer extends Serializer<ZoneCert> {
	@Override
	public ZoneCert read(Kryo kryo, Input in, Class<ZoneCert> arg) {
		Long id = in.readLong();
		Long creation = in.readLong();
		Long level = in.readLong();
		String CAname = in.readString();
		Long expiry = in.readLong();	
		String name = in.readString();
		int signature_size = in.readInt();
		byte[] signature = in.readBytes(signature_size);
		PublicKey publickey = (PublicKey)kryo.readClassAndObject(in);
		
		return new ZoneCert(CAname, id, creation, expiry, level, name, publickey, signature);
	}

	@Override
	public void write(Kryo kryo, Output out, ZoneCert arg) {
		out.writeLong(arg.id);
		out.writeLong(arg.creation);
		out.writeLong(arg.getLevel());
		out.writeString(arg.getCAname());
		out.writeLong(arg.getExpiry());
		out.writeString(arg.getName());
		out.writeInt(arg.getSignature().length);
		out.write(arg.getSignature());
		
		kryo.writeClassAndObject(out, arg.getPublickey());
	}
}