package pl.edu.mimuw.cloudatlas.certification;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.InflaterInputStream;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;

public class AFCert extends Certificate {
	private static final long serialVersionUID = 6288929662233772826L;

	private static final ThreadLocal<Kryo> kryoThreadLocal = new ThreadLocal<Kryo>() {
		@Override
		protected Kryo initialValue() {
			Kryo kryo = new Kryo();
			kryo.register(AFCert.class);
			return kryo;
		}
	};

	// code = queryName + select queries;

	private String code;
	private String name;
	private int minLevel;
	private int maxLevel;
	private ClientCert client;

	public AFCert(Long id, Long creation, Long expiry) {
		super(id, creation, expiry);
	}

	public AFCert(Long id, Long creation, Long expiry, String name,
			String code, int minLevel, int maxLevel, ClientCert client) {
		super(id, creation, expiry);
		this.setCode(code);
		this.setName(name);
		this.setMinLevel(minLevel);
		this.setMaxLevel(maxLevel);
		this.setClient(client);
	}

	@Override
	protected String serialize() {
		StringBuilder sb = new StringBuilder();
		sb.append(getCode() + ",");
		sb.append(name + ",");
		sb.append(minLevel + ",");
		sb.append(maxLevel + ",");
		sb.append(client + ",");
		return sb.toString();
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("AFCCert {");
		sb.append("id : " + id + ", ");
		sb.append("creation : " + creation + ", ");
		sb.append("expiry : " + expiry + ", ");
		sb.append("query name : " + name + ", ");
		sb.append("query code : " + getCode() + ", ");
		sb.append("minLevel : " + minLevel + ", ");
		sb.append("maxLevel : " + maxLevel + ", ");
		sb.append("client : " + client + "} ");
		return sb.toString();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getMinLevel() {
		return minLevel;
	}

	public void setMinLevel(int minLevel) {
		this.minLevel = minLevel;
	}

	public int getMaxLevel() {
		return maxLevel;
	}

	public void setMaxLevel(int maxLevel) {
		this.maxLevel = maxLevel;
	}

	public ClientCert getClient() {
		return client;
	}

	public void setClient(ClientCert client) {
		this.client = client;
	}

	private void writeObject(java.io.ObjectOutputStream stream)
			throws IOException {

		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(
				16384);
		DeflaterOutputStream deflaterOutputStream = new DeflaterOutputStream(
				byteArrayOutputStream);
		Output output = new Output(deflaterOutputStream);
		Kryo kryo = kryoThreadLocal.get();
		kryo.writeObject(output, this);
		output.close();

		byte[] bytes = byteArrayOutputStream.toByteArray();
		stream.write(bytes);
	}

	private void readObject(java.io.ObjectInputStream stream)
			throws IOException, ClassNotFoundException {
		Input input = new Input(new InflaterInputStream(stream));
		Kryo kryo = kryoThreadLocal.get();
		AFCert newAFC = kryo.readObject(input, AFCert.class);

		this.setId(newAFC.getId());
		this.setCreation(newAFC.getCreation());
		this.setExpiry(newAFC.getExpiry());
		this.setSignature(newAFC.getSignature());

		this.setCode(newAFC.getCode());
		this.setName(newAFC.getName());
		this.setMinLevel(newAFC.getMinLevel());
		this.setMaxLevel(newAFC.getMaxLevel());
		this.setClient(newAFC.getClient());
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
}
