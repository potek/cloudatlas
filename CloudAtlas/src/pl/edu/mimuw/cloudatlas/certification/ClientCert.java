package pl.edu.mimuw.cloudatlas.certification;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.PublicKey;
import java.util.List;
import java.util.Set;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.InflaterInputStream;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;

public class ClientCert extends Certificate {
	private static final long serialVersionUID = 882925690915112633L;
	
	private static final ThreadLocal<Kryo> kryoThreadLocal = new ThreadLocal<Kryo>() {
		@Override
		protected Kryo initialValue() {
			Kryo kryo = new Kryo();
			kryo.register(ClientCert.class);
			return kryo;
		}
	};

	private String read_regexp;
	private String write_regexp;
	private List<String> allowedZones;
	private PublicKey publickey;
	private String CAname;

	public ClientCert(Long id, Long creation, Long expiry) {
		super(id, creation, expiry);
		// TODO Auto-generated constructor stub
	}

	public ClientCert(String CAname, Long id, Long creation, Long expiry,
			String read_regexp, String write_regexp,
			List<String> allowedZones, PublicKey publickey) {
		super(id, creation, expiry);
		this.setRead_regexp(read_regexp);
		this.setWrite_regexp(write_regexp);
		this.setAllowedZones(allowedZones);
		this.setPublickey(publickey);
		this.CAname = CAname;
	}
	
	public Boolean canRead(String attr) {
		return attr.matches(read_regexp);
	}

	public Boolean canWrite(String attr) {
		return attr.matches(write_regexp);
	}

	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("ClientCert {");
		sb.append("CAname : " + CAname + ", ");		
		sb.append("id : " + id + ", ");
		sb.append("creation : " + creation + ", ");
		sb.append("expiry : " + expiry + ", ");
		sb.append("read regexo : " + getRead_regexp() +", ");
		sb.append("write regexp : " + getWrite_regexp() + ", ");
		sb.append("zones : ");
		for(String s : getAllowedZones()) {
			sb.append(s +"; ");
		}
		sb.append("publickey : " + getPublickey() + "} ");
		return sb.toString();
	}

	@Override
	protected String serialize() {
		StringBuilder sb = new StringBuilder();
		sb.append(id);
		sb.append(creation);
		sb.append(expiry);
		sb.append(CAname);
		sb.append(getRead_regexp());
		sb.append(getWrite_regexp());
		for(String s : getAllowedZones())
			sb.append(s);
		sb.append(getPublickey());
		return sb.toString();
	}
	
	public boolean canInstallQuery(String zoneName) {
		return allowedZones.contains(zoneName);
	}

	public PublicKey getPublickey() {
		return publickey;
	}

	public void setPublickey(PublicKey publickey) {
		this.publickey = publickey;
	}

	public List<String> getAllowedZones() {
		return allowedZones;
	}

	public void setAllowedZones(List<String> allowedZones) {
		this.allowedZones = allowedZones;
	}

	public String getWrite_regexp() {
		return write_regexp;
	}

	public void setWrite_regexp(String write_regexp) {
		this.write_regexp = write_regexp;
	}

	public String getRead_regexp() {
		return read_regexp;
	}

	public void setRead_regexp(String read_regexp) {
		this.read_regexp = read_regexp;
	}
	
	private void writeObject(java.io.ObjectOutputStream stream)
			throws IOException {

		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(
				16384);
		DeflaterOutputStream deflaterOutputStream = new DeflaterOutputStream(
				byteArrayOutputStream);
		Output output = new Output(deflaterOutputStream);
		Kryo kryo = kryoThreadLocal.get();
		kryo.writeObject(output, this);
		output.close();

		byte[] bytes = byteArrayOutputStream.toByteArray();
		stream.write(bytes);
	}

	private void readObject(java.io.ObjectInputStream stream)
			throws IOException, ClassNotFoundException {
		Input input = new Input(new InflaterInputStream(stream));
		Kryo kryo = kryoThreadLocal.get();
		ClientCert newCC = kryo.readObject(input, ClientCert.class);
		
		this.setId(newCC.getId());
		this.setCreation(newCC.getCreation());
		this.setExpiry(newCC.getExpiry());
		this.setSignature(newCC.getSignature());
		this.setCAname(newCC.getCAname());
		this.setRead_regexp(newCC.getRead_regexp());
		this.setWrite_regexp(newCC.getWrite_regexp());
		this.setAllowedZones(newCC.getAllowedZones());
		this.setPublickey(newCC.getPublickey());
	}

	public String getCAname() {
		return CAname;
	}

	public void setCAname(String cAname) {
		CAname = cAname;
	}
}
