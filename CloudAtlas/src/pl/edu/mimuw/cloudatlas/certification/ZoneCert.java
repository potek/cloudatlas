package pl.edu.mimuw.cloudatlas.certification;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.PublicKey;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.InflaterInputStream;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;

public class ZoneCert extends Certificate {

	private static final ThreadLocal<Kryo> kryoThreadLocal = new ThreadLocal<Kryo>() {
		@Override
		protected Kryo initialValue() {
			Kryo kryo = new Kryo();
			kryo.register(ZoneCert.class, new ZoneCertSerializer(), 10);
			return kryo;
		}
	};

	private static final long serialVersionUID = 3475863094197352647L;

	private Long level;
	private String name;
	private PublicKey publickey;
	private String CAname;

	public ZoneCert() {
		super(0L, 0L, 0L);
	}
	
	public ZoneCert(String CAname, Long id, Long creation, Long expiry, Long level,
			String name, PublicKey publickey) {
		super(id, creation, expiry);
		this.setLevel(level);
		this.setName(name);
		this.setPublickey(publickey);
		this.setCAname(CAname);
	}
	
	public ZoneCert(String CAname, Long id, Long creation, Long expiry,
			Long level, String name, PublicKey publickey, byte[] signature) {
		this(CAname, id, creation,expiry, level, name, publickey);
		this.signature = signature;
	}

	protected String serialize() {
		StringBuilder sb = new StringBuilder();
		sb.append(id);
		sb.append(creation);
		sb.append(expiry);
		sb.append(CAname);
		sb.append(level);
		sb.append(name);
		sb.append(publickey.toString());
		return sb.toString();
	}

	public Long getLevel() {
		return level;
	}

	public void setLevel(Long level) {
		this.level = level;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public PublicKey getPublickey() {
		return publickey;
	}

	public void setPublickey(PublicKey publickey) {
		this.publickey = publickey;
	}

	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("ZoneCert {");
		sb.append("CA name : " + CAname + ", ");
		sb.append("id : " + id + ", ");
		sb.append("creation : " + creation + ", ");
		sb.append("expiry : " + expiry + ", ");
		sb.append("level : " + level + ", ");
		sb.append("name : " + name + ", ");
		sb.append("publickey : " + publickey + "} ");
		return sb.toString();
	}

	private void writeObject(java.io.ObjectOutputStream stream)
			throws IOException {

		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(
				16384);
		DeflaterOutputStream deflaterOutputStream = new DeflaterOutputStream(
				byteArrayOutputStream);
		Output output = new Output(deflaterOutputStream);
		Kryo kryo = kryoThreadLocal.get();
		kryo.writeObject(output, this);
		output.close();

		byte[] bytes = byteArrayOutputStream.toByteArray();
		stream.write(bytes);
	}

	private void readObject(java.io.ObjectInputStream stream)
			throws IOException, ClassNotFoundException {
        Input input = new Input(new InflaterInputStream(stream));
        Kryo kryo = kryoThreadLocal.get();
        ZoneCert newZC = kryo.readObject(input, ZoneCert.class);
        this.id = newZC.id;
        this.creation = newZC.creation;
        this.expiry = newZC.expiry;
        this.signature = newZC.signature;
        this.name = newZC.name;
        this.level = newZC.level;
        this.publickey = newZC.publickey;
        this.CAname = newZC.CAname;
	}

	public String getCAname() {
		return CAname;
	}

	public void setCAname(String cAname) {
		CAname = cAname;
	}

}
