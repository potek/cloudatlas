package pl.edu.mimuw.cloudatlas.certification;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.InflaterInputStream;

import pl.edu.mimuw.cloudatlas.common.CryptoManager;
import pl.edu.mimuw.cloudatlas.common.PropUtils;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;

public class CertificationAuthority {
	private final static Logger LOGGER = Logger
			.getLogger(CertificationAuthority.class.getName());

	private final static String usage = "java <CertificationAuthority class> <path_to_CA's_folder> --create-zone <expirt time> <the_global_zone_name>\n"
			+ "java <CertificationAuthority class> <path_to_CA's_folder> --create-client <expirt time>  <conf file>\n"
			+ "java <CertificationAuthority class> <path_to_CA's_folder> --create-ACC <expirt time> <zonename> <conf file1> <conf file2> ... <conf filen>";

	private static PublicKey publickey;
	private static PrivateKey privatekey;
	private static final Kryo kryo = new Kryo();
	private static Long EXPIRY_TIME = 60*1000L;

	public static void main(String[] args) {
		kryo.register(ZoneCert.class, new ZoneCertSerializer(), 10);
		EXPIRY_TIME = TimeUnit.SECONDS.toMillis(Long.parseLong(args[2]));
		if (args.length >= 3) { 
			// reading public and private keys from file
			if (readKeys(args[0])) {
				// switching options
				if (args[1].compareTo("--create-zone") == 0) {
					//LOGGER.log(Level.INFO, "Create Zone Option");
					createZoneCert(args[0], args[3]);
				} else if (args[1].compareTo("--create-client") == 0) {
					//LOGGER.log(Level.INFO, "Client Cert Option");
					createClientCert(args[0], args[3]);
				} else if (args[1].compareTo("--create-ACC") == 0) {
					//LOGGER.log(Level.INFO, "ACC Option");
					createACC(args);
				} else {
					LOGGER.log(Level.WARNING, "No known option");
					printUsage();
				}

			} else {
				LOGGER.log(Level.WARNING, "Reading key from file doesn't succeed!");
			}
		} else {
			LOGGER.log(Level.WARNING, "Number of arguments must be at least 2");
			printUsage();
		}

	}

	private static void createACC(String[] args) {
		Properties prop = new Properties();
		String CA_path = args[0];
		String zonename = args[3];

		Long id = (new Random()).nextLong();
		Long creation = (new Date()).getTime();
		Long expiry = creation + EXPIRY_TIME;
		AccessControlCert acc = new AccessControlCert(id, creation, expiry);

		
		for (int i = 4; i < args.length; i++) {
			try {
				String conf_filename = args[i];
				InputStream input = new FileInputStream(conf_filename);
				prop.load(input);

				String CAname = PropUtils.getPropOrThrow(prop, "CAname");
				String zoneNameConf = PropUtils.getPropOrThrow(prop, "zone");
				String read_str = PropUtils.getPropOrThrow(prop, "read");
				String write_str = PropUtils.getPropOrThrow(prop, "write");
				String zones_str = PropUtils.getPropOrThrow(prop, "zones");
				
				acc.addRead(zoneNameConf, CAname , stringListToList(read_str));
				acc.addWrite(zoneNameConf, CAname , stringListToList(write_str));
				acc.addZone(zoneNameConf, CAname , stringListToList(zones_str));

			} catch (Exception e) {
				LOGGER.log(Level.WARNING, "Cannot create acc certification", e);
			}
		}
		try {
			String new_path = CA_path + "zones/" + zonename;
			Path p = Paths.get(new_path);
			Files.createDirectories(p);

			acc.generateSignature(privatekey, true);

			File f = new File(p.toString(), "cert_access_control.crt");

			DeflaterOutputStream dos = new DeflaterOutputStream(
					new FileOutputStream(f));
			Output out = new Output(dos);
			kryo.writeObject(out, acc);
			out.close();

			LOGGER.log(Level.INFO, "ACC for " + zonename + " has been generated");
		} catch (Exception e) {
			LOGGER.log(Level.WARNING, "createAcc - Exception");
		}
	}

	private static List<String> stringListToList(String str) {
		List<String> set = new LinkedList<String>();
		if (str != null) {
			for (String s : str.split(",")) {
				set.add(s);
			}
		}
		return set;
	}

	private static void createClientCert(String dir_path, String conf_fn) {

		Properties prop = new Properties();
		try {
			InputStream input = new FileInputStream(conf_fn);
			prop.load(input);
			String clientname = PropUtils.getPropOrThrow(prop, "name");
			String read_str = PropUtils.getPropOrThrow(prop, "read");
			String write_str = PropUtils.getPropOrThrow(prop, "write");
			String zones_str = PropUtils.getPropOrThrow(prop, "zones");

			String new_path = dir_path + "clients/" + clientname;
			Path p = Paths.get(new_path);
			Files.createDirectories(p);

			KeyPair keyPair = CryptoManager.genKey();
			PrivateKey privateKeyClient = keyPair.getPrivate();
			PublicKey publicKeyClient = keyPair.getPublic();
			CryptoManager.writeKey(p.toString() + "/public_key.der",
					publicKeyClient);
			CryptoManager.writeKey(p.toString() + "/private_key.der",
					privateKeyClient);

			Long id = (new Random()).nextLong();
			Long creation = (new Date()).getTime();
			Long expiry = creation + EXPIRY_TIME;
			ClientCert cc = new ClientCert(dir_path, id, creation, expiry,
					read_str, write_str, stringListToList(zones_str),
					publicKeyClient);

			cc.generateSignature(privatekey, true);

			File f = new File(p.toString(), "cert_client.crt");

			DeflaterOutputStream dos = new DeflaterOutputStream(
					new FileOutputStream(f));
			Output out = new Output(dos);
			kryo.writeClassAndObject(out, cc);
			out.close();

			LOGGER.log(Level.INFO, "CC for " + p.toString() + " has been generated");

		} catch (Exception e) {
			LOGGER.log(Level.WARNING, "Cannot create client certification", e);
		}
	}

	private static void createZoneCert(String dir_path, String global_name) {
		String new_path = dir_path + "zones/" + global_name;
		Path p = Paths.get(new_path);
		try {

			Files.createDirectories(p);

			File f = new File(p.toString(), "cert_zone.crt");
			if (f.exists() && !f.isDirectory()) {
				LOGGER.log(Level.WARNING, "Certificate already exists");
				// TODO replace?
			}

			// create private & public key for zone

			PrivateKey privateKeyZone = null;
			PublicKey publicKeyZone = null;

			try {
				KeyPair keyPair = CryptoManager.genKey();
				privateKeyZone = keyPair.getPrivate();
				publicKeyZone = keyPair.getPublic();
				CryptoManager.writeKey(p.toString() + "/public_key.der",
						publicKeyZone);
				CryptoManager.writeKey(p.toString() + "/private_key.der",
						privateKeyZone);
			} catch (NoSuchAlgorithmException e) {
				LOGGER.logp(Level.SEVERE, "CertificationAuthority",
						"createZoneCert",
						"Cannot generate new keyPair. Change EncryptAlgorithm",
						e);
			} catch (IOException e) {
				LOGGER.logp(
						Level.SEVERE,
						"CertificationAuthority",
						"createZoneCert",
						"Cannot generate new keyPair. Cannot write keys into file",
						e);
			}

			// create cert
			Long id_cert = (new Random()).nextLong();
			Long crea_cert = (new Date()).getTime();
			Long exp_cert = crea_cert + EXPIRY_TIME;

			Path zonePath = Paths.get(global_name);

			ZoneCert zc = new ZoneCert(dir_path, id_cert, crea_cert, exp_cert,
					Long.valueOf(zonePath.getNameCount()), global_name,
					publicKeyZone);

			zc.generateSignature(privatekey, true);


			DeflaterOutputStream dos = new DeflaterOutputStream(
					new FileOutputStream(f));
			Output out = new Output(dos);
			kryo.writeObject(out, zc);
			out.close();

			LOGGER.log(Level.INFO, "ZoneCert for " + zonePath + " has been geenrated");

		} catch (IOException e) {
			LOGGER.log(Level.SEVERE, "Cannot create dir " + new_path, e);
		}
	}

	private static Boolean readKeys(String args) {
		Properties prop = new Properties();
		InputStream input = null;

		// Reading conf.ini file
		try {
			input = new FileInputStream(args + "conf.ini");
			prop.load(input);
		} catch (IOException e) {
			LOGGER.logp(Level.SEVERE, "CertificationAuthority", "main",
					"Cannot open input file: conf.ini ", e);
			return false;
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					LOGGER.logp(Level.SEVERE, "CertificationAuthority", "main",
							"Cannot close input file: " + args, e);
				}
			}
		}

		String priv_key_filename = PropUtils.getPropOrNull(prop, "prv-key");
		String pub_key_filename = PropUtils.getPropOrNull(prop, "pub-key");

		if (priv_key_filename != null) {
			try {
				privatekey = CryptoManager.getPrivateKey(priv_key_filename);
			} catch (Exception e) {
				LOGGER.logp(Level.SEVERE, "CertificationAuthority", "readKeys",
						"Cannot read private key from a file:  "
								+ priv_key_filename, e);
				return false;
			}
		}

		if (pub_key_filename != null) {
			try {

				publickey = CryptoManager.getPublicKey(pub_key_filename);
			} catch (Exception e) {
				LOGGER.logp(Level.SEVERE, "CertificationAuthority", "readKeys",
						"Cannot read public key from a file:  " + " or "
								+ pub_key_filename, e);
				return false;
			}
		}

		return true;
	}

	private static void printUsage() {
		System.out.println(usage);
	}
}
