package pl.edu.mimuw.cloudatlas.certification;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.InflaterInputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import com.esotericsoftware.kryo.serializers.JavaSerializer;

import pl.edu.mimuw.cloudatlas.common.CryptoManager;
import pl.edu.mimuw.cloudatlas.message.Package;
import pl.edu.mimuw.cloudatlas.model.Attribute;
import pl.edu.mimuw.cloudatlas.model.Value;

public class ZMICert extends Certificate {
	private static final long serialVersionUID = -579199538205526985L;
	private final static Logger LOGGER = Logger.getLogger(ZMICert.class.getName());
	private static final ThreadLocal<Kryo> kryoThreadLocal = new ThreadLocal<Kryo>() {
		@Override
		protected Kryo initialValue() {
			Kryo kryo = new Kryo();
			kryo.register(ZoneCert.class, new ZoneCertSerializer());
			kryo.register(ZMICert.class);
			kryo.register(Package.class, new JavaSerializer());
			return kryo;
		}
	};
	private final static String DIGEST_ALGORITHM = "SHA-1";

	private ArrayList<Package> packageZMI;
	private ZoneCert zoneCert;

	public ZMICert(long id, long creation, long expiry,
			ArrayList<Package> alp,
			ZoneCert zc) {
		super(id, creation, expiry);
		this.packageZMI = alp;
		this.zoneCert = zc;
	}
	
	public void setID(long id){
		this.id = id;
	}

	protected String serialize() {
		StringBuilder sb = new StringBuilder();
		for(Package p : packageZMI) {
			sb.append(p.serialize());
		}
		sb.append(zoneCert);
		sb.append(id);
		sb.append(expiry);
		sb.append(creation);
		return sb.toString();
	}

	@Override
	public byte[] generateSignature(PrivateKey privatekey, Boolean refresh) {
		String serialized = serialize();

		byte[] raw = null;
		try {
			raw = CryptoManager.toByte(serialized);
		} catch (IOException e) {
			LOGGER.log(Level.WARNING, "in generateSignature IOException");
		}
		byte[] hashed = CryptoManager.calcHash(raw, DIGEST_ALGORITHM);
		byte[] crypted = CryptoManager.signRSA(hashed, privatekey);

		if (refresh)
			this.signature = crypted;

		return crypted;
	}

	@Override
	public Boolean validSignature(PublicKey publickey) {
		try {
			String serialized = serialize();
			byte[] raw = CryptoManager.toByte(serialized);
			byte[] hashed = CryptoManager.calcHash(raw, DIGEST_ALGORITHM);
			byte[] decrypted = CryptoManager.verifyRSA(signature, publickey);

			return Arrays.equals(decrypted, hashed);
		} catch (Exception e) {
			LOGGER.log(Level.WARNING, "ZMICert - validSignature Exception");
			return false;
		}
	}

	public ArrayList<Package> getPackageZMI() {
		return packageZMI;
	}

	public void setPackageZMI(ArrayList<Package> packageZMI) {
		this.packageZMI = packageZMI;
	}

	public ZoneCert getZoneCert() {
		return zoneCert;
	}

	public void setZoneCert(ZoneCert zoneCert) {
		this.zoneCert = zoneCert;
	}

	private void writeObject(java.io.ObjectOutputStream stream)
			throws IOException {

		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(
				16384);
		DeflaterOutputStream deflaterOutputStream = new DeflaterOutputStream(
				byteArrayOutputStream);
		Output output = new Output(deflaterOutputStream);
		Kryo kryo = kryoThreadLocal.get();
		kryo.writeObject(output, this);
		output.close();

		byte[] bytes = byteArrayOutputStream.toByteArray();
		stream.write(bytes);
	}

	private void readObject(java.io.ObjectInputStream stream)
			throws IOException, ClassNotFoundException {
		Input input = new Input(new InflaterInputStream(stream));
		Kryo kryo = kryoThreadLocal.get();
		ZMICert newZC = kryo.readObject(input, ZMICert.class);
		this.id = newZC.id;
		this.creation = newZC.creation;
		this.expiry = newZC.expiry;
		this.signature = newZC.signature;
		
		this.packageZMI = newZC.packageZMI;
		this.zoneCert = newZC.zoneCert;
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("ZMICert {");
		sb.append("id : " + id + ", ");
		sb.append("creation : " + creation + ", ");
		sb.append("expiry : " + expiry + ", ");
		sb.append("PackageList : " + packageZMI + ", ");
		sb.append("zoneCert : " + zoneCert + "} ");
		return sb.toString();
	}
}
