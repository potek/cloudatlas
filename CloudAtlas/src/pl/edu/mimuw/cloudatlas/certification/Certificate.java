package pl.edu.mimuw.cloudatlas.certification;

import java.io.IOException;
import java.io.Serializable;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.Arrays;
import java.util.Date;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

import pl.edu.mimuw.cloudatlas.common.CryptoManager;

public abstract class Certificate implements Serializable {
	private final static Logger LOGGER = Logger.getLogger(Certificate.class.getName());
	private final static String DIGEST_ALGORITHM = "SHA-1";
	private static final long serialVersionUID = 431493414182369513L;
	protected Long id;
	protected Long creation;
	protected byte[] signature;
	protected Long expiry;

	public Certificate(Long id, Long creation, Long expiry) {
		this.id = id;
		this.creation = creation;
		this.expiry = expiry;
	}
	
	public Certificate(Long creation, Long expiry) {
		Random gen = new Random();
		this.id = gen.nextLong();
		this.creation = creation;
		this.expiry = expiry;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCreation() {
		return creation;
	}

	public void setCreation(Long creation) {
		this.creation = creation;
	}

	public byte[] getSignature() {
		return signature;
	}

	public void setSignature(byte[] signature) {
		this.signature = signature;
	}

	public Long getExpiry() {
		return expiry;
	}

	public void setExpiry(Long expiry) {
		this.expiry = expiry;
	}
	
	public boolean certificateHasExpired() {
		return (expiry < (new Date().getTime()));
	}

	public byte[] generateSignature(PrivateKey privatekey, Boolean refresh) {
		String serialized = serialize();

		byte[] raw = null;
		try {
			raw = CryptoManager.toByte(serialized);
		} catch (IOException e) {
			LOGGER.log(Level.WARNING, "generateSignature - IOException");
		}
		byte[] hashed = CryptoManager.calcHash(raw, DIGEST_ALGORITHM);
		byte[] crypted = CryptoManager.signRSA(hashed, privatekey);

		if (refresh)
			this.signature = crypted;

		return crypted;
	}
	

	public Boolean validSignature(PublicKey publickey) {
		try {
			String serialized = serialize();
			byte[] raw = CryptoManager.toByte(serialized);
			byte[] hashed = CryptoManager.calcHash(raw, DIGEST_ALGORITHM);
			byte[] decrypted = CryptoManager.verifyRSA(signature, publickey);

			return Arrays.equals(decrypted, hashed);
		} catch (Exception e) {
			LOGGER.log(Level.WARNING, "Certificate - validSignature Exception");
			return false;
		}
	}
	
	protected abstract String serialize();
	
	public abstract String toString();
}
