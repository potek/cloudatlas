package pl.edu.mimuw.cloudatlas.certification;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.InflaterInputStream;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;

public class AccessControlCert extends Certificate {
	private static final long serialVersionUID = -6821828227599748104L;

	private static final ThreadLocal<Kryo> kryoThreadLocal = new ThreadLocal<Kryo>() {
		@Override
		protected Kryo initialValue() {
			Kryo kryo = new Kryo();
			//kryo.register(AccessControlCert.class);
			return kryo;
		}
	};
	
	private Map<String, List<String>> readAttributes;
	private Map<String, List<String>> writeAttributes;
	private Map<String, List<String>> zonePrivileges;

	public AccessControlCert(Long id, Long creation, Long expiry) {
		super(id, creation, expiry);
		this.readAttributes = new HashMap<String, List<String>>();
		this.writeAttributes = new HashMap<String, List<String>>();
		this.zonePrivileges = new HashMap<String, List<String>>();
	}

	private String combineNames(String zoneName, String CAname) {
		return zoneName + "," + CAname;
	}

	public void addRead(String zoneName, String CAname, List<String> list) {
		readAttributes.put(combineNames(zoneName, CAname), list);
	}

	public void addWrite(String zoneName, String CAname, List<String> list) {
		writeAttributes.put(combineNames(zoneName, CAname), list);
	}

	public void addZone(String zoneName, String CAname, List<String> list) {
		zonePrivileges.put(combineNames(zoneName, CAname), list);
	}

	public Boolean canRead(String zoneName, String CAname, String attr) {
		String clientName = combineNames(zoneName, CAname);
		if (readAttributes.containsKey(clientName)) {
			for (String regex : readAttributes.get(clientName)) {
				if (attr.matches(regex))
					return true;
			}
		}
		return false;
	}
	
	public Boolean canWrite(String zoneName, String CAname, String attr) {
		String clientName = combineNames(zoneName, CAname);
		if (writeAttributes.containsKey(clientName)) {
			for (String regex : writeAttributes.get(clientName)) {
				if (attr.matches(regex))
					return true;
			}
		}
		return false;
	}
	
	public Boolean canInstallQuery(String myZoneName, String whereZoneName, String CAname) {
		List<String> list = zonePrivileges.get(combineNames(myZoneName, CAname));
		if(list == null)
			return false;
		return list.contains(whereZoneName);
	}

	@Override
	protected String serialize() {
		StringBuilder sb = new StringBuilder();
		sb.append(id);
		sb.append(expiry);
		sb.append(creation);

		for (Entry<String, List<String>> entry : getReadAttributes().entrySet()) {
			sb.append(entry.getKey());
			for (String s : entry.getValue()) {
				sb.append(s);
			}
		}
		for (Entry<String, List<String>> entry : getWriteAttributes()
				.entrySet()) {
			sb.append(entry.getKey());
			for (String s : entry.getValue()) {
				sb.append(s);
			}
		}

		for (Entry<String, List<String>> entry : zonePrivileges.entrySet()) {
			sb.append(entry.getKey());
			for (String s : entry.getValue()) {
				sb.append(s);
			}
		}

		return sb.toString();
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("ACCert {");
		sb.append("id : " + id + ", ");
		sb.append("creation : " + creation + ", ");
		sb.append("expiry : " + expiry + ", ");

		sb.append("readAttributes: ");
		for (Entry<String, List<String>> entry : getReadAttributes().entrySet()) {
			sb.append("[" + entry.getKey() + ":");
			for (String s : entry.getValue()) {
				sb.append(s + ";");
			}
			sb.append("] ");
		}
		sb.append("writeAttributes: ");
		for (Entry<String, List<String>> entry : getWriteAttributes()
				.entrySet()) {
			sb.append("[" + entry.getKey() + ":");
			for (String s : entry.getValue()) {
				sb.append(s + ";");
			}
			sb.append("] ");
		}
		sb.append("zonePrivileges: ");
		for (Entry<String, List<String>> entry : getZonePrivileges().entrySet()) {
			sb.append("[" + entry.getKey() + ":");
			for (String s : entry.getValue()) {
				sb.append(s + ";");
			}
			sb.append("] ");
		}

		return sb.toString();
	}

	public Map<String, List<String>> getReadAttributes() {
		return readAttributes;
	}

	public void setReadAttributes(Map<String, List<String>> readAttributes) {
		this.readAttributes = readAttributes;
	}

	public Map<String, List<String>> getWriteAttributes() {
		return writeAttributes;
	}

	public void setWriteAttributes(Map<String, List<String>> writeAttributes) {
		this.writeAttributes = writeAttributes;
	}

	public Map<String, List<String>> getZonePrivileges() {
		return zonePrivileges;
	}

	public void setZonePrivileges(Map<String, List<String>> zonePrivileges) {
		this.zonePrivileges = zonePrivileges;
	}

	private void writeObject(java.io.ObjectOutputStream stream)
			throws IOException {

		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(
				16384);
		DeflaterOutputStream deflaterOutputStream = new DeflaterOutputStream(
				byteArrayOutputStream);
		Output output = new Output(deflaterOutputStream);
		Kryo kryo = kryoThreadLocal.get();
		kryo.writeObject(output, this);
		output.close();

		byte[] bytes = byteArrayOutputStream.toByteArray();
		stream.write(bytes);
	}

	private void readObject(java.io.ObjectInputStream stream)
			throws IOException, ClassNotFoundException {
        Input input = new Input(new InflaterInputStream(stream));
        Kryo kryo = kryoThreadLocal.get();
        AccessControlCert newACC = kryo.readObject(input, AccessControlCert.class);
        this.id = newACC.id;
        this.creation = newACC.creation;
        this.expiry = newACC.expiry;
        this.signature = newACC.signature;
    	this.readAttributes = newACC.readAttributes;
    	this.writeAttributes = newACC.writeAttributes;
    	this.zonePrivileges = newACC.zonePrivileges;
	}
	
	@Override
	public AccessControlCert clone() {
		AccessControlCert newAcc = new AccessControlCert(id, creation, expiry);
		newAcc.readAttributes = new HashMap<String, List<String>>();
		newAcc.readAttributes.putAll(readAttributes);
		newAcc.writeAttributes = new HashMap<String, List<String>>();
		newAcc.writeAttributes.putAll(writeAttributes);
		newAcc.zonePrivileges = new HashMap<String, List<String>>();
		newAcc.zonePrivileges.putAll(zonePrivileges);
		return newAcc;
	}
}
