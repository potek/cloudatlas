#!/bin/bash
. pathes.sh

export CLASSPATH=$CLASSPATH:$CA_PATH/bin/:$CA_PATH/lib/JLex.jar:/$CA_PATH/lib/cup.jar:/$CA_PATH/kryo/lib/minlog-1.2.jar:/$CA_PATH/kryo/lib/objenesis-1.2.jar:/$CA_PATH/kryo/lib/reflectasm-1.09-shaded.jar:/$CA_PATH/kryo/kryo-3.0.0.jar; export CLASSPATH;

java -Djava.rmi.server.codebase=file:$CA_PATH/bin/ -Djava.security.policy=file:$CA_PATH/rmi/client.policy pl.edu.mimuw.cloudatlas.client.DataSender $HOSTNAME $1 $2
