/*
 * Distributed Systems Lab
 * Copyright (C) Konrad Iwanicki, 2012-2014
 *
 * This file contains code samples for the distributed systems
 * course. It is intended for internal use only.
 */
package server;

import fibonacci.Fibonacci;
import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

public class FibonacciServer {

	public static void main(String[] args) {
		if (System.getSecurityManager() == null) {
			System.setSecurityManager(new SecurityManager());
		}
		try {
			FibonacciComputer object = new FibonacciComputer();
			Fibonacci stub =
				(Fibonacci) UnicastRemoteObject.exportObject(object, 0);
			Registry registry = LocateRegistry.getRegistry();
			registry.rebind("Fibonacci", stub);
			System.out.println("FibonacciComputer bound");
		} catch (Exception e) {
			System.err.println("ComputeEngine exception:");
			e.printStackTrace();
		}
	}
}
