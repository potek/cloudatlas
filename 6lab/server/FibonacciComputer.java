/*
 * Distributed Systems Lab
 * Copyright (C) Konrad Iwanicki, 2012-2014
 *
 * This file contains code samples for the distributed systems
 * course. It is intended for internal use only.
 */
package server;

import fibonacci.Fibonacci;
import java.math.BigDecimal;
import java.rmi.RemoteException;

public class FibonacciComputer implements Fibonacci {

	public BigDecimal getNthNumber(BigDecimal n) throws RemoteException {
		BigDecimal f1 = BigDecimal.ONE;
		BigDecimal f2 = BigDecimal.ONE;
		if (n == null) {
			throw new RemoteException("N must not be null!");
		}
		while (BigDecimal.ONE.compareTo(n) < 0) {
			n = n.subtract(BigDecimal.ONE);
			BigDecimal t = f2;
			f2 = f1;
			f1 = t.add(f1);
		}
		return f1;
	}

}

