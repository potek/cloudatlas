#!/bin/sh
#
# Distributed Systems Lab
# Copyright (C) Konrad Iwanicki, 2012-2014
#
# This file contains code samples for the distributed systems
# course. It is intended for internal use only.
#

java -Djava.rmi.server.codebase=file:/home/students/inf/m/mp306407/marek/9semestr/sysroz/6lab -Djava.rmi.server.hostname=violet02 -Djava.security.policy=server.policy server.FibonacciServer
