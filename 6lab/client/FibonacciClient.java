/*
 * Distributed Systems Lab
 * Copyright (C) Konrad Iwanicki, 2012-2014
 *
 * This file contains code samples for the distributed systems
 * course. It is intended for internal use only.
 */
package client;

import fibonacci.Fibonacci;
import java.rmi.registry.LocateRegistry;
import java.rmi.RemoteException;
import java.rmi.registry.Registry;
import java.math.BigDecimal;

public class FibonacciClient {

	public static void main(String[] args) {
		if (args.length != 2) {
			System.err.println("Usage: java FibonacciClient <number>");
			System.exit(1);
		}
		try {
			Registry registry = LocateRegistry.getRegistry(args[0]);
			Fibonacci stub = (Fibonacci) registry.lookup("Fibonacci");
			BigDecimal n = new BigDecimal(args[1]);
			BigDecimal res = stub.getNthNumber(n);
			System.out.println(n.toString() + "-th Fibonacci number: " + res);
		} catch (Exception e) {
			System.err.println("FibonacciClient exception:");
			e.printStackTrace();
		}
	}

}

